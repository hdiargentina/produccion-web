package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;
import java.math.BigDecimal;

public class VersionDTO implements Serializable, Comparable<VersionDTO> {
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String codigo_equivalencia;
	private String descripcion;
	private String origen;
	private String tipo;
	private BigDecimal sumaAsegurada;
	private BigDecimal sumaAseguradaMaxima;
	private BigDecimal sumaAseguradaMinima;
	
	public VersionDTO() {}
	
	public VersionDTO (VersionDTO version) {
		this.codigo = version.getCodigo();
		this.codigo_equivalencia = version.getCodigo_equivalencia();
		this.descripcion = version.getDescripcion();
		this.origen = version.getOrigen();
		this.tipo = version.getTipo();
		this.sumaAsegurada = version.getSumaAsegurada();
		this.sumaAseguradaMaxima = version.getSumaAseguradaMaxima();
		this.sumaAseguradaMinima = version.getSumaAseguradaMinima();
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodigo_equivalencia() {
		return codigo_equivalencia;
	}
	public void setCodigo_equivalencia(String codigo_equivalencia) {
		this.codigo_equivalencia = codigo_equivalencia;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public BigDecimal getSumaAseguradaMaxima() {
		return sumaAseguradaMaxima;
	}
	public void setSumaAseguradaMaxima(BigDecimal sumaAseguradaMaxima) {
		this.sumaAseguradaMaxima = sumaAseguradaMaxima;
	}
	public BigDecimal getSumaAseguradaMinima() {
		return sumaAseguradaMinima;
	}
	public void setSumaAseguradaMinima(BigDecimal sumaAseguradaMinima) {
		this.sumaAseguradaMinima = sumaAseguradaMinima;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo_equivalencia != null)
            ? codigo_equivalencia.equals(((VersionDTO) other).codigo_equivalencia)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo_equivalencia != null) 
            ? (getClass().hashCode() + codigo_equivalencia.hashCode())
            : super.hashCode();
    }

	@Override
	public int compareTo(VersionDTO o) {
		return this.getDescripcion().compareTo(o.getDescripcion());
	}

}
