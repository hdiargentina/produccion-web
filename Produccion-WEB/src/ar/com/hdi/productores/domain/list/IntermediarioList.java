package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.IntermediarioDTO;

@XmlRootElement( name = "intermediarios" )
public class IntermediarioList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<IntermediarioDTO> intermediarios;
	private BigDecimal cantidad;
	public List<IntermediarioDTO> getIntermediarios() {
		return intermediarios;
	}
	@XmlElement( name = "intermediario" )
	public void setIntermediarios(List<IntermediarioDTO> intermediarios) {
		this.intermediarios = intermediarios;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	@XmlElement( name = "cantidad" )
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public void add (IntermediarioDTO intermediario) {
		if (this.intermediarios == null) {
			this.intermediarios = new ArrayList<IntermediarioDTO>();
		}
		this.intermediarios.add(intermediario);
	}
	
}
