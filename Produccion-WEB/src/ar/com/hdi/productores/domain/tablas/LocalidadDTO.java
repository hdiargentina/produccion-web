package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;
import java.math.BigDecimal;

public class LocalidadDTO implements Serializable, Comparable<LocalidadDTO> {
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private BigDecimal codigoPostal;
	private BigDecimal sufijoCodigoPostal;
	private String descripcion;
	private BigDecimal zonaRiesgoAuto;
	private BigDecimal zonaRiesgoVario;
	private String provincia;
	
	public LocalidadDTO() {super();}
	
	public LocalidadDTO(LocalidadDTO localidad) {
		super();
		this.codigo = localidad.getCodigo();
		this.codigoPostal = localidad.getCodigoPostal();
		this.sufijoCodigoPostal = localidad.getSufijoCodigoPostal();
		this.descripcion = localidad.getDescripcion();
		this.zonaRiesgoAuto = localidad.getZonaRiesgoAuto();
		this.zonaRiesgoVario = localidad.getZonaRiesgoVario();
		this.provincia = localidad.getProvincia();
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public BigDecimal getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(BigDecimal codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public BigDecimal getSufijoCodigoPostal() {
		return sufijoCodigoPostal;
	}
	public void setSufijoCodigoPostal(BigDecimal sufijoCodigoPostal) {
		this.sufijoCodigoPostal = sufijoCodigoPostal;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getZonaRiesgoAuto() {
		return zonaRiesgoAuto;
	}
	public void setZonaRiesgoAuto(BigDecimal zonaRiesgoAuto) {
		this.zonaRiesgoAuto = zonaRiesgoAuto;
	}
	public BigDecimal getZonaRiesgoVario() {
		return zonaRiesgoVario;
	}
	public void setZonaRiesgoVario(BigDecimal zonaRiesgoVario) {
		this.zonaRiesgoVario = zonaRiesgoVario;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	@Override
	public String toString() {
		return this.getDescripcion() == null ? "" : this.getDescripcion() + " (" + this.getCodigoPostal() + " - " + this.getSufijoCodigoPostal() + ")";
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((LocalidadDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }

	@Override
	public int compareTo(LocalidadDTO o) {
		return descripcion.compareTo(o.getDescripcion());
	}
}
