package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "inspeccion" )
public class InspeccionPropuestaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal numero;
	private String estado;
	private String domicilio;
	private Date fecha;
	private String observaciones;
	public BigDecimal getNumero() {
		return numero;
	}
	@XmlElement( name = "nroInsp" )
	public void setNumero(BigDecimal numero) {
		this.numero = numero;
	}
	public String getEstado() {
		return estado;
	}
	@XmlElement( name = "estadoInsp" )
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDomicilio() {
		return domicilio;
	}
	@XmlElement( name = "domicilio" )
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public Date getFecha() {
		return fecha;
	}
	@XmlElement( name = "fechaInsp" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getObservaciones() {
		return observaciones;
	}
	@XmlElement( name = "observaciones" )
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	

}
