package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement( name = "linea" )
public class LineaAgrupadoDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String rama;
	private BigDecimal cantidadDiaria;
	private BigDecimal primaNetaDiaria;
	private BigDecimal porcentajeDiario;
	private BigDecimal primaMasRecargoDiaria;
	private BigDecimal porcentajeComisionDiario;
	private BigDecimal comisionDiario;
	private BigDecimal cantidadMensual;
	private BigDecimal primaNetaMensual;
	private BigDecimal porcentajeMensual;
	private BigDecimal primaMasRecargoMensual;
	private BigDecimal comisionMensual;
	private BigDecimal porcentajeComisionMensual;
	
	public String getRama() {
		return rama;
	}
	@XmlElement( name = "rama")
	public void setRama(String rama) {
		this.rama = rama;
	}

	public BigDecimal getCantidadDiaria() {
		return cantidadDiaria;
	}
	@XmlElement( name = "cantidadDiaria")
	public void setCantidadDiaria(BigDecimal cantidadDiaria) {
		this.cantidadDiaria = cantidadDiaria;
	}

	public BigDecimal getPrimaNetaDiaria() {
		return primaNetaDiaria;
	}
	@XmlElement( name = "primaNetaDiaria")
	public void setPrimaNetaDiaria(BigDecimal primaNetaDiaria) {
		this.primaNetaDiaria = primaNetaDiaria;
	}

	public BigDecimal getPorcentajeDiario() {
		return porcentajeDiario;
	}
	@XmlElement( name = "porcDiario")
	public void setPorcentajeDiario(BigDecimal porcentajeDiario) {
		this.porcentajeDiario = porcentajeDiario;
	}

	public BigDecimal getPrimaMasRecargoDiaria() {
		return primaMasRecargoDiaria;
	}
	@XmlElement( name = "primaMasRecDiaria")
	public void setPrimaMasRecargoDiaria(BigDecimal primaMasRecargoDiaria) {
		this.primaMasRecargoDiaria = primaMasRecargoDiaria;
	}

	public BigDecimal getPorcentajeComisionDiario() {
		return porcentajeComisionDiario;
	}
	@XmlElement( name = "porcComiDiario")
	public void setPorcentajeComisionDiario(BigDecimal porcentajeComisionDiario) {
		this.porcentajeComisionDiario = porcentajeComisionDiario;
	}

	public BigDecimal getComisionDiario() {
		return comisionDiario;
	}
	@XmlElement( name = "comisionDiaria")
	public void setComisionDiario(BigDecimal comisionDiario) {
		this.comisionDiario = comisionDiario;
	}

	public BigDecimal getCantidadMensual() {
		return cantidadMensual;
	}
	@XmlElement( name = "cantidadMensual")
	public void setCantidadMensual(BigDecimal cantidadMensual) {
		this.cantidadMensual = cantidadMensual;
	}

	public BigDecimal getPrimaNetaMensual() {
		return primaNetaMensual;
	}
	@XmlElement( name = "primaNetaMensual")
	public void setPrimaNetaMensual(BigDecimal primaNetaMensual) {
		this.primaNetaMensual = primaNetaMensual;
	}

	public BigDecimal getPorcentajeMensual() {
		return porcentajeMensual;
	}
	@XmlElement( name = "porcMensual")
	public void setPorcentajeMensual(BigDecimal porcentajeMensual) {
		this.porcentajeMensual = porcentajeMensual;
	}

	public BigDecimal getPrimaMasRecargoMensual() {
		return primaMasRecargoMensual;
	}
	@XmlElement( name = "primaMasRecMensual")
	public void setPrimaMasRecargoMensual(BigDecimal primaMasRecargoMensual) {
		this.primaMasRecargoMensual = primaMasRecargoMensual;
	}

	public BigDecimal getComisionMensual() {
		return comisionMensual;
	}
	@XmlElement( name = "comisionMensual")
	public void setComisionMensual(BigDecimal comisionMensual) {
		this.comisionMensual = comisionMensual;
	}

	public BigDecimal getPorcentajeComisionMensual() {
		return porcentajeComisionMensual;
	}
	@XmlElement( name = "porcComiMensual")
	public void setPorcentajeComisionMensual(BigDecimal porcentajeComisionMensual) {
		this.porcentajeComisionMensual = porcentajeComisionMensual;
	}

}
