package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;
import java.math.BigDecimal;

public class TipoFormaPagoDTO implements Serializable, Comparable<TipoFormaPagoDTO> {
	private static final long serialVersionUID = 1L;
	
	private Long codigo;
	private String codigo_equivalencia;
	private String descripcion;
	private String porDefecto;
	private BigDecimal cantCuotas;
	private String marp;
	private boolean efectivo = Boolean.FALSE;
	private boolean debito = Boolean.FALSE;
	private boolean credito = Boolean.FALSE;
	
	public TipoFormaPagoDTO(){}
	
	public TipoFormaPagoDTO(Long codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getCodigo_equivalencia() {
		return codigo_equivalencia;
	}

	public void setCodigo_equivalencia(String codigo_equivalencia) {
		this.codigo_equivalencia = codigo_equivalencia;
	}

	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getPorDefecto() {
		return porDefecto;
	}

	public void setPorDefecto(String porDefecto) {
		this.porDefecto = porDefecto;
	}

	public BigDecimal getCantCuotas() {
		return cantCuotas;
	}

	public void setCantCuotas(BigDecimal cantCuotas) {
		this.cantCuotas = cantCuotas;
	}

	public String getMarp() {
		return marp;
	}

	public void setMarp(String marp) {
		this.marp = marp;
	}

	public boolean isEfectivo() {
		return efectivo;
	}

	public void setEfectivo(boolean efectivo) {
		this.efectivo = efectivo;
	}

	public boolean isDebito() {
		return debito;
	}

	public void setDebito(boolean debito) {
		this.debito = debito;
	}

	public boolean isCredito() {
		return credito;
	}

	public void setCredito(boolean credito) {
		this.credito = credito;
	}

	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo_equivalencia != null)
            ? codigo_equivalencia.equals(((TipoFormaPagoDTO) other).codigo_equivalencia)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo_equivalencia != null) 
            ? (getClass().hashCode() + codigo_equivalencia.hashCode())
            : super.hashCode();
    }

	@Override
	public int compareTo(TipoFormaPagoDTO o) {
		return this.getDescripcion().compareTo(o.getDescripcion());
	}

}
