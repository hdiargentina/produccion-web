package ar.com.hdi.productores.services.remote.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import ar.com.hdi.productores.domain.FacturaDTO;
import ar.com.hdi.productores.domain.MayorAuxiliarDTO;
import ar.com.hdi.productores.domain.RetencionDTO;
import ar.com.hdi.productores.domain.SaldoDTO;
import ar.com.hdi.productores.domain.list.FacturaList;
import ar.com.hdi.productores.domain.list.RetencionList;
import ar.com.hdi.productores.services.RESTService;
import ar.com.hdi.productores.services.remote.CuentaCorrienteService;

@Stateless
public class CuentaCorrienteServiceImpl extends RESTService implements CuentaCorrienteService, Serializable {
	private static final long serialVersionUID = 1L;

	@Override
	public SaldoDTO obtenerSaldo(String empresa, String sucursal,MayorAuxiliarDTO mayorAuxiliar) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("empresa", empresa);
		params.put("sucursal", sucursal);
		params.put("codigoMayorAuxiliar", mayorAuxiliar.getCodigo().toString());
		params.put("numeroMayorAuxiliar", mayorAuxiliar.getNumero().toString());
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(SaldoDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			SaldoDTO saldo = (SaldoDTO)jaxbUnmarshaller.unmarshal(this.getRestService("cuentacorriente/listaSaldoPendiente", params));
			return saldo;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<FacturaDTO> obtenerFacturasPendientes(String empresa, String sucursal, MayorAuxiliarDTO mayorAuxiliar) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("empresa", empresa);
		params.put("sucursal", sucursal);
		params.put("codigoMayorAuxiliar", mayorAuxiliar.getCodigo().toString());
		params.put("numeroMayorAuxiliar", mayorAuxiliar.getNumero().toString());
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(FacturaList.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			FacturaList facturas = (FacturaList)jaxbUnmarshaller.unmarshal(this.getRestService("cuentacorriente/listaFacturasPendientes", params));
			return facturas.getFacturas();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<RetencionDTO> obtenerRetenciones(String empresa, String sucursal, MayorAuxiliarDTO mayorAuxiliar, Date fechaDesde, Date fechaHasta) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("empresa", empresa);
		params.put("sucursal", sucursal);
		params.put("codigoMayorAuxiliar", mayorAuxiliar.getCodigo().toString());
		params.put("numeroMayorAuxiliar", mayorAuxiliar.getNumero().toString());
		params.put("fechaDesde", dateToString(fechaDesde));
		params.put("fechaHasta", dateToString(fechaHasta));
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(RetencionList.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			RetencionList retenciones = (RetencionList)jaxbUnmarshaller.unmarshal(this.getRestService("cuentacorriente/listaRetenciones", params));
			return retenciones.getRetenciones();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
