package ar.com.hdi.productores.domain.enume;

import java.io.Serializable;

public enum TipoRama implements Serializable {
	AUTO("A"),
	HOGAR("H"),
	ACCIDENTESPERSONALES("V"),
	INTEGRALDECONSORCIO("C"),
	TRANSPORTESDEMERCADERIAS("M"),
	OTROS("O"),
	INTEGRALDECOMERCIO("R"),
	TRANSPORTESCASCOS("T"),
	NULL("");

	public static TipoRama fromString(String codigo) {
		for (TipoRama rama : TipoRama.values()) {
			if (rama.codigo.equals(codigo)) {
				return rama;
			}
		}
		return NULL;
	}

	private String codigo;

	private TipoRama(String name) {
		this.codigo = name;
	}
	
	public String getCodigo() {
		return codigo;
	}
}
