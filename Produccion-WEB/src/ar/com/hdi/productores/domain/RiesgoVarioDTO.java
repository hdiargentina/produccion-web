package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "ubicacion" )
public class RiesgoVarioDTO extends BienAseguradoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String riesgo;
	private String producto;
	private String articuloTarifa;
	private BigDecimal sumaTotal;
	private BigDecimal zona;
	private String direccion;
	private String provincia;
	private String localidad;
	private String tipoVivienda;
	private String acreedor;
	private String ocupacion;
	private BigDecimal codigoPostal;
	private List<CaracteristicaDTO> caracteristicas;
	private List<MejoraDTO> mejoras;
	private List<CoberturaDTO> coberturas;
	private List<String> Clausulas;
	
	public String getRiesgo() {
		return riesgo;
	}
	@XmlElement( name = "riesgo" )
	public void setRiesgo(String riesgo) {
		this.riesgo = riesgo;
	}
	public String getProducto() {
		return producto;
	}
	@XmlElement( name = "producto" )
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getArticuloTarifa() {
		return articuloTarifa;
	}
	@XmlElement( name = "artTarifa" )
	public void setArticuloTarifa(String articuloTarifa) {
		this.articuloTarifa = articuloTarifa;
	}
	public BigDecimal getSumaTotal() {
		return sumaTotal;
	}
	@XmlElement( name = "sumaTotal" )
	public void setSumaTotal(BigDecimal sumaTotal) {
		this.sumaTotal = sumaTotal;
	}
	public BigDecimal getZona() {
		return zona;
	}
	@XmlElement( name = "zona" )
	public void setZona(BigDecimal zona) {
		this.zona = zona;
	}
	public String getDireccion() {
		return direccion;
	}
	@XmlElement( name = "direccion" )
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getProvincia() {
		return provincia;
	}
	@XmlElement( name = "provincia" )
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getLocalidad() {
		return localidad;
	}
	@XmlElement( name = "localidad" )
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public BigDecimal getCodigoPostal() {
		return codigoPostal;
	}
	@XmlElement( name = "codPostal" )
	public void setCodigoPostal(BigDecimal codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public List<CaracteristicaDTO> getCaracteristicas() {
		return caracteristicas;
	}
	@XmlElementWrapper( name = "caracteristicas" )
	@XmlElement( name = "caracteristica" )
	public void setCaracteristicas(List<CaracteristicaDTO> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}
	public List<MejoraDTO> getMejoras() {
		return mejoras;
	}
	@XmlElementWrapper( name = "mejoras" )
	@XmlElement( name = "mejora" )
	public void setMejoras(List<MejoraDTO> mejoras) {
		this.mejoras = mejoras;
	}
	public List<CoberturaDTO> getCoberturas() {
		return coberturas;
	}
	@XmlElementWrapper( name = "coberturas" )
	@XmlElement( name = "cobertura" )
	public void setCoberturas(List<CoberturaDTO> coberturas) {
		this.coberturas = coberturas;
	}
	public List<String> getClausulas() {
		return Clausulas;
	}
	@XmlElementWrapper( name = "clausulas" )
	@XmlElement( name = "clausula" )
	public void setClausulas(List<String> clausulas) {
		Clausulas = clausulas;
	}
	
	public String getTipoVivienda() {
		return tipoVivienda;
	}
	@XmlElement ( name = "tipoVivienda" )
	public void setTipoVivienda(String tipoVivienda) {
		this.tipoVivienda = tipoVivienda;
	}
	public String getAcreedor() {
		return acreedor;
	}
	@XmlElement ( name = "acreedor" )
	public void setAcreedor(String acreedor) {
		this.acreedor = acreedor;
	}
	public String getOcupacion() {
		return ocupacion;
	}
	@XmlElement ( name = "ocupacion" )
	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
	public String getClausulasString() {
		String clausulas = "";
		int i = 0;
		for (String clausula : this.getClausulas()) {
			if (i == 0) {
				clausulas = clausulas + clausula;
			} else {
				clausulas = clausulas + " - " + clausula;
			}
			i++;
		}
		return clausulas;
	}
}
