package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class ImporteDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private RamaDTO rama;
	private BigDecimal prima;
	private BigDecimal porRecargoFinanciero;
	private BigDecimal impRecargoFinanciero;
	private BigDecimal extraPrimaFija;
	private BigDecimal subtotal;
	private BigDecimal selladosRiesgo;
	private BigDecimal selladosEmpresa;
	private BigDecimal porImpuestosInternos;
	private BigDecimal impImpuestosInternos;
	private BigDecimal porServiciosSociales;
	private BigDecimal impServiciosSociales;
	private BigDecimal porTasaSSN;
	private BigDecimal impTasaSSN;
	private BigDecimal porIva;
	private BigDecimal impIva;
	private BigDecimal porIvaRnI;
	private BigDecimal impIvaRnI;
	private BigDecimal porPercepcionIva;
	private BigDecimal impPercepcionIva;
	private BigDecimal compPremio5;
	private BigDecimal compPremio6;
	private BigDecimal impIngresosBrutosRiesgo;
	private BigDecimal impIngresosBrutosEmpresa;
	private BigDecimal impPremioConfirmado;
	private BigDecimal impPremio;
	private BigDecimal premio;
	private BigDecimal sumatoriaImpuestos;
	public RamaDTO getRama() {
		return rama;
	}
	public void setRama(RamaDTO rama) {
		this.rama = rama;
	}
	public BigDecimal getPrima() {
		return prima;
	}
	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}
	public BigDecimal getPorRecargoFinanciero() {
		return porRecargoFinanciero;
	}
	public void setPorRecargoFinanciero(BigDecimal porRecargoFinanciero) {
		this.porRecargoFinanciero = porRecargoFinanciero;
	}
	public BigDecimal getImpRecargoFinanciero() {
		return impRecargoFinanciero;
	}
	public void setImpRecargoFinanciero(BigDecimal impRecargoFinanciero) {
		this.impRecargoFinanciero = impRecargoFinanciero;
	}
	public BigDecimal getExtraPrimaFija() {
		return extraPrimaFija;
	}
	public void setExtraPrimaFija(BigDecimal extraPrimaFija) {
		this.extraPrimaFija = extraPrimaFija;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getSelladosRiesgo() {
		return selladosRiesgo;
	}
	public void setSelladosRiesgo(BigDecimal selladosRiesgo) {
		this.selladosRiesgo = selladosRiesgo;
	}
	public BigDecimal getSelladosEmpresa() {
		return selladosEmpresa;
	}
	public void setSelladosEmpresa(BigDecimal selladosEmpresa) {
		this.selladosEmpresa = selladosEmpresa;
	}
	public BigDecimal getPorImpuestosInternos() {
		return porImpuestosInternos;
	}
	public void setPorImpuestosInternos(BigDecimal porImpuestosInternos) {
		this.porImpuestosInternos = porImpuestosInternos;
	}
	public BigDecimal getImpImpuestosInternos() {
		return impImpuestosInternos;
	}
	public void setImpImpuestosInternos(BigDecimal impImpuestosInternos) {
		this.impImpuestosInternos = impImpuestosInternos;
	}
	public BigDecimal getPorServiciosSociales() {
		return porServiciosSociales;
	}
	public void setPorServiciosSociales(BigDecimal porServiciosSociales) {
		this.porServiciosSociales = porServiciosSociales;
	}
	public BigDecimal getImpServiciosSociales() {
		return impServiciosSociales;
	}
	public void setImpServiciosSociales(BigDecimal impServiciosSociales) {
		this.impServiciosSociales = impServiciosSociales;
	}
	public BigDecimal getPorTasaSSN() {
		return porTasaSSN;
	}
	public void setPorTasaSSN(BigDecimal porTasaSSN) {
		this.porTasaSSN = porTasaSSN;
	}
	public BigDecimal getImpTasaSSN() {
		return impTasaSSN;
	}
	public void setImpTasaSSN(BigDecimal impTasaSSN) {
		this.impTasaSSN = impTasaSSN;
	}
	public BigDecimal getPorIva() {
		return porIva;
	}
	public void setPorIva(BigDecimal porIva) {
		this.porIva = porIva;
	}
	public BigDecimal getImpIva() {
		return impIva;
	}
	public void setImpIva(BigDecimal impIva) {
		this.impIva = impIva;
	}
	public BigDecimal getPorIvaRnI() {
		return porIvaRnI;
	}
	public void setPorIvaRnI(BigDecimal porIvaRnI) {
		this.porIvaRnI = porIvaRnI;
	}
	public BigDecimal getImpIvaRnI() {
		return impIvaRnI;
	}
	public void setImpIvaRnI(BigDecimal impIvaRnI) {
		this.impIvaRnI = impIvaRnI;
	}
	public BigDecimal getPorPercepcionIva() {
		return porPercepcionIva;
	}
	public void setPorPercepcionIva(BigDecimal porPercepcionIva) {
		this.porPercepcionIva = porPercepcionIva;
	}
	public BigDecimal getImpPercepcionIva() {
		return impPercepcionIva;
	}
	public void setImpPercepcionIva(BigDecimal impPercepcionIva) {
		this.impPercepcionIva = impPercepcionIva;
	}
	public BigDecimal getCompPremio5() {
		return compPremio5;
	}
	public void setCompPremio5(BigDecimal compPremio5) {
		this.compPremio5 = compPremio5;
	}
	public BigDecimal getCompPremio6() {
		return compPremio6;
	}
	public void setCompPremio6(BigDecimal compPremio6) {
		this.compPremio6 = compPremio6;
	}
	public BigDecimal getImpIngresosBrutosRiesgo() {
		return impIngresosBrutosRiesgo;
	}
	public void setImpIngresosBrutosRiesgo(BigDecimal impIngresosBrutosRiesgo) {
		this.impIngresosBrutosRiesgo = impIngresosBrutosRiesgo;
	}
	public BigDecimal getImpIngresosBrutosEmpresa() {
		return impIngresosBrutosEmpresa;
	}
	public void setImpIngresosBrutosEmpresa(BigDecimal impIngresosBrutosEmpresa) {
		this.impIngresosBrutosEmpresa = impIngresosBrutosEmpresa;
	}
	public BigDecimal getImpPremioConfirmado() {
		return impPremioConfirmado;
	}
	public void setImpPremioConfirmado(BigDecimal impPremioConfirmado) {
		this.impPremioConfirmado = impPremioConfirmado;
	}
	public BigDecimal getImpPremio() {
		return impPremio;
	}
	public void setImpPremio(BigDecimal impPremio) {
		this.impPremio = impPremio;
	}
	public BigDecimal getPremio() {
		return premio;
	}
	public void setPremio(BigDecimal premio) {
		this.premio = premio;
	}
	public BigDecimal getSumatoriaImpuestos() {
		return sumatoriaImpuestos;
	}
	public void setSumatoriaImpuestos(BigDecimal sumatoriaImpuestos) {
		this.sumatoriaImpuestos = sumatoriaImpuestos;
	}
}
