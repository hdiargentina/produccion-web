package ar.com.hdi.productores.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import ar.com.hdi.utils.HDIProperties;

public abstract class RESTService implements Serializable {
	private static final long serialVersionUID = 1L;
	transient private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	transient private final SimpleDateFormat dateFormatNumerico = new SimpleDateFormat("yyyyMMdd");
	transient private final SimpleDateFormat horaFormat = new SimpleDateFormat("Hmm");

	private String armarUrl (String serviceName, Map<String, String> params){
		String url = HDIProperties.getPropiedad("config.properties", "urlServicios") +serviceName+ "?";
		
		
		String parametros = "";
		for (Map.Entry<String, String> entry : params.entrySet()) {
			parametros = parametros + entry.getKey() + "=" + entry.getValue() + "&";
		}
		url = url + parametros;
		System.out.println(url);
		return url;
	}
	
	public InputStream getRestService(String serviceName, Map<String, String> params){
		try {
			URL url = new URL(this.armarUrl(serviceName, params));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/xml");
			conn.setDoOutput(true);
			if (conn.getResponseCode() != 200) {
				System.out.println("URL PROBLEMA " + url.toString());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR ,conn.getHeaderField("X-Message"), conn.getHeaderField("X-LongMessage")));
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			conn.getInputStream();
			return conn.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String dateToString (Date fecha) {
		return new SimpleDateFormat("yyyy-MM-dd").format(fecha);
	}
	
	public String dateToStringNumerico (Date fecha) {
		synchronized (dateFormatNumerico) {
			return dateFormatNumerico.format(fecha);
		}
	}
	
	public String horaToString ( Date fecha ) {
		synchronized (horaFormat) {
			return horaFormat.format(fecha);
		}
	}
	
	public <T> Map<String, T> listToMap(List<T> list) {
	   Map<String, T> map = new HashMap<String, T>();
	   for (T el : list) {
	       map.put(el.toString(), el);
	   }   
	   return map;
	}
}
