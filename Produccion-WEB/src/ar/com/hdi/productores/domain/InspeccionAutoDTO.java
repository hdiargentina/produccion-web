package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.util.Date;

public class InspeccionAutoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String tipo;
	private String centroTecnored;
	private String nombreContacto;
	private String telefonoContacto;
	private String domicilioContacto;
	private String numero;
	private String email;
	private String observaciones;
	private Date fecha;
	private Date horaDesde;
	private Date horaHasta;
	public String getTipo() {
		if (this.tipo == null)
			this.tipo = "";
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCentroTecnored() {
		return centroTecnored;
	}
	public void setCentroTecnored(String centroTecnored) {
		this.centroTecnored = centroTecnored;
	}
	public String getNombreContacto() {
		return nombreContacto;
	}
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	public String getTelefonoContacto() {
		return telefonoContacto;
	}
	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}
	public String getDomicilioContacto() {
		return domicilioContacto;
	}
	public void setDomicilioContacto(String domicilioContacto) {
		this.domicilioContacto = domicilioContacto;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Date getFecha() {
		if (this.fecha == null)
			this.fecha = new Date();
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Date getHoraDesde() {
		return horaDesde;
	}
	public void setHoraDesde(Date horaDesde) {
		this.horaDesde = horaDesde;
	}
	public Date getHoraHasta() {
		return horaHasta;
	}
	public void setHoraHasta(Date horaHasta) {
		this.horaHasta = horaHasta;
	}
}
