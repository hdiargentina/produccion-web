package ar.com.hdi.productores.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "caracteristica" )
public class CaracteristicaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String detalle;
	private String contiene;
	public String getDetalle() {
		return detalle;
	}
	@XmlElement( name = "detalle" )
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public String getContiene() {
		return contiene;
	}
	@XmlElement( name = "contiene" )
	public void setContiene(String contiene) {
		this.contiene = contiene;
	}
	
	

}
