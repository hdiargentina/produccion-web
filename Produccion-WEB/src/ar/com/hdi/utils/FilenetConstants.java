package ar.com.hdi.utils;

public class FilenetConstants {
	//Propiedades generales
		public final static String FILENET_PROPERTIES = "filenet.properties";
		public final static String CERTIFICADO = "CertificadoRetencion";
		public final static String CPOLIZA = "PolizaElectronica";
		public final static String CCUOTA = "Cuota";
		public final static String LIBROS_RUBRICADOS = "LibrosRubricados";
		public final static String PREDENUNCIAS = "PreDenuncias";
		public final static String PROCESED_FOLDER_NAME = "Procesados";
		public final static String DOCUMENT_TITLE = "DocumentTitle";
		
		//Propiedades Poliza
		
		public final static String ARTICULO = "Articulo";
		public final static String SUPERPOLIZA = "Superpoliza";
		public final static String SUP_SUPERPOLIZA = "SuplementoSuperpoliza";
		public final static String RAMA = "Rama";
		public final static String POLIZA = "Poliza";
		public final static String SUP_POLIZA = "SuplementoPoliza";
		
		//Propiedades Cuota y Libros Rubricados
		
		public final static String CODIGOEMPRESA = "CodigoEmpresa";
		public final static String CODIGOSUCURSAL = "CodigoSucursal";
		public final static String NIVELLOGEO = "NivelLogeo";
		public final static String CODIGOLOGEO = "CodigoLogeo";
		public final static String FECHAGENERACION = "FechaGeneracion";
		public final static String NIVELINFORMACION = "NivelInformacion";
		public final static String CODIGOPRODUCTOR = "CodigoProductor";
		
//		Solo Libros Rubricados
		
		public final static String NUMEROSOLICITUD = "NumeroSolicitud";
		
//		Solo para Predenuncias
		public final static String NUMEROPREDENUNCIA ="NumeroPredenuncia";
}
