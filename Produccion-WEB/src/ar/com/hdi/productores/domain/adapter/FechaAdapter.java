package ar.com.hdi.productores.domain.adapter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.openjpa.lib.util.ParseException;

public class FechaAdapter extends XmlAdapter<String, Date> implements Serializable {
	private static final long serialVersionUID = 1L;
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public String marshal(Date fecha) throws Exception {
		// TODO Auto-generated method stub
		synchronized (dateFormat) {
			return dateFormat.format(fecha);
		}
	}

	@Override
	public Date unmarshal(String strFecha) throws Exception {
		// TODO Auto-generated method stub
		synchronized (dateFormat) {
			Date fecha = null;
			try {
				fecha = dateFormat.parse(strFecha);
			} catch (ParseException ex) {
				ex.printStackTrace();
			}
			return fecha;
		}
	}

}
  