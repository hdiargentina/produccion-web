package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;
import java.math.BigDecimal;

public class NacionalidadDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long codigo;
	private String descripcion;
	private BigDecimal oredn;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public BigDecimal getOredn() {
		return oredn;
	}
	public void setOredn(BigDecimal oredn) {
		this.oredn = oredn;
	}
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((NacionalidadDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }
	
	
}
