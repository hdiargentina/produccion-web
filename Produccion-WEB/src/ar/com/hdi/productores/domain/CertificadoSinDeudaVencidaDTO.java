package ar.com.hdi.productores.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement ( name = "CertificadoSinDeuda" )
public class CertificadoSinDeudaVencidaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String texto;

	public String getTexto() {
		return texto;
	}
	@XmlElement( name = "Texto" )
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
}
