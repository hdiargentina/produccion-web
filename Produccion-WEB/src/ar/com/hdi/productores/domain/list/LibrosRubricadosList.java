package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import javax.xml.bind.annotation.XmlElement;

import ar.com.hdi.productores.domain.DatosQuincenaDTO;

@XmlRootElement( name = "librosRubricados" )
public class LibrosRubricadosList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	List<DatosQuincenaDTO> listaDatosQuincena;
	@XmlElement( name = "registro")
	public List<DatosQuincenaDTO> getListaDatosQuincena() {
		return listaDatosQuincena;
	}

	public void setListaDatosQuincena(List<DatosQuincenaDTO> listaDatosQuincena) {
		this.listaDatosQuincena = listaDatosQuincena;
	}
	
}
