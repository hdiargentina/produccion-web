package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;
import java.math.BigDecimal;

public class EmpresaTarjetaDTO implements Serializable, Comparable<EmpresaTarjetaDTO> {

	private static final long serialVersionUID = 1L;
	
	private Long codigo;
	private String descripcion;
	private String codigoEquivalente;
	private BigDecimal cantidadDigitos;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodigoEquivalente() {
		return codigoEquivalente;
	}
	public void setCodigoEquivalente(String codigoEquivalente) {
		this.codigoEquivalente = codigoEquivalente;
	}
	public BigDecimal getCantidadDigitos() {
		return cantidadDigitos;
	}
	public void setCantidadDigitos(BigDecimal cantidadDigitos) {
		this.cantidadDigitos = cantidadDigitos;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((EmpresaTarjetaDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }
	@Override
	public int compareTo(EmpresaTarjetaDTO o) {
		return this.getDescripcion().compareTo(o.getDescripcion());
	}
}
