package ar.com.hdi.productores.services.remote.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import ar.com.hdi.productores.domain.FechaSistemaDTO;
import ar.com.hdi.productores.services.RESTService;
import ar.com.hdi.productores.services.remote.SistemaService;

@ManagedBean(name="sistemaService")
@SessionScoped
public class SistemaServiceImpl extends RESTService implements SistemaService, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public FechaSistemaDTO getFechasSistema(String empresa, String sucursal) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("empresa", empresa);
		params.put("sucursal", sucursal);
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(FechaSistemaDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			FechaSistemaDTO fechaSistema = (FechaSistemaDTO)jaxbUnmarshaller.unmarshal(this.getRestService("valoressistema/fechasSistema", params));
			return fechaSistema;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
