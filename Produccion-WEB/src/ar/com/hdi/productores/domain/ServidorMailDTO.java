package ar.com.hdi.productores.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "servidorMail" )
public class ServidorMailDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String servidor;
	private String usuario;
	private String password;
	public String getServidor() {
		return servidor;
	}
	@XmlElement( name = "servidor")
	public void setServidor(String servidor) {
		this.servidor = servidor;
	}
	public String getUsuario() {
		return usuario;
	}
	@XmlElement( name = "usuario")
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	@XmlElement( name = "password")
	public void setPassword(String password) {
		this.password = password;
	}

}
