package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;

public class ModeloDTO implements Serializable, Comparable<ModeloDTO> {
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String codigo_equivalencia;
	private String descripcion;
	
	public ModeloDTO () { }
	
	public ModeloDTO(ModeloDTO modelo) {
		this.codigo = modelo.getCodigo();
		this.codigo_equivalencia = modelo.getCodigo_equivalencia();
		this.descripcion = modelo.getDescripcion();
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodigo_equivalencia() {
		return codigo_equivalencia;
	}
	public void setCodigo_equivalencia(String codigo_equivalencia) {
		this.codigo_equivalencia = codigo_equivalencia;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo_equivalencia != null)
            ? codigo_equivalencia.equals(((ModeloDTO) other).codigo_equivalencia)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo_equivalencia != null) 
            ? (getClass().hashCode() + codigo_equivalencia.hashCode())
            : super.hashCode();
    }

	@Override
	public int compareTo(ModeloDTO o) {
		return this.getDescripcion().compareTo(o.getDescripcion());
	}
	

}
