package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.FacturaDTO;

@XmlRootElement( name = "facturas" )
public class FacturaList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<FacturaDTO> facturas;
	private BigDecimal cantidad;
	public List<FacturaDTO> getFacturas() {
		return facturas;
	}
	@XmlElement( name = "factura" )
	public void setFacturas(List<FacturaDTO> facturas) {
		this.facturas = facturas;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	@XmlElement( name = "cantidad" )
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public void add (FacturaDTO factura) {
		if (this.facturas == null) {
			this.facturas = new ArrayList<FacturaDTO>();
		}
		this.facturas.add(factura);
	}

}
