package ar.com.hdi.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.security.auth.Subject;

import org.apache.commons.io.FileUtils;

import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Connection;
import com.filenet.api.core.Domain;
import com.filenet.api.core.EntireNetwork;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;

public class FilenetUtilities {
	public static final Connection Login(String usuario, String password, String url) {
		
        Connection p8Connection = Factory.Connection.getConnection(url);
        Subject sub = UserContext.createSubject(p8Connection, usuario, password, "FileNetP8WSI");
        UserContext uc = UserContext.get();
		uc.pushSubject(sub);
        
        return p8Connection;
	}	
	public static final  ObjectStore getObjectStore(Connection p8connection, String objectStoreName) {
	
		EntireNetwork entireNetwork = Factory.EntireNetwork.fetchInstance(p8connection, null);
		Domain domain = entireNetwork.get_LocalDomain();
		
		return Factory.ObjectStore.fetchInstance(domain, objectStoreName,null);
	}
	
	public static final void procesedFolder(String root) {
		
		File f = new File(root + "/" + FilenetConstants.PROCESED_FOLDER_NAME);
			
		if(!(f.exists() && f.isDirectory()))
			f.mkdir();
		else{
			System.out.println("La carpeta ya existe");	
//			System.out.println(System.getProperty("log4j.configurationFile"));
		}
	}
	
	public static String createZipFiles(List<String> files, String className, String pdfName) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("Test String");
			
			String fileName = HDIProperties.getPropiedad("config.properties", "download_location") + pdfName;
			File f = new File(fileName);
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(f));
			for (String file : files) {
				String pdfFile = createPdf(file, className);
				ZipEntry e = new ZipEntry(file);
				out.putNextEntry(e);
				FileInputStream fis = new FileInputStream(pdfFile);
				int leer;
				byte[] buffer = new byte[1024];
				while (0 < (leer = fis.read(buffer))) {
					out.write(buffer, 0, leer);
				}
				fis.close();
				out.closeEntry();
				File filePdf = new File(pdfFile);
				filePdf.delete();
			}
			out.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return "";
	}
	
	public static String createPdf(String nombreArchivo, String className) {
		try {
			String fileName = HDIProperties.getPropiedad("config.properties", "download_location") + nombreArchivo;
			if (!archivoExiste(fileName)) {
				FileUtils.copyInputStreamToFile(generarInputStreamArchivoFilenet(nombreArchivo, className), 
						new File(fileName));
			}
			return fileName;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean archivoExiste(String filename) {
		File file = new File(filename);
		return file.exists();
	}
	
	private static InputStream generarInputStreamArchivoFilenet(String fileName, String className) {
		Connection p8Connection = FilenetUtilities.Login(HDIProperties.getPropiedad("filenet.properties", "username"), 
				HDIProperties.getPropiedad("filenet.properties", "password"), HDIProperties.getPropiedad("filenet.properties", "url"));  
		ObjectStore os = FilenetUtilities.getObjectStore(p8Connection, HDIProperties.getPropiedad("filenet.properties", "repositorioFileNet"));
		Iterator it = obtenerIteradorArchivo(os, fileName);
		
		Id id = null;
	    while(it.hasNext()) {
	    	com.filenet.api.core.Document doc = (com.filenet.api.core.Document) it.next();
			id = new Id(doc.get_Id().toString());
		}
	    com.filenet.api.core.Document doc =  Factory.Document.getInstance(os, className, id);
	    doc.refresh(new String[] {PropertyNames.CONTENT_ELEMENTS});
	    return doc.accessContentStream(0);
	}
	
	private static Iterator obtenerIteradorArchivo(ObjectStore os, String nombreArchivo) {
	    String mySQLString = "SELECT ClassDescription, Id FROM Document WHERE DocumentTitle = '"+nombreArchivo+"'";   //Consulta generica para obtener cualquier documento en base al titulo
		SearchSQL sqlObject = new SearchSQL();
		sqlObject.setQueryString(mySQLString); // creacion del objeto SQL
		SearchScope searchScope = new SearchScope(os); //definicion del alcance de la busqueda
		IndependentObjectSet ios = searchScope.fetchObjects(sqlObject, 0, null, Boolean.valueOf(false)); //obtencion de registros
	    
		return ios.iterator();
	}
}
