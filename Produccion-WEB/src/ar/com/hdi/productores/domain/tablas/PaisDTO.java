package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;


public class PaisDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long codigo;
	private String descripcion;
	
	public Long getCodigo() {
		if(this.codigo == null)
			return new Long(0);
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((PaisDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }
}
