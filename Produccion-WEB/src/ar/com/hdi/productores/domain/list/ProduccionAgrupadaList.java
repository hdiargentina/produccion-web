package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "produccionDiaria" )
public class ProduccionAgrupadaList implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private List<AgrupadoDTO> agrupamiento;

	public List<AgrupadoDTO> getAgrupamiento() {
		return agrupamiento;
	}
	@XmlElement( name = "agrupamiento" )
	public void setAgrupamiento(List<AgrupadoDTO> agrupamiento) {
		this.agrupamiento = agrupamiento;
	}
	
}
