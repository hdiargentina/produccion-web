package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;
import ar.com.hdi.productores.domain.tablas.SexoDTO;

public class PersonaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	protected String nombre;
	protected Date fechaNacimiento;
	protected String numeroDocumento;
	protected String condicionFiscal;
	protected String nacionalidad;
	protected String cuit;
	protected BigDecimal codigoPostal;
	protected String domicilio;
	protected String telefonoParticular;
	protected String telefonoLaboral;
	protected String celular;
	protected String fax;
	protected List<MailDTO> correos;
	protected String estadoCivil;
	protected SexoDTO sexo;
	protected String email;
	protected String tipoPersona;
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "nombreAsegurado" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	@XmlElement( name = "fechaNacimiento" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	@XmlElement( name = "nroDocumento")
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getCondicionFiscal() {
		return condicionFiscal;
	}
	@XmlElement( name = "condiFiscal")
	public void setCondicionFiscal(String condicionFiscal) {
		this.condicionFiscal = condicionFiscal;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	@XmlElement( name = "nacionalidad")
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public String getCuit() {
		return cuit;
	}
	@XmlElement( name = "cuit")
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public BigDecimal getCodigoPostal() {
		return codigoPostal;
	}
	@XmlElement( name = "codigoPostal")
	public void setCodigoPostal(BigDecimal codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getDomicilio() {
		return domicilio;
	}
	@XmlElement( name = "domicilio")
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getTelefonoParticular() {
		return telefonoParticular;
	}
	@XmlElement( name = "telParticular")
	public void setTelefonoParticular(String telefonoParticular) {
		this.telefonoParticular = telefonoParticular;
	}
	public String getTelefonoLaboral() {
		return telefonoLaboral;
	}
	@XmlElement( name = "telLaboral")
	public void setTelefonoLaboral(String telefonoLaboral) {
		this.telefonoLaboral = telefonoLaboral;
	}
	public String getCelular() {
		return celular;
	}
	@XmlElement( name = "telCelular")
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getFax() {
		return fax;
	}
	@XmlElement( name = "nroFax")
	public void setFax(String fax) {
		this.fax = fax;
	}
	public List<MailDTO> getCorreos() {
		return correos;
	}
	@XmlElementWrapper( name = "correos" )
	@XmlElement( name = "correo" )
	public void setCorreos(List<MailDTO> correos) {
		this.correos = correos;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public SexoDTO getSexo() {
		return sexo;
	}
	public void setSexo(SexoDTO sexo) {
		this.sexo = sexo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTipoPersona() {
		if(tipoPersona.equals("J"))
			tipoPersona = "Juridica";
		else
			tipoPersona = "Fisica";
		return tipoPersona;
	}
	@XmlElement(name = "tipoPersona")
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	
}
