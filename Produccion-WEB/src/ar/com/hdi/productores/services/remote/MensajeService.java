package ar.com.hdi.productores.services.remote;

import java.util.List;

import javax.ejb.Local;

import ar.com.hdi.productores.domain.IntermediarioDTO;
import ar.com.hdi.productores.domain.MensajeDTO;

@Local
public interface MensajeService {
	public List<MensajeDTO> obtenerMensajes(String empresa, String sucursal, IntermediarioDTO intermediario, IntermediarioDTO vendedor);
	public void cambiiarEstado(String empresa, String sucursal, IntermediarioDTO intermediario, IntermediarioDTO vendedor, List<MensajeDTO> mensajes, String status);
}
