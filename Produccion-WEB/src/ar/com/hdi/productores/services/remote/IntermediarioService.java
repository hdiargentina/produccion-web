package ar.com.hdi.productores.services.remote;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import ar.com.hdi.productores.domain.DatosIntermediarioDTO;
import ar.com.hdi.productores.domain.IntermediarioDTO;
import ar.com.hdi.productores.domain.MayorAuxiliarDTO;

@Local
public interface IntermediarioService {
	public List<IntermediarioDTO> obtenerIntermediarios(String empresa, String sucursal, String cuit, String numeroAgencia, boolean usuarioInterno);
	public DatosIntermediarioDTO obtenerDatosIntermediario(String empresa, String sucursal, IntermediarioDTO intermediario, IntermediarioDTO vendedor);
	public List<IntermediarioDTO> obtenerIntermediariosDependientesList(String empresa, String sucursal, IntermediarioDTO intermediario);
	public Map<String, MayorAuxiliarDTO> obtenerMayoresAuxiliares(String empresa, String sucursal, IntermediarioDTO intermediario);
	public <T> Map<String, T> obtenerIntermediariosDependientes(String empresa, String sucursal, IntermediarioDTO intermediario);
}
