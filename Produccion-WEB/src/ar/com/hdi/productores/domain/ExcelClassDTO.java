package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ExcelClassDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<String> cabecera;
	private Object contenido;
	private Map<String, String> textosAuxiliares;
	public List<String> getCabecera() {
		return cabecera;
	}
	public void setCabecera(List<String> cabecera) {
		this.cabecera = cabecera;
	}
	public Object getContenido() {
		return contenido;
	}
	public void setContenido(Object contenido) {
		this.contenido = contenido;
	}
	public Map<String, String> getTextosAuxiliares() {
		return textosAuxiliares;
	}
	public void setTextosAuxiliares(Map<String, String> textosAuxiliares) {
		this.textosAuxiliares = textosAuxiliares;
	}
	
}
