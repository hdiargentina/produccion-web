package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ar.com.hdi.productores.domain.tablas.LocalidadDTO;
import ar.com.hdi.productores.domain.tablas.ProvinciaDTO;
import ar.com.hdi.productores.domain.tablas.TipoViviendaDTO;

public class HogarCotizacionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal componente;
	private ProvinciaDTO provincia;
	private LocalidadDTO localidad;
	private TipoViviendaDTO tipoVivienda;
	private List<CaracteristicaHogarDTO> caracteristicas;
	private String ubicacion;
	private InspeccionHogarDTO inspeccion;
	
	public HogarCotizacionDTO(){};
	
	public HogarCotizacionDTO(BigDecimal componente){
		this.componente = componente;
	}

	public BigDecimal getComponente() {
		return componente;
	}

	public void setComponente(BigDecimal componente) {
		this.componente = componente;
	}

	public ProvinciaDTO getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinciaDTO provincia) {
		this.provincia = provincia;
	}

	public LocalidadDTO getLocalidad() {
		return localidad;
	}

	public void setLocalidad(LocalidadDTO localidad) {
		this.localidad = localidad;
	}

	public TipoViviendaDTO getTipoVivienda() {
		if (this.tipoVivienda == null)
			this.tipoVivienda = new TipoViviendaDTO();
		return tipoVivienda;
	}

	public void setTipoVivienda(TipoViviendaDTO tipoVivienda) {
		this.tipoVivienda = tipoVivienda;
	}

	public List<CaracteristicaHogarDTO> getCaracteristicas() {
		if (this.caracteristicas == null)
			this.caracteristicas = new ArrayList<CaracteristicaHogarDTO>();
		return caracteristicas;
	}

	public void setCaracteristicas(List<CaracteristicaHogarDTO> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public InspeccionHogarDTO getInspeccion() {
		if (this.inspeccion == null)
			this.inspeccion = new InspeccionHogarDTO();
		return inspeccion;
	}

	public void setInspeccion(InspeccionHogarDTO inspeccion) {
		this.inspeccion = inspeccion;
	}
	
	public String caracteristicasToString() {
		String caracteristicas = "";
		for(CaracteristicaHogarDTO caracteristica: this.caracteristicas) {
			if(caracteristica.isTiene())
				caracteristicas += caracteristica.getDescripcion() + " ";
		}
		return caracteristicas;
	}
	
	

}
