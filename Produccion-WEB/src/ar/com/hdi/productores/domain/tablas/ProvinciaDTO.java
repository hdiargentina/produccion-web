package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;

public class ProvinciaDTO implements Serializable, Comparable<ProvinciaDTO> {
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String descripcion;
	
	public ProvinciaDTO() {super();}
	
	public ProvinciaDTO(ProvinciaDTO provincia) {
		super();
		this.codigo = provincia.getCodigo();
		this.descripcion = provincia.getDescripcion();
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((ProvinciaDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }

	@Override
	public int compareTo(ProvinciaDTO o) {
		return descripcion.compareTo(o.getDescripcion());
	}
	
}
