package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "retencion" )
public class RetencionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String tipoImpuesto;
	private String provincia;
	private BigDecimal mes;
	private BigDecimal anio;
	private BigDecimal baseImponible;
	private BigDecimal porcentaje;
	private BigDecimal importe;
	private BigDecimal numeroCertificado;
	private Date fecha;
	private String icono;
	private boolean tieneIcono;
	private boolean selected;
	public String getTipoImpuesto() {
		return tipoImpuesto;
	}
	@XmlElement( name = "tipoImpuesto" )
	public void setTipoImpuesto(String tipoImpuesto) {
		this.tipoImpuesto = tipoImpuesto;
	}
	public String getProvincia() {
		return provincia;
	}
	@XmlElement( name = "provincia" )
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public BigDecimal getMes() {
		return mes;
	}
	@XmlElement( name = "mes" )
	public void setMes(BigDecimal mes) {
		this.mes = mes;
	}
	public BigDecimal getAnio() {
		return anio;
	}
	@XmlElement( name = "anio" )
	public void setAnio(BigDecimal anio) {
		this.anio = anio;
	}
	public BigDecimal getBaseImponible() {
		return baseImponible;
	}
	@XmlElement( name = "baseImponible" )
	public void setBaseImponible(BigDecimal baseImponible) {
		this.baseImponible = baseImponible;
	}
	public BigDecimal getPorcentaje() {
		return porcentaje;
	}
	@XmlElement( name = "porcRetencion" )
	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	@XmlElement( name = "importeRetencion" )
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public BigDecimal getNumeroCertificado() {
		return numeroCertificado;
	}
	@XmlElement( name = "nroCertificado" )
	public void setNumeroCertificado(BigDecimal numeroCertificado) {
		this.numeroCertificado = numeroCertificado;
	}
	public Date getFecha() {
		return fecha;
	}
	@XmlElement( name = "fechaRetencion" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getIcono() {
		return icono;
	}
	@XmlElement( name = "iconoCertificado" )
	public void setIcono(String icono) {
		this.icono = icono;
	}
	
	public boolean isTieneIcono() {
		if(icono.contains("#"))
			return true;
		return tieneIcono;
	}
	public void setTieneIcono(boolean tieneIcono) {
		this.tieneIcono = tieneIcono;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
}
