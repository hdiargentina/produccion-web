package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;

public class TipoVehiculoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Long codigo;
	public String descripcion;
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((TipoVehiculoDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }
	
}
