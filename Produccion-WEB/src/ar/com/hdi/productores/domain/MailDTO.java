package ar.com.hdi.productores.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "correo" )
public class MailDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String tipo;
	private String direccion;
	public String getTipo() {
		return tipo;
	}
	@XmlElement( name = "tipoCorreo" )
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDireccion() {
		return direccion;
	}
	@XmlElement( name = "direccionCorreo" )
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	

}
