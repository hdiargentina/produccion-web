package ar.com.hdi.productores.services.remote.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import ar.com.hdi.productores.domain.IntermediarioDTO;
import ar.com.hdi.productores.indicadores.Indicador;
import ar.com.hdi.productores.indicadores.IndicadoresList;
import ar.com.hdi.productores.services.RESTService;
import ar.com.hdi.productores.services.remote.IndicadorService;

@Stateless
public class IndicadorServerImpl extends RESTService implements IndicadorService, Serializable {
	private static final long serialVersionUID = 1L;

	@Override
	public List<Indicador> obtenerIndicadores(String empresa, String sucursal, IntermediarioDTO intermediario, IntermediarioDTO vendedor) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("empresa", empresa);
		params.put("sucursal", sucursal);
		params.put("nivt", intermediario.getNivel().toString());
		params.put("nivc", intermediario.getCodigo().toString());
		params.put("nit1", vendedor.getNivel().toString());
		params.put("niv1", vendedor.getCodigo().toString());
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(IndicadoresList.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			IndicadoresList indicadores = (IndicadoresList)jaxbUnmarshaller.unmarshal(this.getRestService("indicadores/indicadores", params));
			List<Indicador> indicadorList = new ArrayList<Indicador>();
			if (indicadores.getCarteraVigenteIndicador() != null) {
				indicadorList.add(indicadores.getCarteraVigenteIndicador());
			}
			if (indicadores.getPolizaVigenteIndicador() != null) {
				indicadorList.add(indicadores.getPolizaVigenteIndicador());
			}
			if (indicadores.getSiniestroIndicador() != null) {
				indicadorList.add(indicadores.getSiniestroIndicador());
			}
			if (indicadores.getCotizacionIndicador() != null) {
				indicadorList.add(indicadores.getCotizacionIndicador());
			}
//			if (indicadores.getCobranzaIndicador() != null) {
//				indicadorList.add(indicadores.getCobranzaIndicador());
//			}
			return indicadorList;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
