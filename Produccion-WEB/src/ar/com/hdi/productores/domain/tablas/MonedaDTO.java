package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;

public class MonedaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String descripcion;
	private String descripcionAbreviada;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescripcionAbreviada() {
		return descripcionAbreviada;
	}
	public void setDescripcionAbreviada(String descripcionAbreviada) {
		this.descripcionAbreviada = descripcionAbreviada;
	}
	
	
	
}
