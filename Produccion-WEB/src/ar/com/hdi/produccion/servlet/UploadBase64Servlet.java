package ar.com.hdi.produccion.servlet;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.misc.BASE64Decoder;

@WebServlet(name = "uploadBase64Servlet",urlPatterns = { "/uploadBase64" })
@MultipartConfig
public class UploadBase64Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private String downloadLocation = "/hdiaplicaciones/data/cargaAux/";
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String imagenesBase64 = req.getParameter("imagenes");
		String nombres = req.getParameter("nombres");
		String[] imagenesBase64Array = imagenesBase64.split(",data");
		String[] nombresArray = nombres.split(",");
		int i = 0;
		for(String imageBase64 : imagenesBase64Array) {
			String[] base64Parts = imageBase64.split(",");
			File outputfile = new File(downloadLocation + File.separator + "Predenuncia_" + nombresArray[i]);
			BASE64Decoder decoder = new BASE64Decoder();
		    byte[] imgBytes = decoder.decodeBuffer(base64Parts[1]);
	    	BufferedImage bufImg = ImageIO.read(new ByteArrayInputStream(imgBytes));
	    	ImageIO.write(bufImg, "png", outputfile);
	    	i++;
		}
	}
}