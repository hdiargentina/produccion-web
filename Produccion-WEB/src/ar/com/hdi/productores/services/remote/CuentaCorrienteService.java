package ar.com.hdi.productores.services.remote;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import ar.com.hdi.productores.domain.FacturaDTO;
import ar.com.hdi.productores.domain.MayorAuxiliarDTO;
import ar.com.hdi.productores.domain.RetencionDTO;
import ar.com.hdi.productores.domain.SaldoDTO;

@Local
public interface CuentaCorrienteService {
	public SaldoDTO obtenerSaldo(String empresa, String sucursal, MayorAuxiliarDTO mayorAuxiliar);
	public List<FacturaDTO> obtenerFacturasPendientes(String empresa, String sucursal, MayorAuxiliarDTO mayorAuxiliar);
	public List<RetencionDTO> obtenerRetenciones(String empresa, String sucursal, MayorAuxiliarDTO mayorAuxiliar, Date fechaDesde, Date fechaHasta);
}
