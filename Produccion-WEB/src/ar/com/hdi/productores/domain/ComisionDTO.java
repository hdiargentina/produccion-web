package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "comision" )
public class ComisionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal nivelProductor;
	private BigDecimal codigoProductor;
	private BigDecimal porcentajeComision;
	private BigDecimal comision;
	private String nombreProductor;
	public BigDecimal getNivelProductor() {
		return nivelProductor;
	}
	@XmlElement( name = "nivel" )
	public void setNivelProductor(BigDecimal nivelProductor) {
		this.nivelProductor = nivelProductor;
	}
	public BigDecimal getCodigoProductor() {
		return codigoProductor;
	}
	@XmlElement( name = "codigo" )
	public void setCodigoProductor(BigDecimal codigoProductor) {
		this.codigoProductor = codigoProductor;
	}
	public BigDecimal getPorcentajeComision() {
		return porcentajeComision;
	}
	@XmlElement( name = "porcComision" )
	public void setPorcentajeComision(BigDecimal porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}
	public BigDecimal getComision() {
		return comision;
	}
	@XmlElement( name = "comision" )
	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}
	public String getNombreProductor() {
		return nombreProductor;
	}
	@XmlElement( name = "nombre" )
	public void setNombreProductor(String nombreProductor) {
		this.nombreProductor = nombreProductor;
	}
}
