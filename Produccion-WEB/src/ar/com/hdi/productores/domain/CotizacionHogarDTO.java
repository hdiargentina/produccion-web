package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.com.hdi.productores.domain.tablas.CoberturaPlanDTO;
import ar.com.hdi.productores.domain.tablas.LocalidadDTO;
import ar.com.hdi.productores.domain.tablas.PlanDTO;
import ar.com.hdi.productores.domain.tablas.ProvinciaDTO;

public class CotizacionHogarDTO extends CotizacionBienDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private PlanDTO plan;
	private HogarCotizacionDTO hogar;
	private List<CoberturaPlanDTO> coberturas;
	private ImporteDTO importes;
	private BigDecimal primaTecnica;
	private BigDecimal totalSumaAsegurada;
	private BigDecimal comision;
	private boolean requiereInspeccion;
	private boolean recotizarCerrado;
	
	public CotizacionHogarDTO(){}
	
	public CotizacionHogarDTO(ProvinciaDTO provincia, LocalidadDTO localidad, BigDecimal componente) {
		this.hogar = new HogarCotizacionDTO(componente);
		hogar.setProvincia(new ProvinciaDTO(provincia));
		hogar.setLocalidad(new LocalidadDTO(localidad));
	}
	
	public PlanDTO getPlan() {
		return plan;
	}

	public void setPlan(PlanDTO plan) {
		this.plan = plan;
	}

	public HogarCotizacionDTO getHogar() {
		return hogar;
	}

	public void setHogar(HogarCotizacionDTO hogar) {
		this.hogar = hogar;
	}

	public List<CoberturaPlanDTO> getCoberturas() {
		return coberturas;
	}

	public void setCoberturas(List<CoberturaPlanDTO> coberturas) {
		this.coberturas = coberturas;
	}

	public ImporteDTO getImportes() {
		return importes;
	}

	public void setImportes(ImporteDTO importes) {
		this.importes = importes;
	}

	public boolean isRequiereInspeccion() {
		return requiereInspeccion;
	}

	public void setRequiereInspeccion(boolean requiereInspeccion) {
		this.requiereInspeccion = requiereInspeccion;
	}

	public BigDecimal getPrimaTecnica() {
		this.primaTecnica = BigDecimal.ZERO;
		for (CoberturaPlanDTO cobertura : getCoberturas()) {
			if (cobertura.getPrima() != null)
				this.primaTecnica = this.primaTecnica.add(cobertura.getPrima());
		}
		return primaTecnica;
	}
	
	public ArrayList<Object[]> getFamiliasCoberturasSeleccionadas() {
		Map<BigDecimal, Object[]> familias = new HashMap<BigDecimal, Object[]>();
		for (CoberturaPlanDTO cobertura : this.getCoberturas()) {
			if (cobertura.isSeleccionada()) {
				Object [] object = new Object[2];
				object[0] = cobertura.getCodigoFamilia();
				object[1] = cobertura.getDescripcionFamilia();
				familias.put(cobertura.getCodigoFamilia(), object);
			}
		}
		List<BigDecimal> keys = new ArrayList<BigDecimal>();
		for (BigDecimal key : familias.keySet()) {
			keys.add(key);
		}
		Collections.sort(keys);
		ArrayList<Object[]> familiasOrdenadas = new ArrayList<Object[]>();
		for (BigDecimal key : keys) {
			familiasOrdenadas.add(familias.get(key));
		}
		return familiasOrdenadas;
	}
	
	public List<CoberturaPlanDTO> getCoberturasSeleccionadasByFamilia(Object codigoFamilia) {
		List<CoberturaPlanDTO> coberturas = new ArrayList<CoberturaPlanDTO>();
		for (CoberturaPlanDTO cobertura : this.getCoberturas()) {
			if (cobertura.isSeleccionada() && cobertura.getCodigoFamilia().equals(codigoFamilia)) {
				coberturas.add(cobertura);
			}
		}
		return coberturas;
	}

	public void setPrimaTecnica(BigDecimal primaTecnica) {
		this.primaTecnica = primaTecnica;
	}

	public BigDecimal getTotalSumaAsegurada() {
		return totalSumaAsegurada;
	}

	public void setTotalSumaAsegurada(BigDecimal totalSumaAsegurada) {
		this.totalSumaAsegurada = totalSumaAsegurada;
	}
	
	public BigDecimal getComision() {
		return comision;
	}

	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}

	public List<CoberturaPlanDTO> getCoberturasSeleccionadas() {
		List<CoberturaPlanDTO> coberturasSeleccionadas = new ArrayList<CoberturaPlanDTO>();
		for(CoberturaPlanDTO cobertura : this.getCoberturas()){
			if(cobertura.isSeleccionada() || cobertura.getBasicaOptativa().equals("B")) 
				coberturasSeleccionadas.add(cobertura);
		}
		return coberturasSeleccionadas;
	}

	public boolean isRecotizarCerrado() {
		return recotizarCerrado;
	}

	public void setRecotizarCerrado(boolean recotizarCerrado) {
		this.recotizarCerrado = recotizarCerrado;
	}
	
	
}
