package ar.com.hdi.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class HDIProperties {
	private static Properties properties = new Properties();
	
	public static String getPropiedad(String archivo, String key) {
		try {
			properties = new Properties();
			properties.load(new FileInputStream("/hdiaplicaciones/config/properties/" + archivo));
			return properties.get(key).toString();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
