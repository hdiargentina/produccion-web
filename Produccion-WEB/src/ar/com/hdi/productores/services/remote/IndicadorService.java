package ar.com.hdi.productores.services.remote;

import java.util.List;

import javax.ejb.Local; 

import ar.com.hdi.productores.domain.IntermediarioDTO;
import ar.com.hdi.productores.indicadores.Indicador;

@Local
public interface IndicadorService {
	public List<Indicador> obtenerIndicadores(String empresa, String sucursal, IntermediarioDTO intermediario, IntermediarioDTO vendedor);
}
