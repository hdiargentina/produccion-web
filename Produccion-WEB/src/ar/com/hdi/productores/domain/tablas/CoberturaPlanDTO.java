package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ar.com.hdi.productores.domain.ObjetoDTO;
import ar.com.hdi.productores.domain.PersonaCoberturaDTO;

public class CoberturaPlanDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long codigo;
	private String descripcion;
	private String descripcionLarga;
	private String tipo;
	private String basicaOptativa;
	private boolean seleccionada;
	private BigDecimal codigoFamilia;
	private String descripcionFamilia;
	private String codigoRiesgo;
	private String descripcionRiesgo;
	private BigDecimal coberturaReaseguro;
	private BigDecimal sumaAsegurada;
	private BigDecimal sumaAseguradaDefault;
	private BigDecimal sumaAseguradaMaxima;
	private BigDecimal sumaAseguradaMinima;
	private BigDecimal porcentajeSaRC;
	private String codigoRiesgoRef;
	private Long codigoCoberturaRef;
	private BigDecimal prima;
	private BigDecimal pormillajePrima;
	private List<ObjetoDTO> objetos;
	private List<PersonaCoberturaDTO> personas;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescripcionLarga() {
		return descripcionLarga;
	}
	public void setDescripcionLarga(String descripcionLarga) {
		this.descripcionLarga = descripcionLarga;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getBasicaOptativa() {
		return basicaOptativa;
	}
	public void setBasicaOptativa(String basicaOptativa) {
		this.basicaOptativa = basicaOptativa;
	}
	public boolean isSeleccionada() {
		return seleccionada;
	}
	public void setSeleccionada(boolean seleccionada) {
		if (this.codigoRiesgoRef !=null && seleccionada == false && this.codigoRiesgoRef.equals("") && this.sumaAseguradaDefault.compareTo(BigDecimal.ZERO) == 0) {
			this.sumaAsegurada = BigDecimal.ZERO;
			this.prima = null;
		}
		this.seleccionada = seleccionada;
	}
	public BigDecimal getCodigoFamilia() {
		return codigoFamilia;
	}
	public void setCodigoFamilia(BigDecimal codigoFamilia) {
		this.codigoFamilia = codigoFamilia;
	}
	public String getDescripcionFamilia() {
		return descripcionFamilia;
	}
	public void setDescripcionFamilia(String descripcionFamilia) {
		this.descripcionFamilia = descripcionFamilia;
	}
	public String getCodigoRiesgo() {
		return codigoRiesgo;
	}
	public void setCodigoRiesgo(String codigoRiesgo) {
		this.codigoRiesgo = codigoRiesgo;
	}
	public String getDescripcionRiesgo() {
		return descripcionRiesgo;
	}
	public void setDescripcionRiesgo(String descripcionRiesgo) {
		this.descripcionRiesgo = descripcionRiesgo;
	}
	public BigDecimal getCoberturaReaseguro() {
		return coberturaReaseguro;
	}
	public void setCoberturaReaseguro(BigDecimal coberturaReaseguro) {
		this.coberturaReaseguro = coberturaReaseguro;
	}
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public BigDecimal getSumaAseguradaDefault() {
		return sumaAseguradaDefault;
	}
	public void setSumaAseguradaDefault(BigDecimal sumaAseguradaDefault) {
		this.sumaAseguradaDefault = sumaAseguradaDefault;
	}
	public BigDecimal getSumaAseguradaMaxima() {
		return sumaAseguradaMaxima;
	}
	public void setSumaAseguradaMaxima(BigDecimal sumaAseguradaMaxima) {
		this.sumaAseguradaMaxima = sumaAseguradaMaxima;
	}
	public BigDecimal getSumaAseguradaMinima() {
		return sumaAseguradaMinima;
	}
	public void setSumaAseguradaMinima(BigDecimal sumaAseguradaMinima) {
		this.sumaAseguradaMinima = sumaAseguradaMinima;
	}
	public BigDecimal getPorcentajeSaRC() {
		return porcentajeSaRC;
	}
	public void setPorcentajeSaRC(BigDecimal porcentajeSaRC) {
		this.porcentajeSaRC = porcentajeSaRC;
	}
	public String getCodigoRiesgoRef() {
		return codigoRiesgoRef;
	}
	public void setCodigoRiesgoRef(String codigoRiesgoRef) {
		this.codigoRiesgoRef = codigoRiesgoRef;
	}
	public Long getCodigoCoberturaRef() {
		return codigoCoberturaRef;
	}
	public void setCodigoCoberturaRef(Long codigoCoberturaRef) {
		this.codigoCoberturaRef = codigoCoberturaRef;
	}
	public BigDecimal getPrima() {
		return prima;
	}
	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}
	public BigDecimal getPormillajePrima() {
		return pormillajePrima;
	}
	public void setPormillajePrima(BigDecimal pormillajePrima) {
		this.pormillajePrima = pormillajePrima;
	}
	public List<ObjetoDTO> getObjetos() {
		if (this.objetos == null)
			this.objetos = new ArrayList<ObjetoDTO>();
		return objetos;
	}
	public void setObjetos(List<ObjetoDTO> objetos) {
		this.objetos = objetos;
	}
	
	public List<PersonaCoberturaDTO> getPersonas() {
		if (this.personas == null)
			this.personas = new ArrayList<PersonaCoberturaDTO>();
		return personas;
	}
	public void setPersonas(List<PersonaCoberturaDTO> personas) {
		this.personas = personas;
	}
	public void agregarObjeto() {
		this.getObjetos().add(new ObjetoDTO());
	}
	
	public void eliminarObjeto(ObjetoDTO objeto) {
		this.getObjetos().remove(objeto);
	}
	public void agregarPersona(){
		this.getPersonas().add(new PersonaCoberturaDTO());
	}
	public void eliminarPersona(PersonaCoberturaDTO persona) {
		this.getPersonas().remove(persona);
	}
	
	

}
