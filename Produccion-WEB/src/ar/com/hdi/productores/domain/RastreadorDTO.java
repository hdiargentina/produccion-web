package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.util.Date;

import ar.com.hdi.productores.domain.tablas.MarcaRastreadorDTO;

public class RastreadorDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private boolean tiene;
	private MarcaRastreadorDTO marca;
	private String nombreContacto;
	private String telefonoContacto;
	private String email;
	private Date horaDesde;
	private Date horaHasta;
	
	public RastreadorDTO() {
		this.tiene = Boolean.TRUE;
	}
	public boolean isTiene() {
		return tiene;
	}
	public void setTiene(boolean tiene) {
		this.tiene = tiene;
	}
	public MarcaRastreadorDTO getMarca() {
		if (this.marca == null)
			this.marca = new MarcaRastreadorDTO();
		return marca;
	}
	public void setMarca(MarcaRastreadorDTO marca) {
		this.marca = marca;
	}
	public String getNombreContacto() {
		return nombreContacto;
	}
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	public String getTelefonoContacto() {
		return telefonoContacto;
	}
	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getHoraDesde() {
		return horaDesde;
	}
	public void setHoraDesde(Date horaDesde) {
		this.horaDesde = horaDesde;
	}
	public Date getHoraHasta() {
		return horaHasta;
	}
	public void setHoraHasta(Date horaHasta) {
		this.horaHasta = horaHasta;
	}
	
	
	
}
