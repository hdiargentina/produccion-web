package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.RetencionDTO;

@XmlRootElement( name = "retenciones" )
public class RetencionList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<RetencionDTO> retenciones;
	private BigDecimal cantidad;
	public List<RetencionDTO> getRetenciones() {
		return retenciones;
	}
	@XmlElement( name = "retencion" )
	public void setRetenciones(List<RetencionDTO> retenciones) {
		this.retenciones = retenciones;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	@XmlElement( name = "cantidad" )
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
}
