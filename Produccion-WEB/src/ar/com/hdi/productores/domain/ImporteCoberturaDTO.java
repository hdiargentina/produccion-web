package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class ImporteCoberturaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal selladoDelRiesgo;
	private BigDecimal selladoDeEmpresa;
	private BigDecimal porImpuestosInternos;
	private BigDecimal impImpuestosInternos;
	private BigDecimal porServiciosSociales;
	private BigDecimal impServiciosSociales;
	private BigDecimal porTasaSsn;
	private BigDecimal impTasaSsn;
	private BigDecimal porIva;
	private BigDecimal impIva;
	private BigDecimal porIvaRnl;
	private BigDecimal impIvaRnl;
	private BigDecimal porIvaPercepcion;
	private BigDecimal impIvaPercepcion;
	private BigDecimal impIibbPercepcion;
	private BigDecimal impIibbRiesgo;
	private BigDecimal impIibbEmpresa;
	private BigDecimal impRecargoFinanciero;
	private BigDecimal impExtraPrimaFija;
	private BigDecimal calculoImpuestos;
	private BigDecimal porComision;
	private BigDecimal impComision;
	private BigDecimal porExtraPrimaVar;
	private BigDecimal impExtraPrimaVar;
	private BigDecimal recFinanciero;
	public BigDecimal getSelladoDelRiesgo() {
		return selladoDelRiesgo;
	}
	public void setSelladoDelRiesgo(BigDecimal selladoDelRiesgo) {
		this.selladoDelRiesgo = selladoDelRiesgo;
	}
	public BigDecimal getSelladoDeEmpresa() {
		return selladoDeEmpresa;
	}
	public void setSelladoDeEmpresa(BigDecimal selladoDeEmpresa) {
		this.selladoDeEmpresa = selladoDeEmpresa;
	}
	public BigDecimal getPorImpuestosInternos() {
		return porImpuestosInternos;
	}
	public void setPorImpuestosInternos(BigDecimal porImpuestosInternos) {
		this.porImpuestosInternos = porImpuestosInternos;
	}
	public BigDecimal getImpImpuestosInternos() {
		return impImpuestosInternos;
	}
	public void setImpImpuestosInternos(BigDecimal impImpuestosInternos) {
		this.impImpuestosInternos = impImpuestosInternos;
	}
	public BigDecimal getPorServiciosSociales() {
		return porServiciosSociales;
	}
	public void setPorServiciosSociales(BigDecimal porServiciosSociales) {
		this.porServiciosSociales = porServiciosSociales;
	}
	public BigDecimal getImpServiciosSociales() {
		return impServiciosSociales;
	}
	public void setImpServiciosSociales(BigDecimal impServiciosSociales) {
		this.impServiciosSociales = impServiciosSociales;
	}
	public BigDecimal getPorTasaSsn() {
		return porTasaSsn;
	}
	public void setPorTasaSsn(BigDecimal porTasaSsn) {
		this.porTasaSsn = porTasaSsn;
	}
	public BigDecimal getImpTasaSsn() {
		return impTasaSsn;
	}
	public void setImpTasaSsn(BigDecimal impTasaSsn) {
		this.impTasaSsn = impTasaSsn;
	}
	public BigDecimal getPorIva() {
		return porIva;
	}
	public void setPorIva(BigDecimal porIva) {
		this.porIva = porIva;
	}
	public BigDecimal getImpIva() {
		return impIva;
	}
	public void setImpIva(BigDecimal impIva) {
		this.impIva = impIva;
	}
	public BigDecimal getPorIvaRnl() {
		return porIvaRnl;
	}
	public void setPorIvaRnl(BigDecimal porIvaRnl) {
		this.porIvaRnl = porIvaRnl;
	}
	public BigDecimal getImpIvaRnl() {
		return impIvaRnl;
	}
	public void setImpIvaRnl(BigDecimal impIvaRnl) {
		this.impIvaRnl = impIvaRnl;
	}
	public BigDecimal getPorIvaPercepcion() {
		return porIvaPercepcion;
	}
	public void setPorIvaPercepcion(BigDecimal porIvaPercepcion) {
		this.porIvaPercepcion = porIvaPercepcion;
	}
	public BigDecimal getImpIvaPercepcion() {
		return impIvaPercepcion;
	}
	public void setImpIvaPercepcion(BigDecimal impIvaPercepcion) {
		this.impIvaPercepcion = impIvaPercepcion;
	}
	public BigDecimal getImpIibbPercepcion() {
		return impIibbPercepcion;
	}
	public void setImpIibbPercepcion(BigDecimal impIibbPercepcion) {
		this.impIibbPercepcion = impIibbPercepcion;
	}
	public BigDecimal getImpIibbRiesgo() {
		return impIibbRiesgo;
	}
	public void setImpIibbRiesgo(BigDecimal impIibbRiesgo) {
		this.impIibbRiesgo = impIibbRiesgo;
	}
	public BigDecimal getImpIibbEmpresa() {
		return impIibbEmpresa;
	}
	public void setImpIibbEmpresa(BigDecimal impIibbEmpresa) {
		this.impIibbEmpresa = impIibbEmpresa;
	}
	public BigDecimal getImpRecargoFinanciero() {
		return impRecargoFinanciero;
	}
	public void setImpRecargoFinanciero(BigDecimal impRecargoFinanciero) {
		this.impRecargoFinanciero = impRecargoFinanciero;
	}
	public BigDecimal getImpExtraPrimaFija() {
		return impExtraPrimaFija;
	}
	public void setImpExtraPrimaFija(BigDecimal impExtraPrimaFija) {
		this.impExtraPrimaFija = impExtraPrimaFija;
	}
	public BigDecimal getCalculoImpuestos() {
		return calculoImpuestos;
	}
	public void setCalculoImpuestos(BigDecimal calculoImpuestos) {
		this.calculoImpuestos = calculoImpuestos;
	}
	public BigDecimal getPorComision() {
		return porComision;
	}
	public void setPorComision(BigDecimal porComision) {
		this.porComision = porComision;
	}
	public BigDecimal getImpComision() {
		return impComision;
	}
	public void setImpComision(BigDecimal impComision) {
		this.impComision = impComision;
	}
	public BigDecimal getPorExtraPrimaVar() {
		return porExtraPrimaVar;
	}
	public void setPorExtraPrimaVar(BigDecimal porExtraPrimaVar) {
		this.porExtraPrimaVar = porExtraPrimaVar;
	}
	public BigDecimal getImpExtraPrimaVar() {
		return impExtraPrimaVar;
	}
	public void setImpExtraPrimaVar(BigDecimal impExtraPrimaVar) {
		this.impExtraPrimaVar = impExtraPrimaVar;
	}
	public BigDecimal getRecFinanciero() {
		return recFinanciero;
	}
	public void setRecFinanciero(BigDecimal recFinanciero) {
		this.recFinanciero = recFinanciero;
	}
	
	

}
