package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "inspeccion" )
public class InspeccionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Date fecha;
	private BigDecimal numeroReclamo;
	private String responsable;
	private String estado;
	private String tipoDocumento;
	public Date getFecha() {
		return fecha;
	}
	@XmlElement( name = "fechaIns" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public BigDecimal getNumeroReclamo() {
		return numeroReclamo;
	}
	@XmlElement( name = "nroReclamo" )
	public void setNumeroReclamo(BigDecimal numeroReclamo) {
		this.numeroReclamo = numeroReclamo;
	}
	public String getResponsable() {
		return responsable;
	}
	@XmlElement( name = "responsable" )
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getEstado() {
		return estado;
	}
	@XmlElement( name = "estado" )
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	@XmlElement( name = "tipoDoc" )
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	
	

}
