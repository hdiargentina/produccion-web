package ar.com.hdi.productores.domain.list;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import ar.com.hdi.productores.domain.LineaDetalleProduccionDTO;

@XmlRootElement(name="detalleDiario")
public class DetalleProduccionDiariaList {
	private List<LineaDetalleProduccionDTO> lineas;

	public List<LineaDetalleProduccionDTO> getLineas() {
		return lineas;
	}
	@XmlElement(name = "linea")
	public void setLineas(List<LineaDetalleProduccionDTO> lineas) {
		this.lineas = lineas;
	}
}
