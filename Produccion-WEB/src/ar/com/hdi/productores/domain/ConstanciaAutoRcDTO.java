package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="constanciaAutosRC")
public class ConstanciaAutoRcDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal numeroComponente;
	private String numeroPoliza;
	private String nombreAsegurado;
	private String horaDesde;
	private String horaHasta;
	private String fechaDesde;
	private String fechaHasta;
	private String vehiculo;
	private String patente;
	private String motor;
	private String chasis;
	private String uso;
	private String sumaAsegurada;
	private String cobertura;
	private String accidenteTotal;
	private String accidenteParcial;
	private String incendioTotal;
	private String incendioParcial;
	private String roboTotal;
	private String roboParcial;
	private String limiteRC;
	public BigDecimal getNumeroComponente() {
		return numeroComponente;
	}
	@XmlElement( name = "componente" )
	public void setNumeroComponente(BigDecimal numeroComponente) {
		this.numeroComponente = numeroComponente;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	@XmlElement( name = "numeroDePoliza" )
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	@XmlElement( name = "nombreAsegurado" )
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public String getHoraDesde() {
		return horaDesde;
	}
	@XmlElement( name = "horaDesde" )
	public void setHoraDesde(String horaDesde) {
		this.horaDesde = horaDesde;
	}
	public String getHoraHasta() {
		return horaHasta;
	}
	@XmlElement( name = "horaHasta" )
	public void setHoraHasta(String horaHasta) {
		this.horaHasta = horaHasta;
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	@XmlElement( name = "fechaDesde" )
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	@XmlElement( name = "fechaHasta" )
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public String getVehiculo() {
		return vehiculo;
	}
	@XmlElement( name = "vehiculo" )
	public void setVehiculo(String vehiculo) {
		this.vehiculo = vehiculo;
	}
	public String getPatente() {
		return patente;
	}
	@XmlElement( name = "patente" )
	public void setPatente(String patente) {
		this.patente = patente;
	}
	public String getMotor() {
		return motor;
	}
	@XmlElement( name = "motor" )
	public void setMotor(String motor) {
		this.motor = motor;
	}
	public String getChasis() {
		return chasis;
	}
	@XmlElement( name = "chasis" )
	public void setChasis(String chasis) {
		this.chasis = chasis;
	}
	public String getUso() {
		return uso;
	}
	@XmlElement( name = "uso" )
	public void setUso(String uso) {
		this.uso = uso;
	}
	@XmlElement( name = "sumaAsegurada" )
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public String getCobertura() {
		return cobertura;
	}
	@XmlElement( name = "cobertura" )
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	public String getAccidenteTotal() {
		return accidenteTotal;
	}
	@XmlElement( name = "accidenteTotal" )
	public void setAccidenteTotal(String accidenteTotal) {
		this.accidenteTotal = accidenteTotal;
	}
	public String getAccidenteParcial() {
		return accidenteParcial;
	}
	@XmlElement( name = "accidenteParcial" )
	public void setAccidenteParcial(String accidenteParcial) {
		this.accidenteParcial = accidenteParcial;
	}
	public String getIncendioTotal() {
		return incendioTotal;
	}
	@XmlElement( name = "incendioTotal" )
	public void setIncendioTotal(String incendioTotal) {
		this.incendioTotal = incendioTotal;
	}
	public String getIncendioParcial() {
		return incendioParcial;
	}
	@XmlElement( name = "incendioParcial" )
	public void setIncendioParcial(String incendioParcial) {
		this.incendioParcial = incendioParcial;
	}
	public String getRoboTotal() {
		return roboTotal;
	}
	@XmlElement( name = "roboTotal" )
	public void setRoboTotal(String roboTotal) {
		this.roboTotal = roboTotal;
	}
	public String getRoboParcial() {
		return roboParcial;
	}
	@XmlElement( name = "roboParcial" )
	public void setRoboParcial(String roboParcial) {
		this.roboParcial = roboParcial;
	}
	public String getLimiteRC() {
		return "$" + limiteRC;
	}
	@XmlElement( name = "limiteDeRC" )
	public void setLimiteRC(String limiteRC) {
		this.limiteRC = limiteRC;
	}
	
}
