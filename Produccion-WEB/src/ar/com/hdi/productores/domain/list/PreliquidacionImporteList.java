package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "preliquidacion" )
public class PreliquidacionImporteList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal importeNeto;
	private BigDecimal importeBruto;
	
	public BigDecimal getImporteNeto() {
		return importeNeto;
	}
	@XmlElement( name = "importeNeto" )
	public void setImporteNeto(BigDecimal importeNeto) {
		this.importeNeto = importeNeto;
	}
	public BigDecimal getImporteBruto() {
		return importeBruto;
	}
	@XmlElement( name = "importeBruto" )
	public void setImporteBruto(BigDecimal importeBruto) {
		this.importeBruto = importeBruto;
	}
	
	

}
