package ar.com.hdi.productores.indicadores;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "cotizacionesWeb" )
public class CotizacionIndicador extends Indicador implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal autoSobreTotal;
	private BigDecimal hogarSobreTotal;
	private BigDecimal vidaSobreTotal;
	private BigDecimal restoSobreTotal;
	
	public CotizacionIndicador () {
		super("Cotizaciones Web", 0, "bar");
	}
	public BigDecimal getAutoSobreTotal() {
		return autoSobreTotal;
	}
	@XmlElement( name = "autosSobreTotal" )
	public void setAutoSobreTotal(BigDecimal autoSobreTotal) {
		this.autoSobreTotal = autoSobreTotal;
	}
	public BigDecimal getHogarSobreTotal() {
		return hogarSobreTotal;
	}
	@XmlElement( name = "hogarSobreTotal" )
	public void setHogarSobreTotal(BigDecimal hogarSobreTotal) {
		this.hogarSobreTotal = hogarSobreTotal;
	}
	public BigDecimal getVidaSobreTotal() {
		return vidaSobreTotal;
	}
	@XmlElement( name = "vidaSobreTotal" )
	public void setVidaSobreTotal(BigDecimal vidaSobreTotal) {
		this.vidaSobreTotal = vidaSobreTotal;
	}
	public BigDecimal getRestoSobreTotal() {
		return restoSobreTotal;
	}
	@XmlElement( name = "restoSobreTotal" )
	public void setRestoSobreTotal(BigDecimal restoSobreTotal) {
		this.restoSobreTotal = restoSobreTotal;
	}
	
	
	

}
