package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CoberturaAutoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String descripcion;
	private boolean requiereRastreador;
	private boolean requiereInspeccion;
	private boolean seleccionada;
	private BigDecimal prima;
	private BigDecimal premio;
	private List<BonificacionAutoDTO> bonificaciones;	
	private String detalle;
	private BigDecimal importeFranquicia;
	private BigDecimal clausulaAjuste;
	private BigDecimal limiteRCAcont;
	private BigDecimal limiteRCLesiones;
	private BigDecimal limiteRCCosas;
	private BigDecimal limiteRCExternos;
	private BigDecimal rebajaTecnica;
	private ImporteCoberturaDTO importe;
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isRequiereRastreador() {
		return requiereRastreador;
	}
	public void setRequiereRastreador(boolean requiereRastreador) {
		this.requiereRastreador = requiereRastreador;
	}
	public boolean isRequiereInspeccion() {
		return requiereInspeccion;
	}
	public void setRequiereInspeccion(boolean requiereInspeccion) {
		this.requiereInspeccion = requiereInspeccion;
	}
	public boolean isSeleccionada() {
		return seleccionada;
	}
	public void setSeleccionada(boolean seleccionada) {
		this.seleccionada = seleccionada;
	}
	public BigDecimal getPrima() {
		return prima;
	}
	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}
	public BigDecimal getPremio() {
		return premio;
	}
	public void setPremio(BigDecimal premio) {
		this.premio = premio;
	}
	public List<BonificacionAutoDTO> getBonificaciones() {
		if (this.bonificaciones == null)
			this.bonificaciones = new ArrayList<BonificacionAutoDTO>();
		return bonificaciones;
	}
	public void setBonificaciones(List<BonificacionAutoDTO> bonificaciones) {
		this.bonificaciones = bonificaciones;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public BigDecimal getImporteFranquicia() {
		return importeFranquicia;
	}
	public void setImporteFranquicia(BigDecimal importeFranquicia) {
		this.importeFranquicia = importeFranquicia;
	}
	public BigDecimal getClausulaAjuste() {
		return clausulaAjuste;
	}
	public void setClausulaAjuste(BigDecimal clausulaAjuste) {
		this.clausulaAjuste = clausulaAjuste;
	}
	public BigDecimal getLimiteRCAcont() {
		return limiteRCAcont;
	}
	public void setLimiteRCAcont(BigDecimal limiteRCAcont) {
		this.limiteRCAcont = limiteRCAcont;
	}
	public BigDecimal getLimiteRCLesiones() {
		return limiteRCLesiones;
	}
	public void setLimiteRCLesiones(BigDecimal limiteRCLesiones) {
		this.limiteRCLesiones = limiteRCLesiones;
	}
	public BigDecimal getLimiteRCCosas() {
		return limiteRCCosas;
	}
	public void setLimiteRCCosas(BigDecimal limiteRCCosas) {
		this.limiteRCCosas = limiteRCCosas;
	}
	public BigDecimal getLimiteRCExternos() {
		return limiteRCExternos;
	}
	public void setLimiteRCExternos(BigDecimal limiteRCExternos) {
		this.limiteRCExternos = limiteRCExternos;
	}
	public BigDecimal getRebajaTecnica() {
		this.rebajaTecnica = BigDecimal.ZERO;
		for (BonificacionAutoDTO bonificacion : getBonificaciones()) {
			this.rebajaTecnica = this.rebajaTecnica.add(bonificacion.getPorcentaje());
		}
		return rebajaTecnica;
	}
	public void setRebajaTecnica(BigDecimal rebajaTecnica) {
		this.rebajaTecnica = rebajaTecnica;
	}
	public ImporteCoberturaDTO getImporte() {
		if (importe == null)
			this.importe = new ImporteCoberturaDTO();
		return importe;
	}
	public void setImporte(ImporteCoberturaDTO importe) {
		this.importe = importe;
	}
	
	
}
