package ar.com.hdi.productores.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.tablas.TipoDocumentoDTO;
@XmlRootElement ( name = "beneficiarioPoliza" )
public class BeneficiarioDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String nombre;
	private String clausulaNoRepeticion;
	private Boolean tieneClausulaNoRepeticion;
	private String cuit;
	private String cuil;
	private TipoDocumentoDTO tipoDocumento;
	private String numeroDocumento;
	private String tipoPersona;
	public String getNombre() {
		return nombre;
	}
	@XmlElement ( name = "nombre" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClausulaNoRepeticion() {
		return clausulaNoRepeticion;
	}
	@XmlElement ( name = "clausulaNoRep" )
	public void setClausulaNoRepeticion(String clausulaNoRepeticion) {
		this.clausulaNoRepeticion = clausulaNoRepeticion;
	}
	public String getCuit() {
		return cuit;
	}
	@XmlElement ( name = "cuit" )
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public String getCuil() {
		return cuil;
	}
	@XmlElement ( name = "cuil" )
	public void setCuil(String cuil) {
		this.cuil = cuil;
	}
	public Boolean getTieneClausulaNoRepeticion() {
		if(tieneClausulaNoRepeticion == null)
			tieneClausulaNoRepeticion = false;
		return tieneClausulaNoRepeticion;
	}
	public void setTieneClausulaNoRepeticion(Boolean tieneClausulaNoRepeticion) {
		this.tieneClausulaNoRepeticion = tieneClausulaNoRepeticion;
	}
	public TipoDocumentoDTO getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(TipoDocumentoDTO tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	
}
