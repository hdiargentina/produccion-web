package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;
import ar.com.hdi.productores.domain.tablas.BancoDTO;
import ar.com.hdi.productores.domain.tablas.TipoFormaPagoDTO;

@XmlRootElement(name = "Valor")
public class PreliquidacionPagoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Date fechaCheque;
	private BigDecimal monto;
	private String numeroCheque;
	private String formaPago;
	private String bancoDescripcion;
	private TipoFormaPagoDTO tipoFormaPago;
	private BancoDTO banco;
	private String cbu;
	
	public TipoFormaPagoDTO getTipoFormaPago() {
		if (this.tipoFormaPago == null)
			this.tipoFormaPago = new TipoFormaPagoDTO();
		return tipoFormaPago;
	}
	public void setTipoFormaPago(TipoFormaPagoDTO tipoFormaPago) {
		this.tipoFormaPago = tipoFormaPago;
	}
	@XmlElement( name = "importe" )
	public BigDecimal getMonto() {
		if (this.monto == null)
			this.monto = BigDecimal.ZERO;
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public String getNumeroCheque() {
		return numeroCheque;
	}
	@XmlElement( name = "nroCheque" )
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}
	public Date getFechaCheque() {
		return fechaCheque;
	}
	@XmlElement ( name = "fecha" )
	@XmlJavaTypeAdapter(FechaAdapter.class)
	public void setFechaCheque(Date fechaCheque) {
		this.fechaCheque = fechaCheque;
	}
	public BancoDTO getBanco() {
		return banco;
	}
	public void setBanco(BancoDTO banco) {
		this.banco = banco;
	}
	public String getFormaPago() {
		return formaPago;
	}
	@XmlElement( name = "formaDePago" )
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getBancoDescripcion() {
		return bancoDescripcion;
	}
	@XmlElement( name = "banco" )
	public void setBancoDescripcion(String bancoDescripcion) {
		this.bancoDescripcion = bancoDescripcion;
	}
	public String getCbu() {
		return cbu;
	}
	public void setCbu(String cbu) {
		this.cbu = cbu;
	}
	
}
