package ar.com.hdi.productores.indicadores;

public abstract class Indicador {
	private String titulo;
	private int order;
	private String tipo;
	
	public Indicador(String titulo, int order, String tipo) {
		super();
		this.titulo = titulo;
		this.order = order;
		this.tipo = tipo;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	
	
	

}
