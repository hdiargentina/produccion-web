package ar.com.hdi.productores.domain.adapter;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MoneyAdapter extends XmlAdapter<String, BigDecimal> {
	private final DecimalFormat decimalFormat = new DecimalFormat("###,###.##");
	
	@Override
	public String marshal(BigDecimal valor) throws Exception {
		synchronized (decimalFormat) {
			return decimalFormat.format(valor);
		}
	}

	@Override
	public BigDecimal unmarshal(String valor) throws Exception {
		synchronized (decimalFormat) {
			return new BigDecimal(valor.replaceAll(",", ""));
		}
	}

}