package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ar.com.hdi.productores.domain.tablas.NacionalidadDTO;
import ar.com.hdi.productores.domain.tablas.TipoDocumentoDTO;

public class PersonaCoberturaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String apellido;
	private String nombre;
	private TipoDocumentoDTO tipoDocumento;
	private String numeroDocumento;
	private Date fechaNacimiento;
	private NacionalidadDTO nacionalidad;
	private BigDecimal sumaAsegurada;
	
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public TipoDocumentoDTO getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(TipoDocumentoDTO tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public NacionalidadDTO getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(NacionalidadDTO nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	
	

}
