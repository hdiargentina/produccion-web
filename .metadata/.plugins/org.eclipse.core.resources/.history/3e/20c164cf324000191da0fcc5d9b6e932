package ar.com.hdi.produccion.beans;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.TabChangeEvent;

import ar.com.hdi.produccion.utils.ExcelGenerator;
import ar.com.hdi.produccion.utils.Fillers;
import ar.com.hdi.productores.domain.FechaSistemaDTO;
import ar.com.hdi.productores.domain.list.DetalleProduccionDiariaList;
import ar.com.hdi.productores.domain.list.ProduccionAgrupadaList;
import ar.com.hdi.productores.domain.list.ProduccionDiariaList;
import ar.com.hdi.productores.services.remote.SistemaService;
import ar.com.hdi.productores.services.remote.impl.ProduccionServiceImpl;
import ar.com.hdi.productores.services.remote.impl.SistemaServiceImpl;
import ar.com.hdi.reports.Report;
import ar.com.hdi.utils.HDIProperties;
import ar.com.hdi.utils.HDIUtils;

@ViewScoped
@ManagedBean(name = "inicioBean")
public class InicioBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String FORMATO_FECHA = "dd/MM/yyy"; 
	private FechaSistemaDTO fechaSistema;
	private Date fechaDesde;
	@ManagedProperty( value = "#{sistemaService}" )
	private SistemaServiceImpl fechaSistemaService;
	@ManagedProperty( value = "#{produccionService}" ) 
	private ProduccionServiceImpl produccionService;
	private ProduccionDiariaList produccionDiaria;
	private ProduccionDiariaList produccionAcumulada;
	private ProduccionAgrupadaList produccionAgrupada;
	private boolean diarioActualizado;
	private boolean acumuladoActualizado;
	private boolean agrupadoActualizado;
	
	private Integer activeTab;
	
	@PostConstruct
	public void init() {
		System.out.println("Entro al init");
		fechaSistema = fechaSistemaService.getFechasSistema("A", "CA");
		this.setFechaDesde(fechaSistema.getProduccion());
		obtenerProduccionDiaria(restarDia(fechaSistema.getProduccion()));
		obtenerProduccionAcumulada(restarDia(fechaSistema.getProduccion()));
		obtenerProduccionAgrupada(restarDia(fechaSistema.getProduccion()));
		actualizarTablas();
		System.out.println("Fin del init");
	}

	public void obtenerProduccionAcumulada() {
		produccionAcumulada = produccionService
				.obtenerDatosProduccionAcumuladaPorFecha("A", "CA",
						fechaSistema.getProduccion());
	}

	public void obtenerProduccionAcumulada(Date fecha) {
		produccionAcumulada = produccionService
				.obtenerDatosProduccionAcumuladaPorFecha("A", "CA",
						fecha);
	}
	
	public void obtenerProduccionAgrupada() {
		produccionAgrupada = produccionService
				.obtenerDatosProduccionAgrupadaPorFecha("A", "CA",
						fechaSistema.getProduccion());
	}

	public void obtenerProduccionAgrupada(Date fecha) {
		produccionAgrupada = produccionService
				.obtenerDatosProduccionAgrupadaPorFecha("A", "CA",
						fecha);
	}

	public void obtenerProduccionDiaria() {
		System.out.println("Entre en obtenerProduccionDiaria");
		produccionDiaria = produccionService.obtenerDatosProduccionPorFecha(
				"A", "CA", fechaSistema.getProduccion());
		diarioActualizado = true;
	}

	public void obtenerProduccionDiaria(Date fechaProduccion) {
		System.out.println("Entre en obtenerProduccionDiariaPorFecha");
		produccionDiaria = produccionService.obtenerDatosProduccionPorFecha(
				"A", "CA", fechaProduccion);
	}

	public void actualizarTablas() {
		obtenerProduccionDiaria();
		obtenerProduccionAcumulada();
		obtenerProduccionAgrupada();
		switch(this.getActiveTab()) {
		case 0 :
			diarioActualizado = false;
			acumuladoActualizado = true;
			agrupadoActualizado = true;
			break;
		case 1 :
			diarioActualizado = true;
			acumuladoActualizado = false;
			agrupadoActualizado = true;
			break;
		case 2 :
			diarioActualizado = true;
			acumuladoActualizado = true;
			agrupadoActualizado = false;
			break;
		}
		
	}

	public void crearExcelDiario() {
		try{
			String filename = "produccionDiaria_" + new Date().getTime() + ".xls";
			ExcelGenerator eg = new ExcelGenerator();
			Map<String, List<String>> encabezados = Fillers
					.fillCabeceraProduccion();
			eg.generarXLSX(
					this.produccionDiaria,
					this.fechaDesde,
					encabezados,
					HDIProperties.getPropiedad("config.properties",
							"download_location") + filename);
			Report.downloadExcel(filename);
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR ,"No se pudo generar el archivo...", "No hay datos, para generar el archivo"));
		}
	}

	public void crearExcelAcumulado() {
		try {
			String filename = "produccionAcumulada_" + new Date().getTime()
					+ ".xls";
			ExcelGenerator eg = new ExcelGenerator();
			Map<String, List<String>> encabezados = Fillers
					.fillCabeceraProduccion();
			eg.generarXLSX(
					this.produccionAcumulada,
					this.fechaDesde,
					encabezados,
					HDIProperties.getPropiedad("config.properties",
							"download_location") + filename);
			Report.downloadExcel(filename);
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR ,"No se pudo generar el archivo...", "No hay datos, para generar el archivo"));
		}
	}

	public void crearExcelAgrupado() {
		try {
			String filename = "produccionAgrupada_" + new Date().getTime() + ".xls";
			ExcelGenerator eg = new ExcelGenerator();
			Map<String, List<String>> encabezados = Fillers
					.fillCabeceraProduccionAgrupada();
			eg.generarXLSX(
					this.produccionAgrupada,
					this.fechaDesde,
					encabezados,
					HDIProperties.getPropiedad("config.properties",
							"download_location") + filename);
			Report.downloadExcel(filename);
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR ,"No se pudo generar el archivo...", "No hay datos, para generar el archivo"));
		}
	}

	public void crearExcelDetalleProduccionDiaria() {
		try {
			String filename = "detalleProduccionDiaria" + new Date().getTime()
					+ ".xls";
			DetalleProduccionDiariaList detalle = this.produccionService
					.obtenerDetalleProduccionDiariaPorFecha("A", "CA",
							this.fechaSistema.getProduccion());
			ExcelGenerator eg = new ExcelGenerator();
			Map<String, List<String>> encabezados = Fillers
					.fillCabeceraDetalleProduccion();
			eg.generarXLSX(
					detalle,
					this.fechaSistema.getProduccion(),
					encabezados,
					HDIProperties.getPropiedad("config.properties",
							"download_location") + filename);
			Report.downloadExcel(filename);
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR ,"No se pudo generar el archivo...", "No hay datos, para generar el archivo"));
		}
	}
	
	public void crearPDF(String tipo) {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			switch (tipo) {
			case "diario":
				Fillers.fillProduccionReport(params, produccionDiaria,
						fechaDesde, false);
				generarPDF(params, "ProduccionDiaria_", "Produccion");
				break;
			case "acumulado":
				Fillers.fillProduccionReport(params, produccionAcumulada,
						fechaDesde, true);
				generarPDF(params, "ProduccionAcumulada_", "Produccion");
				break;
			case "agrupada":
				Fillers.fillProduccionAgrupadaReport(params, produccionAgrupada,
						fechaDesde, fechaSistema.getProduccion());
				generarPDF(params, "ProduccionAgrupada_", "ProduccionAgrupada");
				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR ,"No se pudo generar el archivo...", "No hay datos, para generar el archivo"));
		}
	}

	public void generarPDF(Map<String, Object> params, String fileName,
			String tipoReporte) throws Exception {
		String filename = fileName + new Date().getTime() + ".pdf";
		ExternalContext ec = FacesContext.getCurrentInstance()
				.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();

		String baseURL = request.getScheme() + "://" + request.getServerName()
				+ ":" + request.getServerPort() + ec.getRequestContextPath();
		try {
			Report.generatePdf(
					params,
					HDIProperties.getPropiedad("reportes.properties",
							tipoReporte),
					HDIProperties.getPropiedad("config.properties",
							"download_location") + filename);
			ec.redirect(baseURL + "/download" + "?contentType=pdf&fileName="
					+ filename);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception(e);
		}
	}
	
	public void onTabChange(TabChangeEvent event) {
		String idArray[] = event.getTab().getId().split(":");
		String id = idArray[idArray.length - 1];
		switch (id) {
		case "tabDiario":
			diarioActualizado = false;
			break;
		case "tabAcumulado":
			acumuladoActualizado = false;
			break;
		case "tabAgrupada":
			agrupadoActualizado = false;
			break;
		default:
			break;
		}
	}
	
	public Date restarDia(Date fechaProduccion) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fechaProduccion);
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}

	public FechaSistemaDTO getFechaSistema() {
		return fechaSistema;
	}

	public void setFechaSistema(FechaSistemaDTO fechaSistema) {
		this.fechaSistema = fechaSistema;
	}

	public ProduccionDiariaList getProduccionDiaria() {
		return produccionDiaria;
	}

	public void setProduccionDiaria(ProduccionDiariaList produccionDiaria) {
		this.produccionDiaria = produccionDiaria;
	}

	public ProduccionDiariaList getProduccionAcumulada() {
		return produccionAcumulada;
	}

	public void setProduccionAcumulada(ProduccionDiariaList produccionAcumulada) {
		this.produccionAcumulada = produccionAcumulada;
	}

	public String getFechaFormateada() {
//		return new SimpleDateFormat(FORMATO_FECHA).format(this.getFechaSistema()
//				.getProduccion());
		return new SimpleDateFormat(FORMATO_FECHA).format(this.fechaDesde);
	}
	
	public String getFechaInicioFormateada() {
		System.out.println(this.fechaDesde);
		System.out.println(new SimpleDateFormat(FORMATO_FECHA).format(HDIUtils.obtenerPrimerDiaMes(this.fechaDesde)));
		return new SimpleDateFormat(FORMATO_FECHA).format(HDIUtils.obtenerPrimerDiaMes(this.fechaDesde));
	}

	public ProduccionAgrupadaList getProduccionAgrupada() {
		return produccionAgrupada;
	}

	public void setProduccionAgrupada(ProduccionAgrupadaList produccionAgrupada) {
		this.produccionAgrupada = produccionAgrupada;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public boolean isDiarioActualizado() {
		return diarioActualizado;
	}

	public void setDiarioActualizado(boolean diarioActualizado) {
		this.diarioActualizado = diarioActualizado;
	}

	public Integer getActiveTab() {
		if(activeTab == null) 
			return 0;
		return activeTab;
	}

	public void setActiveTab(Integer activeTab) {
		this.activeTab = activeTab;
	}

	public boolean isAcumuladoActualizado() {
		return acumuladoActualizado;
	}

	public void setAcumuladoActualizado(boolean acumuladoActualizado) {
		this.acumuladoActualizado = acumuladoActualizado;
	}

	public boolean isAgrupadoActualizado() {
		return agrupadoActualizado;
	}

	public void setAgrupadoActualizado(boolean agrupadoActualizado) {
		this.agrupadoActualizado = agrupadoActualizado;
	}

	public SistemaService getFechaSistemaService() {
		return fechaSistemaService;
	}

	public void setFechaSistemaService(SistemaServiceImpl fechaSistemaService) {
		this.fechaSistemaService = fechaSistemaService;
	}

	public ProduccionServiceImpl getProduccionService() {
		return produccionService;
	}

	public void setProduccionService(ProduccionServiceImpl produccionService) {
		this.produccionService = produccionService;
	}
	
	
}
