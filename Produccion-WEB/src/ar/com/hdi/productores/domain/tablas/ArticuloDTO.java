package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ar.com.hdi.productores.domain.RamaDTO;
import ar.com.hdi.productores.domain.enume.TipoRama;

public class ArticuloDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long codigo;
	private String descripcion;
	private String descripcionCorta;
	private List<RamaDTO> ramas;
	public long getCodigo() {
		return codigo;
	}
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescripcionCorta() {
		return descripcionCorta;
	}
	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}
	public List<RamaDTO> getRamas() {
		if(this.ramas == null)
			this.ramas = new ArrayList<RamaDTO>();
		return ramas;
	}
	public void setRamas(List<RamaDTO> ramas) {
		this.ramas = ramas;
	}
	public boolean articuloEnRama(TipoRama tipoRama) {
		if(ramas == null) 
			return false;
		
		for(RamaDTO rama: this.ramas){
			if(rama.getTipoRama().equals(tipoRama))
				return true;
		}
		return false;
	}
	public RamaDTO ramaEnRama(TipoRama tipoRama) {
		if(ramas == null)
			return null;
					
		for(RamaDTO rama: this.ramas){
			if(rama.getTipoRama().equals(tipoRama)) 
				return rama;
		}
		return null;
	}
}
