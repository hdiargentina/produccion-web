package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "mensaje" )
public class MensajeDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id; 
	private String body;
	private Date fecha;
	private int importancia;
	private String status;
	public String getId() {
		return id;
	}
	@XmlElement( name = "idMensaje" )
	public void setId(String id) {
		this.id = id;
	}
	public String getBody() {
		return body;
	}
	@XmlElement( name = "bodyMensaje" )
	public void setBody(String body) {
		this.body = body;
	}
	public Date getFecha() {
		return fecha;
	}
	@XmlElement( name = "fechaMensaje" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getImportancia() {
		return importancia;
	}
	@XmlElement( name = "importancia" )
	public void setImportancia(int importancia) {
		this.importancia = importancia;
	}
	public String getStatus() {
		return status;
	}
	@XmlElement( name = "status" )
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
