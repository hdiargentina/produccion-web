package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "fechasDelSistema" )
public class FechaSistemaDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date produccion;
	private Date siniestros;
	private Date caja;
	private Date ultimoIngfresoCaja;
	private Date asiento;
	public Date getProduccion() {
		return produccion;
	}
	@XmlElement( name = "produccion" )
	@XmlJavaTypeAdapter(FechaAdapter.class)
	public void setProduccion(Date produccion) {
		this.produccion = produccion;
	}
	public Date getSiniestros() {
		return siniestros;
	}
	@XmlElement( name = "siniestros" )
	@XmlJavaTypeAdapter(FechaAdapter.class)
	public void setSiniestros(Date siniestros) {
		this.siniestros = siniestros;
	}
	public Date getCaja() {
		return caja;
	}
	@XmlElement( name = "caja" )
	@XmlJavaTypeAdapter(FechaAdapter.class)
	public void setCaja(Date caja) {
		this.caja = caja;
	}
	public Date getUltimoIngfresoCaja() {
		return ultimoIngfresoCaja;
	}
	@XmlElement( name = "ultimoIngresoDeCaja" )
	@XmlJavaTypeAdapter(FechaAdapter.class)
	public void setUltimoIngfresoCaja(Date ultimoIngfresoCaja) {
		this.ultimoIngfresoCaja = ultimoIngfresoCaja;
	}
	public Date getAsiento() {
		return asiento;
	}
	@XmlElement( name = "Asiento" )
	@XmlJavaTypeAdapter(FechaAdapter.class)
	public void setAsiento(Date asiento) {
		this.asiento = asiento;
	}
	
	

}
