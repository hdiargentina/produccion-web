package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "suplemento" )
public class EndosoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal numero;
	private String operacion;
	private BigDecimal polizaAnterior;
	private BigDecimal polizaSiguiente;
	private Date fechaEmision;
	private Date fechaDesde;
	private Date fechaHasta;
	private String moneda;
	private BigDecimal prima;
	private BigDecimal premio;
	private BigDecimal extraPrimaFija;
	private BigDecimal porcentajeExtraPrimaVariable;
	private BigDecimal recargoFinanciero;
	private BigDecimal porcentajeRecargoFinanciero;
	private String pdf;
	private BigDecimal impuestos;
	private BigDecimal sellado;
	private BigDecimal ivaTotal;
	private BigDecimal porcentajeIva;
	private BigDecimal ingresosBrutos;
	private BigDecimal porcentajeImpuestos;
	private BigDecimal extraPrimaVariable;
	private String formaPago;
	private String empresaTarjetaCredito;
	private String numeroTarjetaCredito;
	private String banco;
	private String cbu;
	private String documentoPdf;
	private String periodoFacturacion;
	private String esperaKausay;
	private BigDecimal tipoOperacion;
	private BigDecimal subTipoOperacion;
	private BigDecimal operacionSistema;
	private String descripcionOperacion; 
	private List<ComisionDTO> comisiones;
	private List<CuotaDTO> cuotas;
	public BigDecimal getNumero() {
		return numero;
	}
	@XmlElement( name = "nro" )
	public void setNumero(BigDecimal numero) {
		this.numero = numero;
	}
	public String getOperacion() {
		return operacion;
	}
	@XmlElement( name = "operacion" )
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public BigDecimal getPolizaAnterior() {
		return polizaAnterior;
	}
	@XmlElement( name = "polAnterior" )
	public void setPolizaAnterior(BigDecimal polizaAnterior) {
		this.polizaAnterior = polizaAnterior;
	}
	public BigDecimal getPolizaSiguiente() {
		return polizaSiguiente;
	}
	@XmlElement( name = "polSiguiente" )
	public void setPolizaSiguiente(BigDecimal polizaSiguiente) {
		this.polizaSiguiente = polizaSiguiente;
	}
	public Date getFechaEmision() {
		return fechaEmision;
	}
	@XmlElement( name = "fechaEmis" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	@XmlElement( name = "fechaDesde" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	@XmlElement( name = "fechaHasta" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public String getMoneda() {
		return moneda;
	}
	@XmlElement( name = "moneda" )
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public BigDecimal getPrima() {
		return prima;
	}
	@XmlElement( name = "prima" )
	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}
	public BigDecimal getPremio() {
		return premio;
	}
	@XmlElement( name = "premio" )
	public void setPremio(BigDecimal premio) {
		this.premio = premio;
	}
	public BigDecimal getExtraPrimaVariable() {
		return extraPrimaVariable;
	}
	@XmlElement( name = "extraPaVar" )
	public void setExtraPrimaVariable(BigDecimal extraPrimaVariable) {
		this.extraPrimaVariable = extraPrimaVariable;
	}
	public BigDecimal getPorcentajeExtraPrimaVariable() {
		return porcentajeExtraPrimaVariable;
	}
	@XmlElement( name = "porcExtraPaVar" )
	public void setPorcentajeExtraPrimaVariable(
			BigDecimal porcentajeExtraPrimaVariable) {
		this.porcentajeExtraPrimaVariable = porcentajeExtraPrimaVariable;
	}
	public BigDecimal getRecargoFinanciero() {
		return recargoFinanciero;
	}
	@XmlElement( name = "recFinanciero" )
	public void setRecargoFinanciero(BigDecimal recargoFinanciero) {
		this.recargoFinanciero = recargoFinanciero;
	}
	public BigDecimal getPorcentajeRecargoFinanciero() {
		return porcentajeRecargoFinanciero;
	}
	@XmlElement( name = "porcRecFinanciero" )
	public void setPorcentajeRecargoFinanciero(
			BigDecimal porcentajeRecargoFinanciero) {
		this.porcentajeRecargoFinanciero = porcentajeRecargoFinanciero;
	}
	public String getPdf() {
		return pdf;
	}
	@XmlElement( name = "documentoPdf" )
	public void setPdf(String pdf) {
		this.pdf = pdf;
	}
	public BigDecimal getImpuestos() {
		return impuestos;
	}
	@XmlElement( name = "impuestos" )
	public void setImpuestos(BigDecimal impuestos) {
		this.impuestos = impuestos;
	}
	public BigDecimal getSellado() {
		return sellado;
	}
	@XmlElement( name = "sellado" )
	public void setSellado(BigDecimal sellado) {
		this.sellado = sellado;
	}
	public BigDecimal getIvaTotal() {
		return ivaTotal;
	}
	@XmlElement( name = "ivaTotal" )
	public void setIvaTotal(BigDecimal ivaTotal) {
		this.ivaTotal = ivaTotal;
	}
	public BigDecimal getPorcentajeIva() {
		return porcentajeIva;
	}
	@XmlElement( name = "porcIva" )
	public void setPorcentajeIva(BigDecimal porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	public BigDecimal getIngresosBrutos() {
		return ingresosBrutos;
	}
	@XmlElement( name = "ingresosBrutos" )
	public void setIngresosBrutos(BigDecimal ingresosBrutos) {
		this.ingresosBrutos = ingresosBrutos;
	}
	public BigDecimal getPorcentajeImpuestos() {
		return porcentajeImpuestos;
	}
	@XmlElement( name = "porcImpuestos" )
	public void setPorcentajeImpuestos(BigDecimal porcentajeImpuestos) {
		this.porcentajeImpuestos = porcentajeImpuestos;
	}
	public String getFormaPago() {
		return formaPago;
	}
	@XmlElement( name = "formaDePago" )
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getEmpresaTarjetaCredito() {
		return empresaTarjetaCredito;
	}
	@XmlElement( name = "empresaTc" )
	public void setEmpresaTarjetaCredito(String empresaTarjetaCredito) {
		this.empresaTarjetaCredito = empresaTarjetaCredito;
	}
	public String getNumeroTarjetaCredito() {
		return numeroTarjetaCredito;
	}
	@XmlElement( name = "numeroTc" )
	public void setNumeroTarjetaCredito(String numeroTarjetaCredito) {
		this.numeroTarjetaCredito = numeroTarjetaCredito;
	}
	public String getBanco() {
		return banco;
	}
	@XmlElement( name = "banco" )
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getCbu() {
		return cbu;
	}
	@XmlElement( name = "nroCbu" )
	public void setCbu(String cbu) {
		this.cbu = cbu;
	}
	public List<ComisionDTO> getComisiones() {
		if (this.comisiones == null)
			this.comisiones = new ArrayList<ComisionDTO>();
		return comisiones;
	}
	@XmlElementWrapper( name = "comisiones" )
	@XmlElement( name = "comision" )
	public void setComisiones(List<ComisionDTO> comisiones) {
		this.comisiones = comisiones;
	}
	public List<CuotaDTO> getCuotas() {
		if (this.cuotas == null)
			this.cuotas = new ArrayList<CuotaDTO>();
		return cuotas;
	}
	@XmlElementWrapper( name = "cuotas" )
	@XmlElement( name = "cuota" )
	public void setCuotas(List<CuotaDTO> cuotas) {
		this.cuotas = cuotas;
	}
	public BigDecimal getExtraPrimaFija() {
		return extraPrimaFija;
	}
	@XmlElement ( name = "extraPaFija" )
	public void setExtraPrimaFija(BigDecimal extraPrimaFija) {
		this.extraPrimaFija = extraPrimaFija;
	}
	@XmlElement ( name = "documentoPdf" )
	public String getDocumentoPdf() {
		return documentoPdf;
	}
	public void setDocumentoPdf(String documentoPdf) {
		this.documentoPdf = documentoPdf;
	}
	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}
	@XmlElement ( name = "duracionPer" )
	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
	public String getEsperaKausay() {
		return esperaKausay;
	}
	@XmlElement ( name = "esperaKausay" )
	public void setEsperaKausay(String esperaKausay) {
		this.esperaKausay = esperaKausay;
	}
	public BigDecimal getTipoOperacion() {
		return tipoOperacion;
	}
	@XmlElement ( name = "tipoOperacion" )
	public void setTipoOperacion(BigDecimal tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public BigDecimal getSubTipoOperacion() {
		return subTipoOperacion;
	}
	@XmlElement ( name = "subTipoOper" )
	public void setSubTipoOperacion(BigDecimal subTipoOperacion) {
		this.subTipoOperacion = subTipoOperacion;
	}
	public BigDecimal getOperacionSistema() {
		return operacionSistema;
	}
	@XmlElement ( name = "operSistema" )
	public void setOperacionSistema(BigDecimal operacionSistema) {
		this.operacionSistema = operacionSistema;
	}
	public String getDescripcionOperacion() {
		return descripcionOperacion;
	}
	@XmlElement ( name = "descripcionOperacion" )
	public void setDescripcionOperacion(String descripcionOperacion) {
		this.descripcionOperacion = descripcionOperacion;
	}
	
	
	
}
