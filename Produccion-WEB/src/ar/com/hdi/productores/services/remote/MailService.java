package ar.com.hdi.productores.services.remote;

import java.util.Map;

import javax.ejb.Local;

import ar.com.hdi.productores.domain.IntermediarioDTO;
import ar.com.hdi.productores.domain.ListaDistribucionDTO;

@Local
public interface MailService {
	public Map<String, Object> getDatosServidor();
	public ListaDistribucionDTO getListaDistribucion(String empresa, String sucursal, IntermediarioDTO intermediario, IntermediarioDTO vendedor, String listaDistribucion);
	
}
