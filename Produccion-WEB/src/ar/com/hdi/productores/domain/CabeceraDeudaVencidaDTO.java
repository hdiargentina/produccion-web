package ar.com.hdi.productores.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement( name = "cabecera" )
public class CabeceraDeudaVencidaDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String primeraColumna;
	private String segundaColumna;
	private String terceraColumna;
	private String fechaProceso;
	private String horaProceso;
	public String getPrimeraColumna() {
		return primeraColumna;
	}
	@XmlElement( name = "firstColumn" )
	public void setPrimeraColumna(String primeraColumna) {
		this.primeraColumna = primeraColumna;
	}
	public String getSegundaColumna() {
		return segundaColumna;
	}
	@XmlElement( name = "secondColumn" )
	public void setSegundaColumna(String segundaColumna) {
		this.segundaColumna = segundaColumna;
	}
	public String getTerceraColumna() {
		return terceraColumna;
	}
	@XmlElement( name = "thirdColumn" )
	public void setTerceraColumna(String terceraColumna) {
		this.terceraColumna = terceraColumna;
	}
	public String getFechaProceso() {
		return fechaProceso;
	}
	@XmlElement( name = "fechaProceso" )
	public void setFechaProceso(String fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	public String getHoraProceso() {
		return horaProceso;
	}
	@XmlElement( name = "horaProceso" )
	public void setHoraProceso(String horaProceso) {
		this.horaProceso = horaProceso;
	}
}
