package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class BonificacionAutoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal codigo;
	private String descripcion;
	private BigDecimal porcentaje;
	private BigDecimal porcentajeMinimo;
	private BigDecimal porcentajeMaximo;
	private boolean modificable;
	private boolean seleccionada;

	public BigDecimal getCodigo() {
		return codigo;
	}

	public void setCodigo(BigDecimal codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

	public BigDecimal getPorcentajeMinimo() {
		return porcentajeMinimo;
	}

	public void setPorcentajeMinimo(BigDecimal porcentajeMinimo) {
		this.porcentajeMinimo = porcentajeMinimo;
	}

	public BigDecimal getPorcentajeMaximo() {
		return porcentajeMaximo;
	}

	public void setPorcentajeMaximo(BigDecimal porcentajeMaximo) {
		this.porcentajeMaximo = porcentajeMaximo;
	}

	public boolean isModificable() {
		return modificable;
	}

	public void setModificable(boolean modificable) {
		this.modificable = modificable;
	}

	public boolean isSeleccionada() {
		return seleccionada;
	}

	public void setSeleccionada(boolean seleccionada) {
		this.seleccionada = seleccionada;
	}

}
