package ar.com.hdi.productores.indicadores;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "cobranzas" )
public class CobranzaIndicador extends Indicador implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal deudaMayorVigente;
	private BigDecimal deudaMenorVigente;
	private BigDecimal anulacion;
	
	public CobranzaIndicador() {
		super("Cobranzas", 0, "pie");
	}
	public BigDecimal getDeudaMayorVigente() {
		return deudaMayorVigente;
	}
	@XmlElement( name = "deudaMayVig" )
	public void setDeudaMayorVigente(BigDecimal deudaMayorVigente) {
		this.deudaMayorVigente = deudaMayorVigente;
	}
	public BigDecimal getDeudaMenorVigente() {
		return deudaMenorVigente;
	}
	@XmlElement( name = "deudaMenVig" )
	public void setDeudaMenorVigente(BigDecimal deudaMenorVigente) {
		this.deudaMenorVigente = deudaMenorVigente;
	}
	public BigDecimal getAnulacion() {
		return anulacion;
	}
	@XmlElement( name = "anulFaPagVi" )
	public void setAnulacion(BigDecimal anulacion) {
		this.anulacion = anulacion;
	}
	
	

}
