package ar.com.hdi.productores.domain;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement( name = "registro" )
public class DatosQuincenaDTO {
	private String descripcion;
	private BigDecimal anio;
	private BigDecimal mes;
	private BigDecimal numero;
	
	@XmlElement( name = "descripcion" )
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@XmlElement( name = "anio" )
	public BigDecimal getAnio() {
		return anio;
	}
	public void setAnio(BigDecimal anio) {
		this.anio = anio;
	}
	@XmlElement( name = "mes" )
	public BigDecimal getMes() {
		return mes;
	}
	public void setMes(BigDecimal mes) {
		this.mes = mes;
	}
	@XmlElement( name = "quincena" )
	public BigDecimal getNumero() {
		return numero;
	}
	public void setNumero(BigDecimal numero) {
		this.numero = numero;
	}
	
}
