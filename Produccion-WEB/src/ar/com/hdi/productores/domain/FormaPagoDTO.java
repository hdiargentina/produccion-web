package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import ar.com.hdi.productores.domain.tablas.EmpresaTarjetaDTO;
import ar.com.hdi.productores.domain.tablas.TipoFormaPagoDTO;

public class FormaPagoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private TipoFormaPagoDTO tipoFormaPago;
	private EmpresaTarjetaDTO empresaTarjeta;
	private BigDecimal numeroTarjeta;
	private BigDecimal vencimientoTarjeta;
	private BigDecimal cbu;
	
	public FormaPagoDTO(){}
	
	public FormaPagoDTO(TipoFormaPagoDTO tipoFormaPago) {
		this.tipoFormaPago = tipoFormaPago;
	}
	
	public TipoFormaPagoDTO getTipoFormaPago() {
		return tipoFormaPago;
	}
	public void setTipoFormaPago(TipoFormaPagoDTO tipoFormaPago) {
		this.tipoFormaPago = tipoFormaPago;
	}
	public EmpresaTarjetaDTO getEmpresaTarjeta() {
		return empresaTarjeta;
	}
	public void setEmpresaTarjeta(EmpresaTarjetaDTO empresaTarjeta) {
		if (this.empresaTarjeta == null)
			this.empresaTarjeta = new EmpresaTarjetaDTO();
		this.empresaTarjeta = empresaTarjeta;
	}
	public BigDecimal getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(BigDecimal numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public BigDecimal getVencimientoTarjeta() {
		return vencimientoTarjeta;
	}
	public void setVencimientoTarjeta(BigDecimal vencimientoTarjeta) {
		this.vencimientoTarjeta = vencimientoTarjeta;
	}
	public BigDecimal getCbu() {
		return cbu;
	}
	public void setCbu(BigDecimal cbu) {
		this.cbu = cbu;
	}

}
