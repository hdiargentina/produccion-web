package ar.com.hdi.reports;

import java.io.IOException;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class Report {
	public static void generatePdf(Map<String, Object> parametros, String nombreReporte, String nombreArchivoPdf) throws Exception {
		try {
			JasperPrint informe = JasperFillManager.fillReport(nombreReporte, parametros, new JREmptyDataSource());
			JasperExportManager.exportReportToPdfFile(informe, nombreArchivoPdf);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			throw new Exception(e.getMessage());
		}
	}
	
	public static void createPdf(String nombreArchivoPdf) {
		ExternalContext ec= FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		String baseURL = request.getScheme()+"://" + request.getServerName() + ":" + request.getServerPort() + ec.getRequestContextPath();
		try {
			ec.redirect(baseURL+"/download"  + "?contentType=pdf&fileName=" + nombreArchivoPdf);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void downloadExcel(String nombreArchivo) {
		ExternalContext ec= FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		String baseURL = request.getScheme()+"://" + request.getServerName() + ":" + request.getServerPort() + ec.getRequestContextPath();
		try {
			ec.redirect(baseURL+"/download"  + "?contentType=vnd.ms-excel&fileName=" + nombreArchivo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
