package ar.com.hdi.productores.domain.adapter;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class VigenciaAdapter extends XmlAdapter<String, Boolean> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public String marshal(Boolean arg0) throws Exception {
		
		return arg0?"S":"N";
	}

	@Override
	public Boolean unmarshal(String arg0) throws Exception {
		return arg0.equals("S")?true:false;
	}

}
