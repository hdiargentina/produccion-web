package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "motor" )
public class MotorDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String fabrica;
	private String modelo;
	private BigDecimal numero;
	private BigDecimal anio;
	private String tipo;
	public String getFabrica() {
		return fabrica;
	}
	@XmlElement( name = "fabrica" )
	public void setFabrica(String fabrica) {
		this.fabrica = fabrica;
	}
	public String getModelo() {
		return modelo;
	}
	@XmlElement( name = "modelo" )
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public BigDecimal getNumero() {
		return numero;
	}
	@XmlElement( name = "numero" )
	public void setNumero(BigDecimal numero) {
		this.numero = numero;
	}
	public BigDecimal getAnio() {
		return anio;
	}
	@XmlElement( name = "anio" )
	public void setAnio(BigDecimal anio) {
		this.anio = anio;
	}
	public String getTipo() {
		return tipo;
	}
	@XmlElement( name = "tipo" )
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
