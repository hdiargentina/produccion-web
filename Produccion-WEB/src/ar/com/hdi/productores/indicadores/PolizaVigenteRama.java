package ar.com.hdi.productores.indicadores;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "indicador" )
public class PolizaVigenteRama implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String descripcion;
	private BigDecimal cantidad;
	private BigDecimal prima;
	private BigDecimal premio;
	public String getDescripcion() {
		return descripcion;
	}
	@XmlElement( name = "ramd" )
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	@XmlElement( name = "cantidad" )
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public BigDecimal getPrima() {
		return prima;
	}
	@XmlElement( name = "prima" )
	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}
	public BigDecimal getPremio() {
		return premio;
	}
	@XmlElement( name = "premio" )
	public void setPremio(BigDecimal premio) {
		this.premio = premio;
	}
	
	

}
