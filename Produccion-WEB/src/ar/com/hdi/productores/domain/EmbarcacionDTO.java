package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "embarcacion" )
public class EmbarcacionDTO extends BienAseguradoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String tipo;
	private String nombre;
	private String astillero;
	private String matricula;
	private String modelo;
	private BigDecimal anio;
	private String material;
	private BigDecimal avaluado;
	private BigDecimal sumaCasco;
	private BigDecimal sumaMotor;
	private BigDecimal sumaTotal;
	private BigDecimal eslora;
	private BigDecimal manga;
	private BigDecimal puntal;
	private List<MotorDTO> motores;
	private String producto;
	private List<CoberturaDTO> coberturas;
	private List<String> clausulas;
	public String getTipo() {
		return tipo;
	}
	@XmlElement( name = "tipo" )
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "nombre" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAstillero() {
		return astillero;
	}
	@XmlElement( name = "astillero" )
	public void setAstillero(String astillero) {
		this.astillero = astillero;
	}
	public String getMatricula() {
		return matricula;
	}
	@XmlElement( name = "matricula" )
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getModelo() {
		return modelo;
	}
	@XmlElement( name = "modelo" )
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public BigDecimal getAnio() {
		return anio;
	}
	@XmlElement( name = "anio" )
	public void setAnio(BigDecimal anio) {
		this.anio = anio;
	}
	public String getMaterial() {
		return material;
	}
	@XmlElement( name = "material" )
	public void setMaterial(String material) {
		this.material = material;
	}
	public BigDecimal getAvaluado() {
		return avaluado;
	}
	@XmlElement( name = "avaluado" )
	public void setAvaluado(BigDecimal avaluado) {
		this.avaluado = avaluado;
	}
	public BigDecimal getSumaCasco() {
		return sumaCasco;
	}
	@XmlElement( name = "sumaCasco" )
	public void setSumaCasco(BigDecimal sumaCasco) {
		this.sumaCasco = sumaCasco;
	}
	public BigDecimal getSumaMotor() {
		return sumaMotor;
	}
	@XmlElement( name = "sumaMotor" )
	public void setSumaMotor(BigDecimal sumaMotor) {
		this.sumaMotor = sumaMotor;
	}
	public BigDecimal getSumaTotal() {
		return sumaTotal;
	}
	@XmlElement( name = "sumaTotal" )
	public void setSumaTotal(BigDecimal sumaTotal) {
		this.sumaTotal = sumaTotal;
	}
	public BigDecimal getEslora() {
		return eslora;
	}
	@XmlElement( name = "eslora" )
	public void setEslora(BigDecimal eslora) {
		this.eslora = eslora;
	}
	public BigDecimal getManga() {
		return manga;
	}
	@XmlElement( name = "manga" )
	public void setManga(BigDecimal manga) {
		this.manga = manga;
	}
	public BigDecimal getPuntal() {
		return puntal;
	}
	@XmlElement( name = "puntal" )
	public void setPuntal(BigDecimal puntal) {
		this.puntal = puntal;
	}
	public List<MotorDTO> getMotores() {
		return motores;
	}
	@XmlElementWrapper( name = "motores" )
	@XmlElement( name = "motor" )
	public void setMotores(List<MotorDTO> motores) {
		this.motores = motores;
	}
	public String getProducto() {
		return producto;
	}
	@XmlElement( name = "producto" )
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public List<CoberturaDTO> getCoberturas() {
		return coberturas;
	}
	@XmlElementWrapper( name = "coberturas" )
	@XmlElement( name = "cobertura" )
	public void setCoberturas(List<CoberturaDTO> coberturas) {
		this.coberturas = coberturas;
	}
	public List<String> getClausulas() {
		return clausulas;
	}
	@XmlElementWrapper( name = "clausulas" )
	@XmlElement( name = "clausula" )
	public void setClausulas(List<String> clausulas) {
		this.clausulas = clausulas;
	}
	public String getClausulasString() {
		String clausulas = "";
		int i = 0;
		for (String clausula : this.getClausulas()) {
			if (i == 0) {
				clausulas = clausulas + clausula;
			} else {
				clausulas = clausulas + " - " + clausula;
			}
			i++;
		}
		return clausulas;
	}
	

}
