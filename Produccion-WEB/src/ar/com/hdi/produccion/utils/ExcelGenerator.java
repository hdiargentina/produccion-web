package ar.com.hdi.produccion.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import ar.com.hdi.productores.domain.LineaAgrupadoDTO;
import ar.com.hdi.productores.domain.LineaDetalleProduccionDTO;
import ar.com.hdi.productores.domain.LineaDiarioDTO;
import ar.com.hdi.productores.domain.list.AgrupadoDTO;
import ar.com.hdi.productores.domain.list.DetalleProduccionDiariaList;
import ar.com.hdi.productores.domain.list.ProduccionAgrupadaList;
import ar.com.hdi.productores.domain.list.ProduccionDiariaList;
import ar.com.hdi.utils.HDIUtils;


public class ExcelGenerator {
	 private HSSFWorkbook wb;
	    
	    private CellStyle alinearRowDerecha(){
	    	CellStyle alineadoDerecha = wb.createCellStyle();
//	    	alineadoDerecha = setearEstiloEncabezado();
	    	alineadoDerecha.setAlignment(CellStyle.ALIGN_RIGHT);
	    	return alineadoDerecha;
	    }
	    
	    public void agregarEncabezadoVerde(Row row, HSSFSheet sheet) {
	    	Iterator<Cell> cellIterator = row.cellIterator();
			while(cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				cell.setCellStyle(setearEstiloEncabezado());
			}
	    }
	    
	    private void alinearDerecha(Row row, HSSFSheet sheet) {
			Iterator<Cell> cellIterator = row.cellIterator();
			while(cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				int columnIndex = cell.getColumnIndex();
				if(!(columnIndex == 0))
					sheet.autoSizeColumn(columnIndex);
				if(!(columnIndex == 0))
					cell.setCellStyle(alinearRowDerecha());
			}
		}
	    
	    private void chequearArchivo(String filename) {
	    	try {
				File archivo = new File(filename);
				if(archivo.exists()) {
					archivo.delete();
					archivo.createNewFile();
				} else {
					archivo.createNewFile();
				}
			} catch (IOException e1) {
				System.out.println(e1.getMessage());
			}
	    }
	    private HSSFCellStyle setearEstiloEncabezado() {
	    	HSSFCellStyle style = wb.createCellStyle();
	        style.setFillForegroundColor(HSSFColor.GREEN.index);
	        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	        HSSFFont font = wb.createFont();
	        font.setColor(HSSFColor.WHITE.index);
	        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	        style.setFont(font);
	        return style;
	    }
		public void generarXLSX(Object produccion, Date fechaProduccion, Map<String, List<String>> encabezados, String filename) {		
			chequearArchivo(filename);
			wb = new HSSFWorkbook();
	        HSSFSheet sheet = wb.createSheet("Hoja 1");
	        System.out.println("Creating excel");
	        if(produccion instanceof ProduccionDiariaList) {
	        	crearHojaListado((ProduccionDiariaList)produccion, fechaProduccion, encabezados, sheet);
	        	crearHojaTotales((ProduccionDiariaList)produccion, fechaProduccion, encabezados, sheet);
	        } else if(produccion instanceof ProduccionAgrupadaList) {
	        	crearHojaListadoAgrupado((ProduccionAgrupadaList)produccion, fechaProduccion, encabezados, sheet);
	        } else if (produccion instanceof DetalleProduccionDiariaList) {
	        	crearHojaListadoDetalle((DetalleProduccionDiariaList) produccion, fechaProduccion, encabezados, sheet);
	        }
	        crearArchivo(wb, filename);
	        wb = null;
	        System.out.println("Finish excel");
		}
		
		private void crearHojaListadoDetalle(DetalleProduccionDiariaList detalleProduccionDiaria, Date fechaProduccion, Map<String, List<String>> encabezados, HSSFSheet sheet) {
			crearEncabezadoDetalle(encabezados,sheet);
			int rownum = sheet.getLastRowNum()+1;
			for(LineaDetalleProduccionDTO linea : detalleProduccionDiaria.getLineas()) {
				HSSFRow row = sheet.createRow(rownum++);
				row.createCell(0).setCellValue(linea.getRama().longValue());
				row.createCell(1).setCellValue(linea.getPoliza().floatValue());
				row.createCell(2).setCellValue(linea.getSuplemento().floatValue());
				row.createCell(3).setCellValue(linea.getCertificado().floatValue());
				row.createCell(4).setCellValue(linea.getArticulo().floatValue());
				row.createCell(5).setCellValue(linea.getSuperPoliza().floatValue());
				row.createCell(6).setCellValue(linea.getAsegurado());
				row.createCell(7).setCellValue(linea.getOperacion());
				row.createCell(8).setCellValue(linea.getTipoOperacion());
				row.createCell(9).setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(linea.getFechaEmision()==null?new Date():linea.getFechaEmision()));
				row.createCell(10).setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(linea.getFechaVigencia()==null?new Date():linea.getFechaVigencia()));
				row.createCell(11).setCellValue(Double.parseDouble(linea.getLapso().toString()));
				row.createCell(12).setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(linea.getFechaPropuesta()==null?new Date():linea.getFechaPropuesta()));
				row.createCell(13).setCellValue(Double.parseDouble(linea.getLapsoPropuesta().toString()));
				row.createCell(14).setCellValue(Double.parseDouble(linea.getPrimaEmitida().toString()));
				row.createCell(15).setCellValue(Double.parseDouble(linea.getRecargosEmitidos().toString()));
				row.createCell(16).setCellValue(Double.parseDouble(linea.getImpuestosEmitidos().toString()));
				row.createCell(17).setCellValue(Double.parseDouble(linea.getPremioEmitido().toString()));
				row.createCell(18).setCellValue(Double.parseDouble(linea.getComisionesEmitidas().toString()));
				row.createCell(19).setCellValue(linea.getCodigoProductor().floatValue());
				row.createCell(20).setCellValue(linea.getNombreProductor());
			}
			ajustarAncho(21, sheet);
		}
		
		private void crearEncabezadoDetalle(Map<String, List<String>> encabezados,HSSFSheet sheet) {
			int i = 0;
			int rownum = sheet.getLastRowNum()+1;
			HSSFRow row = sheet.createRow(rownum++);
			for(String texto : encabezados.get("nivelUno")) {
				row.createCell(i).setCellValue(texto);
				i++;
			}
		}
		
		private void crearHojaListadoAgrupado(ProduccionAgrupadaList produccionAgrupada, Date fechaProduccion, Map<String, List<String>> encabezados, HSSFSheet sheet) {
			crearFilasCabecera(sheet, fechaProduccion);
			for(AgrupadoDTO agrupado : produccionAgrupada.getAgrupamiento()) {
				if(!agrupado.getLineaAgrupado().isEmpty()) {
					agregarTituloAgrupado(sheet, agrupado.getNombre());
					agregarFilasCabeceraTabla(sheet, encabezados, 5);
					int rowNum = sheet.getLastRowNum()+1;
					for(LineaAgrupadoDTO linea : agrupado.getLineaAgrupado()) {
						HSSFRow row = sheet.createRow(rowNum++);
			            row.createCell(0).setCellValue(linea.getRama());
			            row.createCell(1).setCellValue(linea.getCantidadDiaria().toString());
			            row.createCell(2).setCellValue(linea.getPrimaNetaDiaria().toString());
			            row.createCell(3).setCellValue(linea.getPorcentajeDiario().toString());
			            row.createCell(4).setCellValue(linea.getPrimaMasRecargoDiaria().toString());
			            row.createCell(5).setCellValue(linea.getPorcentajeComisionDiario().toString());
			            row.createCell(6).setCellValue(linea.getComisionDiario().toString());
			            row.createCell(7).setCellValue(linea.getCantidadMensual().toString());
			            row.createCell(8).setCellValue(linea.getPrimaNetaMensual().toString());
			            row.createCell(9).setCellValue(linea.getPorcentajeMensual().toString());
			            row.createCell(10).setCellValue(linea.getPrimaMasRecargoMensual().toString());
			            row.createCell(11).setCellValue(linea.getPorcentajeComisionMensual().toString());
			            row.createCell(12).setCellValue(linea.getComisionMensual().toString());
					}
				}
			}
		}
		
		private void agregarTituloAgrupado(HSSFSheet sheet, String nombre) {
			int rownum = sheet.getLastRowNum()+2;
			sheet.createRow(rownum++).createCell(0).setCellValue(nombre);
		}
		
		private void crearHojaListado(ProduccionDiariaList produccionDiaria, Date fechaProduccion, Map<String, List<String>> encabezados, HSSFSheet sheet) {
			crearEncabezadoProduccion(sheet, fechaProduccion, encabezados, 1);
			int rowNum = sheet.getLastRowNum()+1;
			for (LineaDiarioDTO linea : produccionDiaria.getLineasDiario().getLineas()) {
	        	HSSFRow row = sheet.createRow(rowNum++);
	            row.createCell(0).setCellValue(linea.getRamaDisplay());
	            row.createCell(1).setCellValue(linea.getCantidadNuevas().toString());
	            row.createCell(2).setCellValue(linea.getPrimaNueva().toString());
	            row.createCell(3).setCellValue(linea.getCantidadRenovaciones().toString());
	            row.createCell(4).setCellValue(linea.getPrimaRenovaciones().toString());
	            row.createCell(5).setCellValue(linea.getCantidadEndosoPositivo().toString());
	            row.createCell(6).setCellValue(linea.getPrimaEndosoPositivo().toString());
	            row.createCell(7).setCellValue(linea.getCantidadEndosoNegativo().toString());
	            row.createCell(8).setCellValue(linea.getPrimaEndosoNegativo().toString());
	            row.createCell(9).setCellValue(linea.getCantidadNeta().toString());
	            row.createCell(10).setCellValue(linea.getPrimaNeta().toString());
	            row.createCell(11).setCellValue(linea.getPorcentajePrimaMasExtra().toString());
	            row.createCell(12).setCellValue(linea.getPrimaMasExtra().toString());
	            row.createCell(13).setCellValue(linea.getPorcentajeComisiones().toString());
	            row.createCell(14).setCellValue(linea.getImporteComisiones().toString());
	            alinearDerecha(row, sheet);
	        }
			ajustarAncho(14, sheet);
		}
		
		private void crearHojaTotales(ProduccionDiariaList produccionDiaria, Date fechaProduccion, Map<String, List<String>> encabezados, HSSFSheet sheet) {
			int rowNum = sheet.getLastRowNum()+2;
	        	HSSFRow row = sheet.createRow(rowNum++);
	            row.createCell(0).setCellValue(produccionDiaria.getTotalDiario().getRamaDisplay());
	            row.createCell(1).setCellValue(produccionDiaria.getTotalDiario().getCantidadNuevas().toString());
	            row.createCell(2).setCellValue(produccionDiaria.getTotalDiario().getPrimaNueva().toString());
	            row.createCell(3).setCellValue(produccionDiaria.getTotalDiario().getCantidadRenovaciones().toString());
	            row.createCell(4).setCellValue(produccionDiaria.getTotalDiario().getPrimaRenovaciones().toString());
	            row.createCell(5).setCellValue(produccionDiaria.getTotalDiario().getCantidadEndosoPositivo().toString());
	            row.createCell(6).setCellValue(produccionDiaria.getTotalDiario().getPrimaEndosoPositivo().toString());
	            row.createCell(7).setCellValue(produccionDiaria.getTotalDiario().getCantidadEndosoNegativo().toString());
	            row.createCell(8).setCellValue(produccionDiaria.getTotalDiario().getPrimaEndosoNegativo().toString());
	            row.createCell(9).setCellValue(produccionDiaria.getTotalDiario().getCantidadNeta().toString());
	            row.createCell(10).setCellValue(produccionDiaria.getTotalDiario().getPrimaNeta().toString());
	            row.createCell(11).setCellValue(produccionDiaria.getTotalDiario().getPorcentajePrimaMasExtra().toString());
	            row.createCell(12).setCellValue(produccionDiaria.getTotalDiario().getPrimaMasExtra().toString());
	            row.createCell(13).setCellValue(produccionDiaria.getTotalDiario().getPorcentajeComisiones().toString());
	            row.createCell(14).setCellValue(produccionDiaria.getTotalDiario().getImporteComisiones().toString());
	            alinearDerecha(row, sheet);
		}
		
		private void crearArchivo(HSSFWorkbook wb, String filename) {
			try {
	            FileOutputStream outputStream = new FileOutputStream(filename);
	            wb.write(outputStream);
	            outputStream.close();
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}
		private void crearEncabezadoProduccion(HSSFSheet sheet, Date fechaProduccion, Map<String, List<String>> encabezados, int cantidadColumnas) {
			crearFilasCabecera(sheet, fechaProduccion);
			agregarFilasCabeceraTabla(sheet, encabezados, cantidadColumnas);
		}
		
		private void agregarFilasCabeceraTabla(HSSFSheet sheet, Map<String, List<String>> encabezados, int cantidadColumnas) {
			HSSFRow row2 = sheet.createRow(sheet.getLastRowNum()+1);
			HSSFRow row3 = sheet.createRow(sheet.getLastRowNum()+1);
			crearCabeceraProduccion1(row2, encabezados.get("nivelUno"),0, sheet, cantidadColumnas);
			crearCabeceraProduccion(row3, encabezados.get("nivelDos"),1, null);
		}
		
		private void crearFilasCabecera(HSSFSheet sheet, Date fechaProduccion) {
			HSSFRow titulo = sheet.createRow(0);
			titulo.createCell(0).setCellValue("Cierre General de Emisión");
			HSSFRow row1 = sheet.createRow(sheet.getLastRowNum()+1);
			row1.createCell(0).setCellValue("Resumen de producción. Información del lote del día: "+ new SimpleDateFormat("dd/MM/yyyy").format(HDIUtils.obtenerPrimerDiaMes(fechaProduccion)) + " al d¡a: " +  new SimpleDateFormat("dd/MM/yyyy").format(fechaProduccion));
			sheet.addMergedRegion(new CellRangeAddress(titulo.getRowNum(), titulo.getRowNum(), 0, 14));
			row1.createCell(11).setCellValue("*** Todos los Importes están expresados en Moneda corriente ***");
			sheet.addMergedRegion(new CellRangeAddress(row1.getRowNum(), row1.getRowNum(), 11, 17));
			sheet.addMergedRegion(new CellRangeAddress(row1.getRowNum(), row1.getRowNum(), 0, 9));
		}
		
		private void crearCabeceraProduccion1(HSSFRow row, List<String> cabecera, int i, HSSFSheet sheet, int cantidadColumnas) {
			for(String texto : cabecera) {
				if(texto.equals("Rama")){
					row.createCell(i).setCellValue(texto);
					i++;
				} else {
					row.createCell(i).setCellValue(texto);
					sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), i, i+cantidadColumnas));
					i+=cantidadColumnas+1;
				}
			}
		}
		
		private void crearCabeceraProduccion(HSSFRow row, List<String> cabecera, int i, HSSFSheet sheet) {
			for(String texto : cabecera) {
				row.createCell(i).setCellValue(texto);
				i++;
			}
		}
		
//		private void formatoTitulo(HSSFRow titulo) {
//			Iterator<Cell> cellIterator = titulo.cellIterator();
//			while(cellIterator.hasNext()) {
//				Cell cell = cellIterator.next();
//				cell.setCellStyle(setearFormatoTitulo());
//			}
//		}
		
//		private HSSFCellStyle setearFormatoTitulo() {
//			HSSFCellStyle style = wb.createCellStyle();
//	        HSSFFont font = wb.createFont();
//	        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
//	        font.setFontHeightInPoints(Short.parseShort("16"));
//	        style.setFont(font);
//	        return style;
//		}
		
		private void ajustarAncho(int cantidadColumnas, HSSFSheet sheet) {
			for(int i = 0; i< cantidadColumnas; i++) {
				sheet.autoSizeColumn(i);
			}
		}
}
