package ar.com.hdi.productores.services.remote.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import ar.com.hdi.productores.domain.IntermediarioDTO;
import ar.com.hdi.productores.domain.MensajeDTO;
import ar.com.hdi.productores.domain.list.MensajeList;
import ar.com.hdi.productores.services.RESTService;
import ar.com.hdi.productores.services.remote.MensajeService;

@Stateless
public class MensajeServiceImpl extends RESTService implements MensajeService, Serializable {
	private static final long serialVersionUID = 1L;

	@Override
	public List<MensajeDTO> obtenerMensajes(String empresa, String sucursal, IntermediarioDTO intermediario, IntermediarioDTO vendedor) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("empresa", empresa);
		params.put("sucursal", sucursal);
		params.put("nivt", vendedor.getNivel().toString());
		params.put("nivc", vendedor.getCodigo().toString());
		params.put("nit1", intermediario.getNivel().toString());
		params.put("niv1", intermediario.getCodigo().toString());
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(MensajeList.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MensajeList mensajes = (MensajeList)jaxbUnmarshaller.unmarshal(this.getRestService("intermediarios/mensajesPorProductor", params));
			return mensajes.getMensajes();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void cambiiarEstado(String empresa, String sucursal, IntermediarioDTO intermediario, IntermediarioDTO vendedor, List<MensajeDTO> mensajes, String status) {
		// TODO Auto-generated method stub
		for (MensajeDTO mensaje: mensajes) {
			Map<String, String> params = new HashMap<String, String>();
			params.put("empresa", empresa);
			params.put("sucursal", sucursal);
			params.put("nivt", vendedor.getNivel().toString());
			params.put("nivc", vendedor.getCodigo().toString());
			params.put("nit1", intermediario.getNivel().toString());
			params.put("niv1", intermediario.getCodigo().toString());
			params.put("idmsg", mensaje.getId());
			params.put("accion", status);
			this.getRestService("intermediarios/cambiarEstado", params);
			
		}
		
	}

	

}
