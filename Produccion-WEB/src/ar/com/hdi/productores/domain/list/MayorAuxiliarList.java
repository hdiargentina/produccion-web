package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.MayorAuxiliarDTO;

@XmlRootElement( name = "mayores" )
public class MayorAuxiliarList implements Serializable {
	private static final long serialVersionUID = 1L;
		
	private List<MayorAuxiliarDTO> mayores;
	private BigDecimal cantidad;
	
	public List<MayorAuxiliarDTO> getMayores() {
		return mayores;
	}
	@XmlElement( name = "mayor" )
	public void setMayores(List<MayorAuxiliarDTO> mayores) {
		this.mayores = mayores;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	@XmlElement( name = "cantidad" )
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public void add (MayorAuxiliarDTO mayor) {
		if (this.mayores == null) {
			this.mayores = new ArrayList<MayorAuxiliarDTO>();
		}
		this.mayores.add(mayor);
	}
		
	

}
