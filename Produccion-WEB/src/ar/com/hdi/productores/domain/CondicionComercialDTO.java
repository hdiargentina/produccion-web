package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import ar.com.hdi.utils.HDIUtils;

public class CondicionComercialDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal porcentajeComision;
	private BigDecimal importeComision;
	private BigDecimal porcentajeExtraPrima;
	private BigDecimal importeExtraPrima;
	private BigDecimal cantAumentar;
	private BigDecimal cantDisminuir;
	private int variacion;
	
	public CondicionComercialDTO(){}
	
	public CondicionComercialDTO(int variacion){
		this.variacion = variacion;
	}
	
	public BigDecimal getPorcentajeComision() {
		return porcentajeComision;
	}
	public void setPorcentajeComision(BigDecimal porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}
	public BigDecimal getImporteComision() {
		return importeComision;
	}
	public void setImporteComision(BigDecimal importeComision) {
		this.importeComision = importeComision;
	}
	public BigDecimal getPorcentajeExtraPrima() {
		return porcentajeExtraPrima;
	}
	public void setPorcentajeExtraPrima(BigDecimal porcentajeExtraPrima) {
		this.porcentajeExtraPrima = porcentajeExtraPrima;
	}
	public BigDecimal getImporteExtraPrima() {
		return importeExtraPrima;
	}
	public void setImporteExtraPrima(BigDecimal importeExtraPrima) {
		this.importeExtraPrima = importeExtraPrima;
	}
	public BigDecimal getCantAumentar() {
		return cantAumentar;
	}
	public void setCantAumentar(BigDecimal cantAumentar) {
		this.cantAumentar = cantAumentar;
	}
	public BigDecimal getCantDisminuir() {
		return cantDisminuir;
	}
	public void setCantDisminuir(BigDecimal cantDisminuir) {
		this.cantDisminuir = cantDisminuir;
	}
	public int getVariacion() {
		return variacion;
	}
	public void setVariacion(int variacion) {
		if (variacion != this.variacion) {
			int coeficiente = variacion - this.variacion;
			this.variacion = variacion;
			setPorcentajeComision(getPorcentajeComision().add(HDIUtils.toBigDecimal(coeficiente)));
			setPorcentajeExtraPrima(getPorcentajeExtraPrima().add(HDIUtils.toBigDecimal(coeficiente)));
		}
	}
	
	

}
