package ar.com.hdi.productores.domain.list;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.LineaDiarioDTO;

@XmlRootElement( name = "produccionDiaria" )
public class ProduccionDiariaList implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private LineaDiarioList lineasDiario;
	private LineaDiarioDTO totalDiario;
	
	public LineaDiarioList getLineasDiario() {
		return lineasDiario;
	}
	@XmlElement( name = "diario" )
	public void setLineasDiario(LineaDiarioList lineasDiario) {
		this.lineasDiario = lineasDiario;
	}
	public LineaDiarioDTO getTotalDiario() {
		return totalDiario;
	}
	@XmlElement( name = "total" )
	public void setTotalDiario(LineaDiarioDTO totalDiario) {
		this.totalDiario = totalDiario;
	}
	
}
