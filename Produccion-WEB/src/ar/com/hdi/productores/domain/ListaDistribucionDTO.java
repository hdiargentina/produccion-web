package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "configuracionMail" )
public class ListaDistribucionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String from;
	private String fromAddress;
	private String subject;
	private List<DestinatarioMailDTO> destinatarios;
	public String getFrom() {
		return from;
	}
	@XmlElement( name = "from")
	public void setFrom(String from) {
		this.from = from;
	}
	public String getFromAddress() {
		return fromAddress;
	}
	@XmlElement( name = "fromAddress")
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}
	public String getSubject() {
		return subject;
	}
	@XmlElement( name = "subject")
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public List<DestinatarioMailDTO> getDestinatarios() {
		return destinatarios;
	}
	
	@XmlElementWrapper( name = "destinatarios" )
	@XmlElement( name = "destinatario")
	public void setDestinatarios(List<DestinatarioMailDTO> destinatarios) {
		this.destinatarios = destinatarios;
	}
	
	
}
