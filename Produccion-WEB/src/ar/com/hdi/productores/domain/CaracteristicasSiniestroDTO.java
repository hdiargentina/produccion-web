package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class CaracteristicasSiniestroDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String tipoAccidente;
	private String colisionCon;
	private BigDecimal kmPorHora;
	private String detalle;
	private boolean denunciaPolicial;
	private String comisaria;
	private String caracteristicasLugar;
	private String domicilioTestigo;
	private boolean huboTestigo;
	private String otro;
	public String getTipoAccidente() {
		return tipoAccidente;
	}
	public void setTipoAccidente(String tipoAccidente) {
		this.tipoAccidente = tipoAccidente;
	}
	public String getColisionCon() {
		return colisionCon;
	}
	public void setColisionCon(String colisionCon) {
		this.colisionCon = colisionCon;
	}
	public BigDecimal getKmPorHora() {
		return kmPorHora;
	}
	public void setKmPorHora(BigDecimal kmPorHora) {
		this.kmPorHora = kmPorHora;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public boolean isDenunciaPolicial() {
		return denunciaPolicial;
	}
	public void setDenunciaPolicial(boolean denunciaPolicial) {
		this.denunciaPolicial = denunciaPolicial;
	}
	public boolean isHuboTestigo() {
		return huboTestigo;
	}
	public void setHuboTestigo(boolean huboTestigo) {
		this.huboTestigo = huboTestigo;
	}
	public String getOtro() {
		return otro;
	}
	public void setOtro(String otro) {
		this.otro = otro;
	}
	public String getComisaria() {
		return comisaria;
	}
	public void setComisaria(String comisaria) {
		this.comisaria = comisaria;
	}
	public String getCaracteristicasLugar() {
		return caracteristicasLugar;
	}
	public void setCaracteristicasLugar(String caracteristicasLugar) {
		this.caracteristicasLugar = caracteristicasLugar;
	}
	public String getDomicilioTestigo() {
		return domicilioTestigo;
	}
	public void setDomicilioTestigo(String domicilioTestigo) {
		this.domicilioTestigo = domicilioTestigo;
	}
	
}
