package ar.com.hdi.produccion.servlet;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import ar.com.hdi.utils.HDIProperties;

@WebServlet(name = "downloadServlet",urlPatterns = { "/download" })
public class DownloadServlet extends HttpServlet {

	private String downloadLocation = HDIProperties.getPropiedad("config.properties", "download_location");

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String fileName = request.getParameter("fileName");
		String contentType = request.getParameter("contentType");
		String name = request.getParameter("name");
		
		if (StringUtils.isBlank(name)) {
			name = fileName;
		}

		ServletOutputStream outStream = response.getOutputStream();
		response.setContentType("application/" + contentType);
		response.addHeader("content-disposition", "attachment; filename=" + name);

		Path path = Paths.get(downloadLocation +"/"+ fileName);

		if (!path.getParent().equals(Paths.get(downloadLocation))) {
			throw new ServletException(String.valueOf(HttpURLConnection.HTTP_FORBIDDEN));
		}

		try {
			byte[] bytes = Files.readAllBytes(path);
			outStream.write(bytes);
			outStream.flush();
			outStream.close();
		} catch (IOException e) {
			System.out.println(e.toString());
			
		} finally {
			try {
				if (outStream != null) {
					outStream.close(); 
				}
			} catch (Exception e) {
				System.out.println(e.toString());
			}
		}

	}
}
