package ar.com.hdi.productores.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement ( name = "documentoPdf" )
public class CertificadoPolizaDTO {
	private String descripcion;
	private String codigo;
	public String getDescripcion() {
		return descripcion;
	}
	@XmlElement ( name = "descripcion" )
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodigo() {
		return codigo;
	}
	@XmlElement ( name = "codigo" )
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((CertificadoPolizaDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }
}
