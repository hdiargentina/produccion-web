package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement( name = "linea" )
public class LineaDiarioDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String rama;
	private String descripcionRama;
	private String ramaAbreviada;
	private String ramaDisplay;
	private BigDecimal cantidadNuevas;
	private BigDecimal primaNueva;
	private BigDecimal cantidadRenovaciones;
	private BigDecimal primaRenovaciones;
	private BigDecimal cantidadEndosoPositivo;
	private BigDecimal primaEndosoPositivo;
	private BigDecimal cantidadEndosoNegativo;
	private BigDecimal primaEndosoNegativo;
	private BigDecimal cantidadNeta;
	private BigDecimal primaNeta;
	private BigDecimal porcentajePrimaMasExtra;
	private BigDecimal primaMasExtra;
	private BigDecimal porcentajeComisiones;
	private BigDecimal importeComisiones;
	public String getRama() {
		return rama;
	}
	@XmlElement( name = "rama" )
	public void setRama(String rama) {
		this.rama = rama;
	}
	public String getDescripcionRama() {
		return descripcionRama;
	}
	@XmlElement( name = "ramd" )
	public void setDescripcionRama(String descripcionRama) {
		this.descripcionRama = descripcionRama;
	}
	public String getRamaAbreviada() {
		return ramaAbreviada;
	}
	@XmlElement( name = "ramb" )
	public void setRamaAbreviada(String ramaAbreviada) {
		this.ramaAbreviada = ramaAbreviada;
	}
	public String getRamaDisplay() {
		return ramaDisplay;
	}
	@XmlElement( name = "ramaDisplay" )
	public void setRamaDisplay(String ramaDisplay) {
		this.ramaDisplay = ramaDisplay;
	}
	public BigDecimal getCantidadNuevas() {
		return cantidadNuevas;
	}
	@XmlElement( name = "cantidadNuevas" )
	public void setCantidadNuevas(BigDecimal cantidadNuevas) {
		this.cantidadNuevas = cantidadNuevas;
	}
	public BigDecimal getPrimaNueva() {
		return primaNueva;
	}
	@XmlElement( name = "primaNuevas" )
	public void setPrimaNueva(BigDecimal primaNueva) {
		this.primaNueva = primaNueva;
	}
	public BigDecimal getCantidadRenovaciones() {
		return cantidadRenovaciones;
	}
	@XmlElement( name = "cantidadRenovaciones" )
	public void setCantidadRenovaciones(BigDecimal cantidadRenovaciones) {
		this.cantidadRenovaciones = cantidadRenovaciones;
	}
	public BigDecimal getPrimaRenovaciones() {
		return primaRenovaciones;
	}
	@XmlElement( name = "primaRenovaciones" )
	public void setPrimaRenovaciones(BigDecimal primaRenovaciones) {
		this.primaRenovaciones = primaRenovaciones;
	}
	public BigDecimal getCantidadEndosoPositivo() {
		return cantidadEndosoPositivo;
	}
	@XmlElement( name = "cantidadEndPositivos" )
	public void setCantidadEndosoPositivo(BigDecimal cantidadEndosoPositivo) {
		this.cantidadEndosoPositivo = cantidadEndosoPositivo;
	}
	public BigDecimal getPrimaEndosoPositivo() {
		return primaEndosoPositivo;
	}
	@XmlElement( name = "primaEndPositivos" )
	public void setPrimaEndosoPositivo(BigDecimal primaEndosoPositivo) {
		this.primaEndosoPositivo = primaEndosoPositivo;
	}
	public BigDecimal getCantidadEndosoNegativo() {
		return cantidadEndosoNegativo;
	}
	@XmlElement( name = "cantidadEndNegativos" )
	public void setCantidadEndosoNegativo(BigDecimal cantidadEndosoNegativo) {
		this.cantidadEndosoNegativo = cantidadEndosoNegativo;
	}
	public BigDecimal getCantidadNeta() {
		return cantidadNeta;
	}
	@XmlElement( name = "cantidadNeta" )
	public void setCantidadNeta(BigDecimal cantidadNeta) {
		this.cantidadNeta = cantidadNeta;
	}
	public BigDecimal getPrimaNeta() {
		return primaNeta;
	}
	@XmlElement( name = "primaNeta" )
	public void setPrimaNeta(BigDecimal primaNeta) {
		this.primaNeta = primaNeta;
	}
	public BigDecimal getPorcentajePrimaMasExtra() {
		return porcentajePrimaMasExtra;
	}
	@XmlElement( name = "porcPrimaMasExtra" )
	public void setPorcentajePrimaMasExtra(BigDecimal porcentajePrimaMasExtra) {
		this.porcentajePrimaMasExtra = porcentajePrimaMasExtra;
	}
	public BigDecimal getPrimaMasExtra() {
		return primaMasExtra;
	}
	@XmlElement( name = "primaMasExtra" )
	public void setPrimaMasExtra(BigDecimal primaMasExtra) {
		this.primaMasExtra = primaMasExtra;
	}
	public BigDecimal getPorcentajeComisiones() {
		return porcentajeComisiones;
	}
	@XmlElement( name = "porcComisiones" )
	public void setPorcentajeComisiones(BigDecimal porcentajeComisiones) {
		this.porcentajeComisiones = porcentajeComisiones;
	}
	public BigDecimal getImporteComisiones() {
		return importeComisiones;
	}
	@XmlElement( name = "importeComisiones" )
	public void setImporteComisiones(BigDecimal importeComisiones) {
		this.importeComisiones = importeComisiones;
	}
	public BigDecimal getPrimaEndosoNegativo() {
		return primaEndosoNegativo;
	}
	@XmlElement( name = "primaEndNegativos")
	public void setPrimaEndosoNegativo(BigDecimal primaEndosoNegativo) {
		this.primaEndosoNegativo = primaEndosoNegativo;
	}
	
}
