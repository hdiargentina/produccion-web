package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.MensajeDTO;

@XmlRootElement( name = "mensajes" )
public class MensajeList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<MensajeDTO> mensajes;
	private int cantidadLeidos;
	private int cantidadNoLeidos;
	public List<MensajeDTO> getMensajes() {
		return mensajes;
	}
	@XmlElement( name = "mensaje" )
	public void setMensajes(List<MensajeDTO> mensajes) {
		this.mensajes = mensajes;
	}
	public int getCantidadLeidos() {
		return cantidadLeidos;
	}
	public void setCantidadLeidos(int cantidadLeidos) {
		this.cantidadLeidos = cantidadLeidos;
	}
	public int getCantidadNoLeidos() {
		return cantidadNoLeidos;
	}
	public void setCantidadNoLeidos(int cantidadNoLeidos) {
		this.cantidadNoLeidos = cantidadNoLeidos;
	}
	public void add (MensajeDTO mensaje) {
		if (this.mensajes == null) {
			this.mensajes = new ArrayList<MensajeDTO>();
		}
		this.mensajes.add(mensaje);
		if (mensaje.getStatus().equals("NL")) {
			this.cantidadNoLeidos++;
		} else {
			this.cantidadLeidos++;
		}
	}
	
	
	
}
