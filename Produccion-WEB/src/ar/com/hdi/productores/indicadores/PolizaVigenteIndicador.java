package ar.com.hdi.productores.indicadores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "polizasVigentes" )
public class PolizaVigenteIndicador extends Indicador implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<PolizaVigenteRama> ramas;

	
	public PolizaVigenteIndicador() {
		super("Producción Vigente", 0, "bar");
	}
	public List<PolizaVigenteRama> getRamas() {
		return ramas;
	}
	@XmlElement( name = "indicador" )
	public void setRamas(List<PolizaVigenteRama> ramas) {
		this.ramas = ramas;
	}
	
	public void add (PolizaVigenteRama rama) {
		if (this.ramas == null) {
			this.ramas = new ArrayList<PolizaVigenteRama>();
		}
		this.ramas.add(rama);
	}
	
	

}
