package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "cuota" )
public class CuotaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal suplemento;
	private BigDecimal numero;
	private BigDecimal numeroSubCuota;
	private Date fechaVencimiento;
	private BigDecimal importe;
	private Date fechaPago;
	private BigDecimal importePago;
	private String moneda;
	public BigDecimal getSuplemento() {
		return suplemento;
	}
	@XmlElement( name = "suplemento" )
	public void setSuplemento(BigDecimal suplemento) {
		this.suplemento = suplemento;
	}
	public BigDecimal getNumero() {
		return numero;
	}
	@XmlElement( name = "nroCuota" )
	public void setNumero(BigDecimal numero) {
		this.numero = numero;
	}
	public BigDecimal getNumeroSubCuota() {
		return numeroSubCuota;
	}
	@XmlElement( name = "nroSubCuota" )
	public void setNumeroSubCuota(BigDecimal numeroSubCuota) {
		this.numeroSubCuota = numeroSubCuota;
	}
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	@XmlElement( name = "fecVto" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public BigDecimal getImporte() {
		if (this.importe == null)
			this.importe = BigDecimal.ZERO;
		return importe;
	}
	@XmlElement( name = "importeCuota" )
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public Date getFechaPago() {
		return fechaPago;
	}
	@XmlElement( name = "fecPago" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public BigDecimal getImportePago() {
		return importePago;
	}
	@XmlElement( name = "importePago" )
	public void setImportePago(BigDecimal importePago) {
		this.importePago = importePago;
	}
	public String getMoneda() {
		return moneda;
	}
	@XmlElement( name = "moneda" )
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
	

}
