package ar.com.hdi.productores.services.remote;

import java.util.Map;

import ar.com.hdi.productores.domain.CertificadoPolizaDTO;

public interface DocumentosService {
	public Map<String, CertificadoPolizaDTO> obtenerCertificadosPdf();
}
