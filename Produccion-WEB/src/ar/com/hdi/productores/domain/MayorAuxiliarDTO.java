package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "mayor" )
public class MayorAuxiliarDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String cuit;
	private String nombre;
	private BigDecimal codigo;
	private BigDecimal numero;
	public String getCuit() {
		return cuit;
	}
	@XmlElement( name = "cuitInter" )
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "nombreInter" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public BigDecimal getCodigo() {
		return codigo;
	}
	@XmlElement( name = "codMayorAux" )
	public void setCodigor(BigDecimal codigo) {
		this.codigo = codigo;
	}
	public BigDecimal getNumero() {
		return numero;
	}
	@XmlElement( name = "nroMayorAux" )
	public void setNumero(BigDecimal numero) {
		this.numero = numero;
	}
	
	public String toString() {
		return this.codigo.toString()+"-"+this.numero;
	}
	
	
}
