package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "intermediario" )
public class IntermediarioDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String cuit;
	private BigDecimal nivel;
	private BigDecimal codigo;
	private String nombre;
	private BigDecimal codigoMayorAuxiliar;
	private BigDecimal numeroMayorAuxiliar;
	private DatosIntermediarioDTO datosIntermediario;
	private String clave;
	
	public String getCuit() {
		return cuit;
	}
	@XmlElement( name = "cuitInter" )
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public BigDecimal getNivel() {
		return nivel;
	}
	@XmlElement( name = "nivelInter" )
	public void setNivel(BigDecimal nivel) {
		this.nivel = nivel;
	}
	public BigDecimal getCodigo() {
		return codigo;
	}
	@XmlElement( name = "codigoInter" )
	public void setCodigo(BigDecimal codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "nombreInter" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public BigDecimal getCodigoMayorAuxiliar() {
		return codigoMayorAuxiliar;
	}
	@XmlElement( name = "codMayorAux" )
	public void setCodigoMayorAuxiliar(BigDecimal codigoMayorAuxiliar) {
		this.codigoMayorAuxiliar = codigoMayorAuxiliar;
	}
	public BigDecimal getNumeroMayorAuxiliar() {
		return numeroMayorAuxiliar;
	}
	@XmlElement( name = "nroMayorAux" )
	public void setNumeroMayorAuxiliar(BigDecimal numeroMayorAuxiliar) {
		this.numeroMayorAuxiliar = numeroMayorAuxiliar;
	}
	public DatosIntermediarioDTO getDatosIntermediario() {
		return datosIntermediario;
	}
	public void setDatosIntermediario(DatosIntermediarioDTO datosIntermediario) {
		this.datosIntermediario = datosIntermediario;
	}
	
	public String getClave() {
		return this.getNivel()+"-"+this.getCodigo();
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String toString() {
		return this.getNivel()+"-"+this.getCodigo();
	}
}
