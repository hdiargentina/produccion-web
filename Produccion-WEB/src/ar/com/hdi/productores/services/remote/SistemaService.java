package ar.com.hdi.productores.services.remote;

import ar.com.hdi.productores.domain.FechaSistemaDTO;

public interface SistemaService {
	public FechaSistemaDTO getFechasSistema(String empresa, String sucursal);
}
