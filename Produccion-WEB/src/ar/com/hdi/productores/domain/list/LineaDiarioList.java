package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.LineaDiarioDTO;

@XmlRootElement( name = "diario" )
public class LineaDiarioList implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<LineaDiarioDTO> lineas;

	public List<LineaDiarioDTO> getLineas() {
		return lineas;
	}
	@XmlElement( name="linea" )
	public void setLineas(List<LineaDiarioDTO> lineas) {
		this.lineas = lineas;
	}
}
