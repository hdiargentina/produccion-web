package ar.com.hdi.productores.services.remote;

import java.util.List;

import javax.ejb.Local;

import ar.com.hdi.productores.domain.IntermediarioDTO;
import ar.com.hdi.productores.domain.MensajeServicioDTO;
import ar.com.hdi.productores.domain.DatosQuincenaDTO;

@Local
public interface LibrosRubricadosService {
	public List<DatosQuincenaDTO> obtenerDatosQuincena();
	public MensajeServicioDTO solicitarLibros(String empresa, String sucursal, IntermediarioDTO intermediario, IntermediarioDTO vendedor, DatosQuincenaDTO quincena,
			int operacion, String formato);
}
