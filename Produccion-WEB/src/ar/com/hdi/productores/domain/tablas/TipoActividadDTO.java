package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;

public class TipoActividadDTO implements Serializable, Comparable<TipoActividadDTO> {
	private static final long serialVersionUID = 1L;
	
	private Long codigo;
	private String descripcion;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((TipoActividadDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }
	@Override
	public int compareTo(TipoActividadDTO o) {
		return this.getDescripcion().compareTo(o.getDescripcion());
	}
	

}
