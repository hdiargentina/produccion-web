package ar.com.hdi.productores.indicadores;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "carteraVigente" )
public class CarteraVigenteIndicador extends Indicador implements Serializable {
	
	public CarteraVigenteIndicador() {
		super("Cartera Vigente", 1, "bar");
		// TODO Auto-generated constructor stub
	}
	private static final long serialVersionUID = 1L;
	
	private BigDecimal autosSobreTotal;
	private BigDecimal hogarSobreTotal;
	private BigDecimal vidaSobreTotal;
	private BigDecimal restoSobreTotal;
	public BigDecimal getAutosSobreTotal() {
		return autosSobreTotal;
	}
	@XmlElement( name = "autosSobreTotal" )
	public void setAutosSobreTotal(BigDecimal autosSobreTotal) {
		this.autosSobreTotal = autosSobreTotal;
	}
	public BigDecimal getHogarSobreTotal() {
		return hogarSobreTotal;
	}
	@XmlElement( name = "hogarSobreTotal" )
	public void setHogarSobreTotal(BigDecimal hogarSobreTotal) {
		this.hogarSobreTotal = hogarSobreTotal;
	}
	public BigDecimal getVidaSobreTotal() {
		return vidaSobreTotal;
	}
	@XmlElement( name = "vidaSobreTotal" )
	public void setVidaSobreTotal(BigDecimal vidaSobreTotal) {
		this.vidaSobreTotal = vidaSobreTotal;
	}
	public BigDecimal getRestoSobreTotal() {
		return restoSobreTotal;
	}
	@XmlElement( name = "restoSobreTotal" )
	public void setRestoSobreTotal(BigDecimal restoSobreTotal) {
		this.restoSobreTotal = restoSobreTotal;
	}
	
	
	

}
