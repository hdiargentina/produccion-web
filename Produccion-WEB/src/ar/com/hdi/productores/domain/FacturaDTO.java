package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "factura" )
public class FacturaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal comision;
	private BigDecimal iva;
	private BigDecimal total;
	private BigDecimal retencionIva;
	private Date fecha;
	public BigDecimal getComision() {
		return comision;
	}
	@XmlElement( name = "comision" )
	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}
	public BigDecimal getIva() {
		return iva;
	}
	@XmlElement( name = "iva" )
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getTotal() {
		return total;
	}
	@XmlElement( name = "totalFactura" )
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getRetencionIva() {
		return retencionIva;
	}
	@XmlElement( name = "retIva" )
	public void setRetencionIva(BigDecimal retencionIva) {
		this.retencionIva = retencionIva;
	}
	public Date getFecha() {
		return fecha;
	}
	@XmlElement( name = "fechaFactura" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	

}
