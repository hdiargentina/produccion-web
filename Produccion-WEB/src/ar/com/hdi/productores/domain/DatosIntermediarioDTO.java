package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "intermediario" )
public class DatosIntermediarioDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private Date fechaNacimiento;
	private String tipoDocumento;
	private String numeroDocumento;
	private BigDecimal numeroIibb;
	private BigDecimal numeroMatricula;
	private String cuit;
	private String pais;
	private String provincia;
	private String localidad;
	private BigDecimal codigoPostal;
	private String domicilio;
	private String telefonoParticular;
	private String telefonoLaboral;
	private String correoElectronico;
	private String fax;
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "nombre" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	@XmlElement( name = "fechaNac" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	@XmlElement( name = "tipoDocumento" )
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	@XmlElement( name = "nroDocumento" )
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public BigDecimal getNumeroIibb() {
		return numeroIibb;
	}
	@XmlElement( name = "nroDeIibb" )
	public void setNumeroIibb(BigDecimal numeroIibb) {
		this.numeroIibb = numeroIibb;
	}
	public BigDecimal getNumeroMatricula() {
		return numeroMatricula;
	}
	@XmlElement( name = "nroMatricula" )
	public void setNumeroMatricula(BigDecimal numeroMatricula) {
		this.numeroMatricula = numeroMatricula;
	}
	public String getCuit() {
		return cuit;
	}
	@XmlElement( name = "nroDeCuit" )
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public String getPais() {
		return pais;
	}
	@XmlElement( name = "pais" )
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getProvincia() {
		return provincia;
	}
	@XmlElement( name = "provincia" )
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getLocalidad() {
		return localidad;
	}
	@XmlElement( name = "localidad" )
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public BigDecimal getCodigoPostal() {
		return codigoPostal;
	}
	@XmlElement( name = "codigoPostal" )
	public void setCodigoPostal(BigDecimal codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getDomicilio() {
		return domicilio;
	}
	@XmlElement( name = "domicilio" )
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getTelefonoParticular() {
		return telefonoParticular;
	}
	@XmlElement( name = "telefonoParticular" )
	public void setTelefonoParticular(String telefonoParticular) {
		this.telefonoParticular = telefonoParticular;
	}
	public String getTelefonoLaboral() {
		return telefonoLaboral;
	}
	@XmlElement( name = "telefonoLaboral" )
	public void setTelefonoLaboral(String telefonoLaboral) {
		this.telefonoLaboral = telefonoLaboral;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	@XmlElement( name = "correoElectronico" )
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public String getFax() {
		return fax;
	}
	@XmlElement( name = "nroFax" )
	public void setFax(String fax) {
		this.fax = fax;
	}
}
