package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;
import java.math.BigDecimal;

public class TipoOperacionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal tipo;
	private BigDecimal subTipoUsuario;
	private BigDecimal subTipoSistema;
	private String descripcion;
	public BigDecimal getTipo() {
		return tipo;
	}
	public void setTipo(BigDecimal tipo) {
		this.tipo = tipo;
	}
	public BigDecimal getSubTipoUsuario() {
		return subTipoUsuario;
	}
	public void setSubTipoUsuario(BigDecimal subTipoUsuario) {
		this.subTipoUsuario = subTipoUsuario;
	}
	public BigDecimal getSubTipoSistema() {
		return subTipoSistema;
	}
	public void setSubTipoSistema(BigDecimal subTipoSistema) {
		this.subTipoSistema = subTipoSistema;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}
