package ar.com.hdi.productores.domain.list;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


import ar.com.hdi.productores.domain.PreliquidacionPagoDTO;

@XmlRootElement( name = "Valores" )
public class PreliquidacionPagoList {
	private List<PreliquidacionPagoDTO> listaPagos;

	public List<PreliquidacionPagoDTO> getListaPagos() {
		return listaPagos;
	}
	@XmlElement ( name = "Valor" )
	public void setListaPagos(List<PreliquidacionPagoDTO> listaPagos) {
		this.listaPagos = listaPagos;
	}
	
}
