package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "agrupamiento" )
public class AgrupamientoList implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String nombre;
	private List<LineaAgrupadoDTO> lineas;

	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "nombre" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<LineaAgrupadoDTO> getLineas() {
		return lineas;
	}
	@XmlElement( name = "lineas" )
	public void setLineas(List<LineaAgrupadoDTO> lineas) {
		this.lineas = lineas;
	}
}
