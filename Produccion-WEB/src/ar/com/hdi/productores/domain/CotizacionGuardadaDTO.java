package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "cotizacion" )
public class CotizacionGuardadaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal token;
	private String asegurado;
	private String articulo;
	private Date fechaIngreso;
	private BigDecimal propuesta;
	private String operacion;
	private String estado;
	private String retomar;
	private String recotizar;
	private String certificado;
	private String tieneConstanciaRC;
	private BigDecimal vigenciaDesde;
	private BigDecimal vigenciaHasta;
	
	public BigDecimal getToken() {
		return token;
	}
	@XmlElement( name = "numeroCotizacion" )
	public void setToken(BigDecimal token) {
		this.token = token;
	}
	public String getAsegurado() {
		return asegurado;
	}
	@XmlElement( name = "asegurado" )
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	public String getArticulo() {
		return articulo;
	}
	@XmlElement( name = "descripcionArticulo" )
	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	@XmlElement( name = "fechaIngr" )
	@XmlJavaTypeAdapter(FechaAdapter.class)
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public BigDecimal getPropuesta() {
		return propuesta;
	}
	@XmlElement( name = "propuesta" )
	public void setPropuesta(BigDecimal propuesta) {
		this.propuesta = propuesta;
	}
	public String getOperacion() {
		return operacion;
	}
	@XmlElement( name = "operacion" )
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getEstado() {
		return estado;
	}
	@XmlElement( name = "estado" )
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getRetomar() {
		return retomar;
	}
	@XmlElement( name = "retomar" )
	public void setRetomar(String retomar) {
		this.retomar = retomar;
	}
	public String getRecotizar() {
		return recotizar;
	}
	@XmlElement( name = "recotizar" )
	public void setRecotizar(String recotizar) {
		this.recotizar = recotizar;
	}
	public String getCertificado() {
		return certificado;
	}
	@XmlElement( name = "certificado" )
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	public String getTieneConstanciaRC() {
		return tieneConstanciaRC;
	}
	@XmlElement( name = "constancia" )
	public void setTieneConstanciaRC(String tieneConstanciaRC) {
		this.tieneConstanciaRC = tieneConstanciaRC;
	}
	public BigDecimal getVigenciaDesde() {
		return vigenciaDesde;
	}
	@XmlElement( name = "vigenciaDesde" )
	public void setVigenciaDesde(BigDecimal vigenciaDesde) {
		this.vigenciaDesde = vigenciaDesde;
	}
	public BigDecimal getVigenciaHasta() {
		return vigenciaHasta;
	}
	@XmlElement( name = "vigenciaHasta" )
	public void setVigenciaHasta(BigDecimal vigenciaHasta) {
		this.vigenciaHasta = vigenciaHasta;
	}
	
}
