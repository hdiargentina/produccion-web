package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.util.Date;

public class InspeccionHogarDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nombreContacto;
	private String telefonoContacto;
	private String domicilioContacto;
	private String emailContacto;
	private String observaciones;
	private Date horaDesde;
	private Date horaHasta;
	public String getNombreContacto() {
		return nombreContacto;
	}
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	public String getTelefonoContacto() {
		return telefonoContacto;
	}
	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}
	public String getDomicilioContacto() {
		return domicilioContacto;
	}
	public void setDomicilioContacto(String domicilioContacto) {
		this.domicilioContacto = domicilioContacto;
	}
	public String getEmailContacto() {
		return emailContacto;
	}
	public void setEmailContacto(String emailContacto) {
		this.emailContacto = emailContacto;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Date getHoraDesde() {
		return horaDesde;
	}
	public void setHoraDesde(Date horaDesde) {
		this.horaDesde = horaDesde;
	}
	public Date getHoraHasta() {
		return horaHasta;
	}
	public void setHoraHasta(Date horaHasta) {
		this.horaHasta = horaHasta;
	}
	
	

}
