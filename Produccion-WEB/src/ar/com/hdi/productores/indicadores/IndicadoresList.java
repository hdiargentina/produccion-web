package ar.com.hdi.productores.indicadores;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "indicadores" )
public class IndicadoresList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private PolizaVigenteIndicador polizaVigenteIndicador;
	private CarteraVigenteIndicador carteraVigenteIndicador;
	private SiniestroIndicador siniestroIndicador;
	private CotizacionIndicador cotizacionIndicador;
//	private CobranzaIndicador cobranzaIndicador;

	public PolizaVigenteIndicador getPolizaVigenteIndicador() {
		return polizaVigenteIndicador;
	}
	@XmlElement( name = "polizasVigentes" )
	public void setPolizaVigenteIndicador(
			PolizaVigenteIndicador polizaVigenteIndicador) {
		this.polizaVigenteIndicador = polizaVigenteIndicador;
	}
	public CarteraVigenteIndicador getCarteraVigenteIndicador() {
		return carteraVigenteIndicador;
	}
	@XmlElement( name = "carteraVigente" )
	public void setCarteraVigenteIndicador(CarteraVigenteIndicador carteraVigenteIndicador) {
		this.carteraVigenteIndicador = carteraVigenteIndicador;
	}
	public SiniestroIndicador getSiniestroIndicador() {
		return siniestroIndicador;
	}
	@XmlElement( name = "siniestros" )
	public void setSiniestroIndicador(SiniestroIndicador siniestroIndicador) {
		this.siniestroIndicador = siniestroIndicador;
	}
	public CotizacionIndicador getCotizacionIndicador() {
		return cotizacionIndicador;
	}
	@XmlElement( name = "cotizacionesWeb" )
	public void setCotizacionIndicador(CotizacionIndicador cotizacionIndicador) {
		this.cotizacionIndicador = cotizacionIndicador;
	}
//	public CobranzaIndicador getCobranzaIndicador() {
//		return cobranzaIndicador;
//	}
//	@XmlElement( name = "cobranzas" )
//	public void setCobranzaIndicador(CobranzaIndicador cobranzaIndicador) {
//		this.cobranzaIndicador = cobranzaIndicador;
//	}
	
}
