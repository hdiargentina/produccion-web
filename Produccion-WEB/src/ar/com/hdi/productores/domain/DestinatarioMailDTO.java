package ar.com.hdi.productores.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "destinatario" )
public class DestinatarioMailDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String to;
	private String toAddress;
	private String toType;
	public String getTo() {
		return to;
	}
	@XmlElement( name = "to")
	public void setTo(String to) {
		this.to = to;
	}
	public String getToAddress() {
		return toAddress;
	}
	@XmlElement( name = "toAddress")
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}
	public String getToType() {
		return toType;
	}
	@XmlElement( name = "toType")
	public void setToType(String toType) {
		this.toType = toType;
	}
	
	

}
