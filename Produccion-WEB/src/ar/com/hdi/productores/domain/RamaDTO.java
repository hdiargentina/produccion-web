package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import ar.com.hdi.productores.domain.enume.TipoRama;

public class RamaDTO implements Serializable, Comparable<RamaDTO> {
	private static final long serialVersionUID = 1L;
	
	private Long codigo;
	private String descripcion;
	private BigDecimal arse;
	private TipoRama tipoRama;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getArse() {
		return arse;
	}
	public void setArse(BigDecimal arse) {
		this.arse = arse;
	}
	public TipoRama getTipoRama() {
		return tipoRama;
	}
	public void setTipoRama(TipoRama tipoRama) {
		this.tipoRama = tipoRama;
	}
	public boolean isRamaVida() {
		return (this.tipoRama.equals(TipoRama.ACCIDENTESPERSONALES));
	}
	public boolean isRamaAuto() {
		return (this.tipoRama.equals(TipoRama.AUTO));
	}
	public boolean isRamaCasco() {
		return (this.tipoRama.equals(TipoRama.TRANSPORTESCASCOS));
	}
	public boolean isRamaRiesgoVario() {
		return (this.tipoRama.equals(TipoRama.HOGAR) || this.tipoRama.equals(TipoRama.INTEGRALDECOMERCIO) || this.tipoRama.equals(TipoRama.INTEGRALDECONSORCIO) || this.tipoRama.equals(TipoRama.OTROS) || this.tipoRama.equals(TipoRama.TRANSPORTESDEMERCADERIAS));
	}
	public String getIconRama() {
		if (this.tipoRama.equals(TipoRama.HOGAR)) {
			return "hogar";
		}else if (this.tipoRama.equals(TipoRama.AUTO)) {
			return "auto-3";
		} else if (this.tipoRama.equals(TipoRama.ACCIDENTESPERSONALES)) {
			return "vida";
		} else if (this.tipoRama.equals(TipoRama.TRANSPORTESCASCOS)) {
			return "embarcacion";
		} else if (this.tipoRama.equals(TipoRama.TRANSPORTESDEMERCADERIAS)) {
			return "mercaderia";
		} else if (this.tipoRama.equals(TipoRama.INTEGRALDECOMERCIO)) {
			return "comercio";
		} else if (this.tipoRama.equals(TipoRama.INTEGRALDECONSORCIO)) {
			return "consorcio";
		} else {
			return "hogar";
		}
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((RamaDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }
	@Override
	public int compareTo(RamaDTO o) {
		return this.getDescripcion().compareTo(o.getDescripcion());
	}
}
