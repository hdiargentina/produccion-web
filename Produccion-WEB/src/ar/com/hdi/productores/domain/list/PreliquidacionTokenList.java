package ar.com.hdi.productores.domain.list;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "preliquidacion" )
public class PreliquidacionTokenList {
	private BigDecimal numero;
	private Date fechaHasta;
	public BigDecimal getNumero() {
		return numero;
	}
	@XmlElement( name = "numero" )
	public void setNumero(BigDecimal numero) {
		this.numero = numero;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	@XmlElement( name = "fechaHasta" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	
}
