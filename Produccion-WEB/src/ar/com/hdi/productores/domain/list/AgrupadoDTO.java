package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.LineaAgrupadoDTO;

@XmlRootElement ( name = "agrupamiento" )
public class AgrupadoDTO  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<LineaAgrupadoDTO> lineaAgrupado;
	private String nombre;
	public List<LineaAgrupadoDTO> getLineaAgrupado() {
		return lineaAgrupado;
	}
	@XmlElementWrapper( name = "lineas" )
	@XmlElement( name = "linea" )
	public void setLineaAgrupado(List<LineaAgrupadoDTO> lineaAgrupado) {
		this.lineaAgrupado = lineaAgrupado;
	}
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "nombre" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	} 
	
	
}
