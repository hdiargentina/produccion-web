package ar.com.hdi.productores.domain;

import java.io.Serializable;

public class CaracteristicaHogarDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long codigo;
	private String descripcion;
	private boolean tiene;
	private boolean aplicaBonificacion;
	private boolean modificable;
	private String codigo_equivalencia;
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isTiene() {
		return tiene;
	}
	public void setTiene(boolean tiene) {
		this.tiene = tiene;
	}
	public boolean isAplicaBonificacion() {
		return aplicaBonificacion;
	}
	public void setAplicaBonificacion(boolean aplicaBonificacion) {
		this.aplicaBonificacion = aplicaBonificacion;
	}
	public boolean isModificable() {
		return modificable;
	}
	public void setModificable(boolean modificable) {
		this.modificable = modificable;
	}
	public String getCodigo_equivalencia() {
		return codigo_equivalencia;
	}
	public void setCodigo_equivalencia(String codigo_equivalencia) {
		this.codigo_equivalencia = codigo_equivalencia;
	}
	
	
	

}
