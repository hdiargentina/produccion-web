package ar.com.hdi.productores.domain.tablas;

public class CausaSiniestroDTO {
	private Long codigo;
	private String codigo_equivalencia;
	private String descripcion;
	private String descripcionFrances;
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescripcionEnFrances() {
		return descripcionFrances;
	}
	public void setDescripcionFrances(String descripcionFrances) {
		this.descripcionFrances = descripcionFrances;
	}
	public String getCodigo_equivalencia() {
		return codigo_equivalencia;
	}
	public void setCodigo_equivalencia(String codigo_equivalencia) {
		this.codigo_equivalencia = codigo_equivalencia;
	}
	public String getDescripcionFrances() {
		return descripcionFrances;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo_equivalencia != null)
            ? codigo_equivalencia.equals(((CausaSiniestroDTO) other).codigo_equivalencia)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo_equivalencia != null) 
            ? (getClass().hashCode() + codigo_equivalencia.hashCode())
            : super.hashCode();
    }
}
