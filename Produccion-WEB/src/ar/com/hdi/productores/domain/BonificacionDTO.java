package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "bonificacionAuto" )
public class BonificacionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private BigDecimal porcentaje;
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "bonificacionNombre" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public BigDecimal getPorcentaje() {
		return porcentaje;
	}
	@XmlElement( name = "bonificacionPorcen" )
	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}
	
	

}
