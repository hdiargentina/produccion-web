package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class LineaDetalleProduccionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private BigDecimal rama;
	private BigDecimal poliza;
	private BigDecimal suplemento;
	private BigDecimal certificado;
	private BigDecimal articulo;
	private BigDecimal superPoliza;;
	private String asegurado;
	private String tipoOperacion;
	private String operacion;
	private Date fechaEmision;
	private Date fechaVigencia;
	private BigDecimal lapso;
	private Date fechaPropuesta;
	private BigDecimal lapsoPropuesta;
	private BigDecimal primaEmitida;
	private BigDecimal recargosEmitidos;
	private BigDecimal impuestosEmitidos;
	private BigDecimal premioEmitido;
	BigDecimal  comisionesEmitidas;
	private BigDecimal codigoProductor;
	private String nombreProductor;
	public BigDecimal getRama() {
		return rama;
	}
	@XmlElement( name = "rama")
	public void setRama(BigDecimal rama) {
		this.rama = rama;
	}
	public BigDecimal getPoliza() {
		return poliza;
	}
	@XmlElement( name = "poliza")
	public void setPoliza(BigDecimal poliza) {
		this.poliza = poliza;
	}
	public BigDecimal getSuplemento() {
		return suplemento;
	}
	@XmlElement( name = "suplemento")
	public void setSuplemento(BigDecimal suplemento) {
		this.suplemento = suplemento;
	}
	public BigDecimal getCertificado() {
		return certificado;
	}
	@XmlElement( name = "certificado")
	public void setCertificado(BigDecimal certificado) {
		this.certificado = certificado;
	}
	public BigDecimal getArticulo() {
		return articulo;
	}
	@XmlElement( name = "articulo")
	public void setArticulo(BigDecimal articulo) {
		this.articulo = articulo;
	}
	public BigDecimal getSuperPoliza() {
		return superPoliza;
	}
	@XmlElement( name = "superpoliza")
	public void setSuperPoliza(BigDecimal superPoliza) {
		this.superPoliza = superPoliza;
	}
	public String getAsegurado() {
		return asegurado;
	}
	@XmlElement( name = "asegurado")
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	@XmlElement( name = "tipoOperacion")
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public String getOperacion() {
		return operacion;
	}
	@XmlElement( name = "operacion")
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public Date getFechaEmision() {
		return fechaEmision;
	}
	@XmlElement( name = "fechaEmision")
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public Date getFechaVigencia() {
		return fechaVigencia;
	}
	@XmlElement( name = "fechaVigencia")
	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}
	public BigDecimal getLapso() {
		return lapso;
	}
	@XmlElement( name = "lapso")
	public void setLapso(BigDecimal lapso) {
		this.lapso = lapso;
	}
	public Date getFechaPropuesta() {
		return fechaPropuesta;
	}
	@XmlElement( name = "fechaPropuesta")
	public void setFechaPropuesta(Date fechaPropuesta) {
		this.fechaPropuesta = fechaPropuesta;
	}
	public BigDecimal getLapsoPropuesta() {
		return lapsoPropuesta;
	}
	@XmlElement( name = "lapsoPropuesta")
	public void setLapsoPropuesta(BigDecimal lapsoPropuesta) {
		this.lapsoPropuesta = lapsoPropuesta;
	}
	public BigDecimal getPrimaEmitida() {
		return primaEmitida;
	}
	@XmlElement( name = "primaEmitida")
	public void setPrimaEmitida(BigDecimal primaEmitida) {
		this.primaEmitida = primaEmitida;
	}
	public BigDecimal getRecargosEmitidos() {
		return recargosEmitidos;
	}
	@XmlElement( name = "recargosEmitidos")
	public void setRecargosEmitidos(BigDecimal recargosEmitidos) {
		this.recargosEmitidos = recargosEmitidos;
	}
	public BigDecimal getImpuestosEmitidos() {
		return impuestosEmitidos;
	}
	@XmlElement( name = "impuestosEmitidos")
	public void setImpuestosEmitidos(BigDecimal impuestosEmitidos) {
		this.impuestosEmitidos = impuestosEmitidos;
	}
	public BigDecimal getPremioEmitido() {
		return premioEmitido;
	}
	@XmlElement( name = "premioEmitido")
	public void setPremioEmitido(BigDecimal premioEmitido) {
		this.premioEmitido = premioEmitido;
	}
	public BigDecimal getComisionesEmitidas() {
		return comisionesEmitidas;
	}
	@XmlElement( name = "comisionesEmitidas")
	public void setComisionesEmitidas(BigDecimal comisionesEmitidas) {
		this.comisionesEmitidas = comisionesEmitidas;
	}
	public BigDecimal getCodigoProductor() {
		return codigoProductor;
	}
	@XmlElement( name = "codigoProductor")
	public void setCodigoProductor(BigDecimal codigoProductor) {
		this.codigoProductor = codigoProductor;
	}
	public String getNombreProductor() {
		return nombreProductor;
	}
	@XmlElement( name = "nombreProductor")
	public void setNombreProductor(String nombreProductor) {
		this.nombreProductor = nombreProductor;
	}
	
}
