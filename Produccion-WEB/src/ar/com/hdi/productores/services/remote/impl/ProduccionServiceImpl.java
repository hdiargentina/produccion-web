package ar.com.hdi.productores.services.remote.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import ar.com.hdi.productores.domain.list.DetalleProduccionDiariaList;
import ar.com.hdi.productores.domain.list.ProduccionAgrupadaList;
import ar.com.hdi.productores.domain.list.ProduccionDiariaList;
import ar.com.hdi.productores.services.RESTService;
import ar.com.hdi.productores.services.remote.ProduccionService;

@ManagedBean(name="produccionService")
@SessionScoped
public class ProduccionServiceImpl extends RESTService implements ProduccionService, Serializable{

	private static final long serialVersionUID = 1L;
	@Override
	public ProduccionDiariaList obtenerDatosProduccionPorFecha(String empresa, String sucursal, Date fecha) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("empresa", empresa);
		params.put("sucursal", sucursal);
		params.put("fecha", dateToString(fecha));
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(ProduccionDiariaList.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			ProduccionDiariaList produccionDiaria = (ProduccionDiariaList)jaxbUnmarshaller.unmarshal(this.getRestService("listadosProduccionApp/produccionDiaria", params));
			return produccionDiaria;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public ProduccionDiariaList obtenerDatosProduccionAcumuladaPorFecha(String empresa, String sucursal, Date fecha) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("empresa", empresa);
		params.put("sucursal", sucursal);
		params.put("fecha", dateToString(fecha));
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(ProduccionDiariaList.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			ProduccionDiariaList produccionDiaria = (ProduccionDiariaList)jaxbUnmarshaller.unmarshal(this.getRestService("listadosProduccionApp/produccionAcumulada", params));
			return produccionDiaria;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public ProduccionAgrupadaList obtenerDatosProduccionAgrupadaPorFecha(String empresa, String sucursal, Date fecha) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("empresa", empresa);
		params.put("sucursal", sucursal);
		params.put("fecha", dateToString(fecha));
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(ProduccionAgrupadaList.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			ProduccionAgrupadaList produccionAgrupada = (ProduccionAgrupadaList)jaxbUnmarshaller.unmarshal(this.getRestService("listadosProduccionApp/produccionAgrupada", params));
			return produccionAgrupada;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public DetalleProduccionDiariaList obtenerDetalleProduccionDiariaPorFecha(String empresa, String sucursal, Date fecha) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("empresa", empresa);
		params.put("sucursal", sucursal);
		params.put("fecha", dateToString(fecha));
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(DetalleProduccionDiariaList.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			DetalleProduccionDiariaList detalle = (DetalleProduccionDiariaList)jaxbUnmarshaller.unmarshal(this.getRestService("listadosProduccionApp/detalleProduccionDiaria", params));
			return detalle;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
