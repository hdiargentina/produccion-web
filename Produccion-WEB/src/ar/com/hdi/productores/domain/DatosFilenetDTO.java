package ar.com.hdi.productores.domain;


public class DatosFilenetDTO {
	protected String urlFilenet;
	protected String desktop;
	protected String repositorio;
	protected String docId;
	protected String template;
	protected String version;
	protected String nivel;
	protected String codigo;
	
	
	public String getUrlFilenet() {
		return urlFilenet;
	}
	public void setUrlFilenet(String urlFilenet) {
		this.urlFilenet = urlFilenet;
	}
	public String getDesktop() {
		return desktop;
	}
	public void setDesktop(String desktop) {
		this.desktop = desktop;
	}
	public String getRepositorio() {
		return repositorio;
	}
	public void setRepositorio(String repositorio) {
		this.repositorio = repositorio;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
}
