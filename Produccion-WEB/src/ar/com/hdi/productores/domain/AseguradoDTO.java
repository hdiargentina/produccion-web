package ar.com.hdi.productores.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "aseguradoPoliza" )
public class AseguradoDTO extends PersonaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String pais;
	private String codigoPais;
	private String provincia;
	private String codigoProvincia;
	private String localidad;
	private String sufijoCodigoPostal;
	private String tipoDocumento;
	private String codigoTipoDocumento;
	
	@XmlElement( name = "codigoDocumento")
	public String getCodigoTipoDocumento() {
		return codigoTipoDocumento;
	}
	public void setCodigoTipoDocumento(String codigoTipoDocumento) {
		this.codigoTipoDocumento = codigoTipoDocumento;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	@XmlElement( name = "tipoDocumento")
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getPais() {
		return pais;
	}
	@XmlElement( name = "pais")
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getProvincia() {
		return provincia;
	}
	@XmlElement( name = "provincia")
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getLocalidad() {
		return localidad;
	}
	@XmlElement( name = "localidad")
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public String getCodigoPais() {
		return codigoPais;
	}
	@XmlElement( name = "codigoPais")
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	public void setCodigoProvincia(String codigoProvincia) {
		this.codigoProvincia = codigoProvincia;
	}
	@XmlElement( name = "codigoProvincia")
	public String getCodigoProvincia() {
		return codigoProvincia;
	}
	public String getSufijoCodigoPostal() {
		return sufijoCodigoPostal;
	}
	@XmlElement( name = "sufijoCodigoPostal" )
	public void setSufijoCodigoPostal(String sufijoCodigoPostal) {
		this.sufijoCodigoPostal = sufijoCodigoPostal;
	}
}
