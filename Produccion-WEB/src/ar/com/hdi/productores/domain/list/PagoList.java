package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.PagoDTO;

@XmlRootElement( name = "pagosSiniestros" )
public class PagoList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<PagoDTO> pagos;
	private BigDecimal cantidad;
	public List<PagoDTO> getPagos() {
		return pagos;
	}
	@XmlElement( name = "pago" )
	public void setPagos(List<PagoDTO> pagos) {
		this.pagos = pagos;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	@XmlElement( name = "cantidad" )
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	
	public void add (PagoDTO pago) {
		if (this.pagos == null) {
			this.pagos = new ArrayList<PagoDTO>();
		}
		this.pagos.add(pago);
	}

}
