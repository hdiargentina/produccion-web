package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;
import java.math.BigDecimal;

public class PlanDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long codigo;
	private String codigo_equivalencia;
	private String descripcionCorta;
	private String descripcionLarga;
	private BigDecimal tarifa; 
	private String inciso1; 
	private String inciso2; 
	private String descripcionTarifa; 
	private BigDecimal agravamiento;
	private boolean abierto;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getCodigo_equivalencia() {
		return codigo_equivalencia;
	}
	public void setCodigo_equivalencia(String codigo_equivalencia) {
		this.codigo_equivalencia = codigo_equivalencia;
	}
	public String getDescripcionCorta() {
		return descripcionCorta;
	}
	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}
	public String getDescripcionLarga() {
		return descripcionLarga;
	}
	public void setDescripcionLarga(String descripcionLarga) {
		this.descripcionLarga = descripcionLarga;
	}
	public BigDecimal getTarifa() {
		return tarifa;
	}
	public void setTarifa(BigDecimal tarifa) {
		this.tarifa = tarifa;
	}
	public String getInciso1() {
		return inciso1;
	}
	public void setInciso1(String inciso1) {
		this.inciso1 = inciso1;
	}
	public String getInciso2() {
		return inciso2;
	}
	public void setInciso2(String inciso2) {
		this.inciso2 = inciso2;
	}
	public String getDescripcionTarifa() {
		return descripcionTarifa;
	}
	public void setDescripcionTarifa(String descripcionTarifa) {
		this.descripcionTarifa = descripcionTarifa;
	}
	public BigDecimal getAgravamiento() {
		return agravamiento;
	}
	public void setAgravamiento(BigDecimal agravamiento) {
		this.agravamiento = agravamiento;
	}
	
	
	public boolean isAbierto() {
		return abierto;
	}
	public void setAbierto(boolean abierto) {
		this.abierto = abierto;
	}
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo_equivalencia != null)
            ? codigo_equivalencia.equals(((PlanDTO) other).codigo_equivalencia)
            : (other == this);
    }
    @Override
    public int hashCode() {
        return (codigo_equivalencia != null) 
            ? (getClass().hashCode() + codigo_equivalencia.hashCode())
            : super.hashCode();
    }
	@Override
	public String toString() {
		return this.codigo.toString() + " - " + this.descripcionLarga;
	}
    
    
	
	
	
}
