package ar.com.hdi.utils;

import java.io.File;
import java.io.FileInputStream;

import com.filenet.api.collection.ContentElementList;
import com.filenet.api.constants.AutoClassify;
import com.filenet.api.constants.CheckinType;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Connection;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.property.Properties;
//import org.apache.logging.log4j.Logger;
//import org.apache.logging.log4j.LogManager;

public class FilenetUpload {
	public static void main(String[] args) {
		subirArchivosAFileNet();
	}
	
	public static void subirArchivosAFileNet() {
		
		String username = HDIProperties.getPropiedad(FilenetConstants.FILENET_PROPERTIES, "username");
		String password= HDIProperties.getPropiedad(FilenetConstants.FILENET_PROPERTIES, "password");
		String url = HDIProperties.getPropiedad(FilenetConstants.FILENET_PROPERTIES, "url");
		String objectStore = HDIProperties.getPropiedad(FilenetConstants.FILENET_PROPERTIES, "objectstore");
		String path = HDIProperties.getPropiedad(FilenetConstants.FILENET_PROPERTIES, "path");
		
		File[] fileList = new File(path).listFiles();
		
		String temppath = "";
		
		Connection conn = FilenetUtilities.Login(username, password, url);
		ObjectStore os = FilenetUtilities.getObjectStore(conn, objectStore);
		FilenetUtilities.procesedFolder(path);
		
		
		for (int i=0;i < fileList.length;i++) {
            
			if(fileList[i].isFile())
			{
			
				String shortname[] = fileList[i].getName().split("_");
				String classname = "";
				int temp_num = -1;
				
				switch (shortname[0].toUpperCase()) {
				case "PREDENUNCIA":
					classname = FilenetConstants.PREDENUNCIAS;
					temp_num=0;
					break;
				default:
					break; 
				}
				
				Document doc = Factory.Document.createInstance(os, classname);
				
				try	{
				
				ContentTransfer ct = Factory.ContentTransfer.createInstance();
				
				ct.setCaptureSource(new FileInputStream(fileList[i]));
				
				ct.set_RetrievalName(fileList[i].getName());
				
				ContentElementList cel = Factory.ContentElement.createList();
				
				cel.add(ct);
				
				doc.set_ContentElements(cel);
				
				Properties docprops = doc.getProperties();
				
				// Agrego las properties al documento
				documentAddProps(docprops, temp_num, fileList[i], i, shortname);
				
				doc.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
				
				doc.save(RefreshMode.NO_REFRESH);
				
				temppath = fileList[i].getPath();
				
				fileList[i].renameTo(new File(path + "/" + FilenetConstants.PROCESED_FOLDER_NAME + "/" + fileList[i].getName()));
				
				}catch(Exception ex){
					System.out.println(ex.getMessage());
					
					if(temppath.equals(fileList[i].getPath())){
						doc.delete();
						doc.save(RefreshMode.REFRESH);
						System.out.println("El archivo " + fileList[i].getName() + " no pudo ser almacenado");
					}	
				}
			}
        }	
	}
	
	private static void documentAddProps(Properties docprops, int temp_num, File file, int i, String shortname[]) {
		docprops.putValue(FilenetConstants.DOCUMENT_TITLE, file.getName());
		switch(temp_num){
		case 0:
			docprops.putValue(FilenetConstants.NUMEROPREDENUNCIA, shortname[1]);
			break;
		}
	}
}
