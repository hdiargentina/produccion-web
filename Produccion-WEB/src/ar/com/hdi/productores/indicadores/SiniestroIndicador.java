package ar.com.hdi.productores.indicadores;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "siniestros" )
public class SiniestroIndicador extends Indicador implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal cantidadSiniestroAuto;
	private BigDecimal cantidadVigneciaAuto;
	private BigDecimal cantidadSiniestroHogar;
	private BigDecimal cantidadVigenciaHogar;
	private BigDecimal cantidadSiniestroVida;
	private BigDecimal cantidadVigenciaVida;
	private BigDecimal canitdadSiniestroResto;
	private BigDecimal cantidadVigenciaResto;
	private BigDecimal siniestroAutoSobreVigencia;
	private BigDecimal siniestroHogarSobreVigencia;
	private BigDecimal siniestroVidaSobreVigencia;
	private BigDecimal siniestroRestoSobreVigencia;
	
	public SiniestroIndicador() {
		super("Siniestros", 0, "bar");
	}
	
	public BigDecimal getCantidadSiniestroAuto() {
		return cantidadSiniestroAuto;
	}
	@XmlElement( name = "cantStrAutos" )
	public void setCantidadSiniestroAuto(BigDecimal cantidadSiniestroAuto) {
		this.cantidadSiniestroAuto = cantidadSiniestroAuto;
	}
	public BigDecimal getCantidadVigneciaAuto() {
		return cantidadVigneciaAuto;
	}
	@XmlElement( name = "cantVigAutos" )
	public void setCantidadVigneciaAuto(BigDecimal cantidadVigneciaAuto) {
		this.cantidadVigneciaAuto = cantidadVigneciaAuto;
	}
	public BigDecimal getCantidadSiniestroHogar() {
		return cantidadSiniestroHogar;
	}
	@XmlElement( name = "cantStrHogar" )
	public void setCantidadSiniestroHogar(BigDecimal cantidadSiniestroHogar) {
		this.cantidadSiniestroHogar = cantidadSiniestroHogar;
	}
	public BigDecimal getCantidadVigenciaHogar() {
		return cantidadVigenciaHogar;
	}
	@XmlElement( name = "cantVigHogar" )
	public void setCantidadVigenciaHogar(BigDecimal cantidadVigenciaHogar) {
		this.cantidadVigenciaHogar = cantidadVigenciaHogar;
	}
	public BigDecimal getCantidadSiniestroVida() {
		return cantidadSiniestroVida;
	}
	@XmlElement( name = "cantStrVida" )
	public void setCantidadSiniestroVida(BigDecimal cantidadSiniestroVida) {
		this.cantidadSiniestroVida = cantidadSiniestroVida;
	}
	public BigDecimal getCantidadVigenciaVida() {
		return cantidadVigenciaVida;
	}
	@XmlElement( name = "cantVigVida" )
	public void setCantidadVigenciaVida(BigDecimal cantidadVigenciaVida) {
		this.cantidadVigenciaVida = cantidadVigenciaVida;
	}
	public BigDecimal getCanitdadSiniestroResto() {
		return canitdadSiniestroResto;
	}
	@XmlElement( name = "cantStrResto" )
	public void setCanitdadSiniestroResto(BigDecimal canitdadSiniestroResto) {
		this.canitdadSiniestroResto = canitdadSiniestroResto;
	}
	public BigDecimal getCantidadVigenciaResto() {
		return cantidadVigenciaResto;
	}
	@XmlElement( name = "cantVigResto" )
	public void setCantidadVigenciaResto(BigDecimal cantidadVigenciaResto) {
		this.cantidadVigenciaResto = cantidadVigenciaResto;
	}
	public BigDecimal getSiniestroAutoSobreVigencia() {
		return siniestroAutoSobreVigencia;
	}
	@XmlElement( name = "strAutoSoVig" )
	public void setSiniestroAutoSobreVigencia(BigDecimal siniestroAutoSobreVigencia) {
		this.siniestroAutoSobreVigencia = siniestroAutoSobreVigencia;
	}
	public BigDecimal getSiniestroHogarSobreVigencia() {
		return siniestroHogarSobreVigencia;
	}
	@XmlElement( name = "strHogaSoVig" )
	public void setSiniestroHogarSobreVigencia(
			BigDecimal siniestroHogarSobreVigencia) {
		this.siniestroHogarSobreVigencia = siniestroHogarSobreVigencia;
	}
	public BigDecimal getSiniestroVidaSobreVigencia() {
		return siniestroVidaSobreVigencia;
	}
	@XmlElement( name = "strVidaSoVig" )
	public void setSiniestroVidaSobreVigencia(BigDecimal siniestroVidaSobreVigencia) {
		this.siniestroVidaSobreVigencia = siniestroVidaSobreVigencia;
	}
	public BigDecimal getSiniestroRestoSobreVigencia() {
		return siniestroRestoSobreVigencia;
	}
	@XmlElement( name = "strRestSoVig" )
	public void setSiniestroRestoSobreVigencia(
			BigDecimal siniestroRestoSobreVigencia) {
		this.siniestroRestoSobreVigencia = siniestroRestoSobreVigencia;
	}
	
	

}
