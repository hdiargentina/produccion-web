package ar.com.hdi.productores.services.remote;

import java.util.Date;

import ar.com.hdi.productores.domain.list.DetalleProduccionDiariaList;
import ar.com.hdi.productores.domain.list.ProduccionAgrupadaList;
import ar.com.hdi.productores.domain.list.ProduccionDiariaList;

public interface ProduccionService {
	public ProduccionDiariaList obtenerDatosProduccionPorFecha(String empresa, String sucursal, Date fecha);
	public ProduccionDiariaList obtenerDatosProduccionAcumuladaPorFecha(String empresa, String sucursal, Date fecha);
	public ProduccionAgrupadaList obtenerDatosProduccionAgrupadaPorFecha(String empresa, String sucursal, Date fecha);
	public DetalleProduccionDiariaList obtenerDetalleProduccionDiariaPorFecha(String empresa, String sucursal, Date fecha);
}
