package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "conductorAuto" )
public class ConductorDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String apellido;
	private String nroRegistro;
	private Date fechaVtoRegistro;
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "conductorNombre" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	@XmlElement( name = "conductorApellido" )
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNroRegistro() {
		return nroRegistro;
	}
	@XmlElement( name = "conductorNroReg" )
	public void setNroRegistro(String nroRegistro) {
		this.nroRegistro = nroRegistro;
	}
	public Date getFechaVtoRegistro() {
		return fechaVtoRegistro;
	}
	@XmlElement( name = "conductorRegVto" )
	@XmlJavaTypeAdapter(FechaAdapter.class)
	public void setFechaVtoRegistro(Date fechaVtoRegistro) {
		this.fechaVtoRegistro = fechaVtoRegistro;
	}

}
