package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;

public class BancoDTO implements Serializable, Comparable<BancoDTO>{
	private static final long serialVersionUID = 1L;
	
	private Long codigo;
	private String nombre;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((BancoDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }
	@Override
	public int compareTo(BancoDTO o) {
		return this.getNombre().compareTo(o.getNombre());
	}
	
}
