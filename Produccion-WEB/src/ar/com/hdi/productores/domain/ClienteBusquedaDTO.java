package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 
 * @author nicoPera
 *
 *Esta clase es una version redusida de ClienteDTO solo se usa para que la busqueda en el cotizador y emision
 *sea más rápida. 
 *
 */
@XmlRootElement( name = "asegurado" )
public class ClienteBusquedaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal numero;
	private String nombre;
	private String tipoDocumento;
	private String numeroDocumento;
	private String numeroCuit;
	private String condicionIva;
	
	public BigDecimal getNumero() {
		return numero;
	}
	@XmlElement( name = "codigo" )
	public void setNumero(BigDecimal numero) {
		this.numero = numero;
	}
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "nombre" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	@XmlElement( name = "tipoDeDocumento" )
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	@XmlElement( name = "numeroDeDocumento" )
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getNumeroCuit() {
		return numeroCuit;
	}
	@XmlElement( name = "numeroDeCuit" )
	public void setNumeroCuit(String numeroCuit) {
		this.numeroCuit = numeroCuit;
	}
	public String getCondicionIva() {
		return condicionIva;
	}
	@XmlElement( name = "descripcionIva" )
	public void setCondicionIva(String condicionIva) {
		this.condicionIva = condicionIva;
	}
	

}
