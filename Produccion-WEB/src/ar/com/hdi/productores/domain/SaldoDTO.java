package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "saldo" )
public class SaldoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String fecha;
	private BigDecimal importe;
	private String condicion;
	public String getFecha() {
		return fecha;
	}
	@XmlElement( name = "fechaSaldo" )
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	@XmlElement( name = "importeSaldo" )
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public String getCondicion() {
		return condicion;
	}
	@XmlElement( name = "condicionSaldo" )
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	} 
	
	
}
