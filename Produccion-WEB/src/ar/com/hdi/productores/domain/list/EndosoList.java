package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.EndosoDTO;

@XmlRootElement( name = "suplementos" )
public class EndosoList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<EndosoDTO> endosos;

	public List<EndosoDTO> getEndosos() {
		return endosos;
	}
	
	@XmlElement( name = "suplemento" )
	public void setEndosos(List<EndosoDTO> endosos) {
		this.endosos = endosos;
	}
	
	public void add (EndosoDTO endoso) {
		if (this.endosos == null) {
			this.endosos = new ArrayList<EndosoDTO>();
		}
		this.endosos.add(endoso);
	}
	
	

}
