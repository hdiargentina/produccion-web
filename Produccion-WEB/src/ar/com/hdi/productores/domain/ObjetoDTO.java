package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "objeto" )
public class ObjetoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String detalle;
	private BigDecimal sumaAsegurada;
	private String marca;
	private String modelo;
	private String numeroSerie;
	private String detalleAdicional;
	
	public String getDetalle() {
		return detalle;
	}
	@XmlElement( name = "detalle" )
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}
	@XmlElement( name = "sumaAse" )
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getDetalleAdicional() {
		return detalleAdicional;
	}
	public void setDetalleAdicional(String detalleAdicional) {
		this.detalleAdicional = detalleAdicional;
	}
}
