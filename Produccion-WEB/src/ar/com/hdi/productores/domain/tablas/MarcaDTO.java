package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;
import java.math.BigDecimal;

public class MarcaDTO implements Serializable, Comparable<MarcaDTO> {
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String descripcion;
	private BigDecimal orden;
	
	public MarcaDTO() { }
	
	public MarcaDTO (MarcaDTO marca) {
		this.codigo = marca.getCodigo();
		this.descripcion = marca.getDescripcion();
		this.orden = marca.getOrden();
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getOrden() {
		return orden;
	}
	public void setOrden(BigDecimal orden) {
		this.orden = orden;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo != null)
            ? codigo.equals(((MarcaDTO) other).codigo)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo != null) 
            ? (getClass().hashCode() + codigo.hashCode())
            : super.hashCode();
    }
    
    @Override
	public int compareTo(MarcaDTO o) {
		if (orden.compareTo(o.orden) < 0) {
			return -1;
		}
		if (orden.compareTo(o.orden) > 0) {
			return 1;
		}
		return 0;
	}
	

}
