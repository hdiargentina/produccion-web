package ar.com.hdi.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.ObjectUtils;
import org.joda.time.DateTime;

public class HDIUtils {
	private static SecureRandom secureRandom = new SecureRandom();

	/**
	 * <p>
	 * From
	 * http://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha
	 * -numeric-string
	 * </p>
	 * 
	 * <p>
	 * This works by choosing 130 bits from a cryptographically secure random
	 * bit generator, and encoding them in base-32. 128 bits is considered to be
	 * cryptographically strong, but each digit in a base 32 number can encode 5
	 * bits, so 128 is rounded up to the next multiple of 5. This encoding is
	 * compact and efficient, with 5 random bits per character. Compare this to
	 * a random UUID, which only has 3.4 bits per character in standard layout,
	 * and only 122 random bits in total.
	 * </p>
	 * 
	 * @param length
	 *            length of secure token to return
	 * 
	 * @return
	 * @see <a
	 *      href="http://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string">how-to-generate-a-random-alpha-numeric-string</a>
	 */
	public static String getSecureToken(int length) {
		return new BigInteger(length * 5, secureRandom).toString(32);
	}

	public static String encodeURL(String name)
			throws UnsupportedEncodingException {
		return URLEncoder.encode(name, "UTF-8");
	}

	public static XMLGregorianCalendar StringtoXMLGregorianCalendar(String fecha)
			throws ParseException, DatatypeConfigurationException {
		SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
		Date d = simple.parse(fecha);
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(
				gc.get(Calendar.YEAR), gc.get(Calendar.MONTH) + 1,
				gc.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0,
				DatatypeConstants.FIELD_UNDEFINED);
	}

	public static Date toDate(BigDecimal date, String format) {
		return toDate(date.toString(), format);
	}

	public static Date toDate(String date, String format) {
		if (date.equals("") || format.equals("")
				|| date.equals("00000000") || date.equals("0")) {
			return null;
		}
		if (date.length() < 6) {
			date = "0" + date;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

	public static String toString(Object obj) {
		return ObjectUtils.toString(obj);
	}

	public static BigDecimal toBigDecimal(String str) {
		try {
			return new BigDecimal(str);
		} catch (Exception e) {
			return null;
		}
	}

	public static BigDecimal toBigDecimal(Integer i) {
		try {
			return new BigDecimal(i);
		} catch (Exception e) {
			return null;
		}
	}

	public static BigDecimal toBigDecimal(Long l) {
		try {
			if (l == null)
				return BigDecimal.ZERO;
			return new BigDecimal(l);
		} catch (Exception e) {
			return null;
		}
	}

	public static BigDecimal toBigDecimal(Date fecha) {
		if (fecha == null)
			return BigDecimal.ZERO;
		DateFormat fechaHora = new SimpleDateFormat("yyyyMMdd");
		String fechaConvertida = fechaHora.format(fecha);
		return toBigDecimal(fechaConvertida);
	}
	
	public static BigDecimal toBigDecimal(Date fecha, String format) {
		if (fecha == null)
			return BigDecimal.ZERO;
		DateFormat fechaHora = new SimpleDateFormat(format);
		String fechaConvertida = fechaHora.format(fecha);
		return toBigDecimal(fechaConvertida);
	}

	/**
	 * 
	 * @param obj
	 * @return Integer.valueOf or 0
	 */
	public static Integer toInt(Object obj) {
		try {
			return Integer.valueOf((toString(obj)));
		} catch (Exception e) {
			return 0;
		}
	}

	public static Long toLong(Object obj) {
		try {
			return Long.valueOf((toString(obj)));
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Long toLong(String str) {
		try {
			return Long.valueOf(str);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Long toLong(BigDecimal bigDecimal) {
		try {
			return bigDecimal.longValue();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param obj
	 * @return Boolean.valueOf or null
	 */
	public static Boolean toBool(Object obj) {
		try {
			return Boolean.valueOf((toString(obj)));
		} catch (Exception e) {
			return null;
		}
	}

	public static Date toDate(XMLGregorianCalendar calendar) {
		if (calendar == null) {
			return null;
		}
		return calendar.toGregorianCalendar().getTime();
	}

	public static String enmascararNumeroTarjeta(String numeroTarjeta) {
		String numeroEnmascarado = "";
		numeroEnmascarado = numeroEnmascarado.concat(numeroTarjeta
				.substring(numeroTarjeta.length() - 4));
		numeroEnmascarado = org.apache.commons.lang.StringUtils.leftPad(
				numeroEnmascarado, numeroTarjeta.length() - 4, "x");
		return numeroEnmascarado;
	}

	public static String dateToString(String format, Date fecha) {
		DateFormat df = new SimpleDateFormat(format);
		return df.format(fecha);
	}

	public static Date sumarDias(Date fecha, int dias) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(Calendar.DAY_OF_YEAR, dias);
		return calendar.getTime();
	}

	public static Date sumarMeses(Date fecha, int meses) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(Calendar.MONTH, meses);
		return calendar.getTime();
	}
	
	public static int diferenciaDias(Date fechaDesde, Date fechaHasta) {
		if (fechaDesde == null || fechaHasta == null)
			return 0;
		try {
			Calendar calendardesde = Calendar.getInstance();
			calendardesde.setTime(fechaDesde);
			calendardesde.set(Calendar.MILLISECOND, 0);
			calendardesde.set(Calendar.SECOND, 0);
			calendardesde.set(Calendar.MINUTE, 0);
			calendardesde.set(Calendar.HOUR, 0);
			
			Calendar calendarhasta = Calendar.getInstance();
			calendarhasta.setTime(fechaHasta);
			calendarhasta.set(Calendar.MILLISECOND, 0);
			calendarhasta.set(Calendar.SECOND, 0);
			calendarhasta.set(Calendar.MINUTE, 0);
			calendarhasta.set(Calendar.HOUR, 0);
			
	        
			int dias=(int) ((calendarhasta.getTime().getTime()-calendardesde.getTime().getTime())/86400000) + 1;
			return dias;
		} catch (Exception e) {
			return 0;
		}
	}
	
	public static String obtenerUrlFilenet(BigDecimal numeroPredenuncia) {
		String urlFilenet = HDIProperties.getPropiedad("filenet.properties", "protocoloFileNet") + HDIProperties.getPropiedad("filenet.properties", "ipFileNet") + ":" + HDIProperties.getPropiedad("filenet.properties", "puerto2FileNet");
		String desktop = HDIProperties.getPropiedad("filenet.properties", "desktopFileNet");
		String repositorio = HDIProperties.getPropiedad("filenet.properties", "objectstore");
		String template = HDIProperties.getPropiedad("filenet.properties", "templateFileNet");
		String version = HDIProperties.getPropiedad("filenet.properties", "objectstore");
		String docid = HDIProperties.getPropiedad("filenet.properties", "docidPredenunciasFilenet");
		String url = urlFilenet + "/navigator/bookmark.jsp?desktop=" + desktop + "&repositoryId=" + repositorio + 
					 "&docid=" + docid + "&template_name=" + template + "&version=" + 
					 version + "&searchCriteria=[{%22property%22%3A%22NumeroPredenuncia%22%2C%22value%22%3A%22" + 
					 numeroPredenuncia + "%22}]&autoRunSearch=true";
		return url;
	}
	public static Date obtenerPrimerDiaMes(Date fecha) {
//		Date fechaInicio = new Date();
		DateTime fechaInicioConvert = new DateTime(fecha);
		return fechaInicioConvert.dayOfMonth().withMinimumValue().toDate();
	}
}
