package ar.com.hdi.productores.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "danioAuto" )
public class DanioDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String estado;
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "danioNombre" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEstado() {
		return estado;
	}
	@XmlElement( name = "danioEstado" )
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

}
