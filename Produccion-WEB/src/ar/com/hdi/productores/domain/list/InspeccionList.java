package ar.com.hdi.productores.domain.list;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.hdi.productores.domain.InspeccionDTO;

@XmlRootElement( name = "inspeccionesSiniestros" )
public class InspeccionList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<InspeccionDTO> inspecciones;
	private BigDecimal cantidad;
	public List<InspeccionDTO> getInspecciones() {
		return inspecciones;
	}
	@XmlElement( name = "inspeccion" )
	public void setInspecciones(List<InspeccionDTO> inspecciones) {
		this.inspecciones = inspecciones;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	@XmlElement( name = "cantidad" )
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	
	

}
