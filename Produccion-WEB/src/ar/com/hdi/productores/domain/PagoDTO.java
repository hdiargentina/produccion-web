package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "pago" )
public class PagoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Date fechaLiquidacion;
	private BigDecimal importe;
	private String tipoBeneficiario;
	private String beneficiario;
	private String tipo;
	private String cobertura;
	private Date fechaPago;
	private BigDecimal numeroOrdenPago;
	public Date getFechaLiquidacion() {
		return fechaLiquidacion;
	}
	@XmlElement( name = "fechaLiq" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFechaLiquidacion(Date fechaLiquidacion) {
		this.fechaLiquidacion = fechaLiquidacion;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	@XmlElement( name = "importePago" )
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public String getTipoBeneficiario() {
		return tipoBeneficiario;
	}
	@XmlElement( name = "tipoBen" )
	public void setTipoBeneficiario(String tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	@XmlElement( name = "beneficiario" )
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getTipo() {
		return tipo;
	}
	@XmlElement( name = "tipoPago" )
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCobertura() {
		return cobertura;
	}
	@XmlElement( name = "cobertura" )
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	public Date getFechaPago() {
		return fechaPago;
	}
	@XmlElement( name = "fechaPago" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public BigDecimal getNumeroOrdenPago() {
		return numeroOrdenPago;
	}
	@XmlElement( name = "nroOdp" )
	public void setNumeroOrdenPago(BigDecimal numeroOrdenPago) {
		this.numeroOrdenPago = numeroOrdenPago;
	}
	
	
}
