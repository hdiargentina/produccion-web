package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ar.com.hdi.productores.domain.adapter.FechaAdapter;

@XmlRootElement( name = "mejora" )
public class MejoraDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String descripcion;
	private Date vencimiento;
	private String estado;
	private String cumplimiento;
	public String getDescripcion() {
		return descripcion;
	}
	@XmlElement( name = "descripcion" )
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getVencimiento() {
		return vencimiento;
	}
	@XmlElement( name = "vencimiento" )
	@XmlJavaTypeAdapter( FechaAdapter.class )
	public void setVencimiento(Date vencimiento) {
		this.vencimiento = vencimiento;
	}
	public String getEstado() {
		return estado;
	}
	@XmlElement( name = "estado" )
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCumplimiento() {
		return cumplimiento;
	}
	@XmlElement( name = "cumplimiento" )
	public void setCumplimiento(String cumplimiento) {
		this.cumplimiento = cumplimiento;
	}
	
	

}
