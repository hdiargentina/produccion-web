package ar.com.hdi.productores.domain.tablas;

import java.io.Serializable;

public class ParentescoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long codigo;
	private String codigo_equivalencia;
	private String descripcion;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getCodigo_equivalencia() {
		return codigo_equivalencia;
	}
	public void setCodigo_equivalencia(String codigo_equivalencia) {
		this.codigo_equivalencia = codigo_equivalencia;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && codigo_equivalencia != null)
            ? codigo_equivalencia.equals(((ParentescoDTO) other).codigo_equivalencia)
            : (other == this);
    }

    @Override
    public int hashCode() {
        return (codigo_equivalencia != null) 
            ? (getClass().hashCode() + codigo_equivalencia.hashCode())
            : super.hashCode();
    }

}
