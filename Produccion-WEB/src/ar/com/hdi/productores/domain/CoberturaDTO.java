package ar.com.hdi.productores.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "cobertura" )
public class CoberturaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private BigDecimal suma;
	private String tipoCobertura;
	private BigDecimal tasa;
	private BigDecimal prima;
	private List<ObjetoDTO> objetos;
	private String franquicia;
	
	public String getNombre() {
		return nombre;
	}
	@XmlElement( name = "nombreCob" )
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoCobertura() {
		return tipoCobertura;
	}
	@XmlElement ( name = "tipoCob" )
	public void setTipoCobertura(String tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}
	public BigDecimal getSuma() {
		return suma;
	}
	@XmlElement( name = "sumaCob" )
	public void setSuma(BigDecimal suma) {
		this.suma = suma;
	}
	public BigDecimal getTasa() {
		return tasa;
	}
	@XmlElement( name = "tasaCob" )
	public void setTasa(BigDecimal tasa) {
		this.tasa = tasa;
	}
	public BigDecimal getPrima() {
		return prima;
	}
	@XmlElement( name = "primCob" )
	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}
	public List<ObjetoDTO> getObjetos() {
		if (this.objetos == null)
			this.objetos = new ArrayList<ObjetoDTO>();
		return objetos;
	}
	@XmlElementWrapper( name = "objetos" )
	@XmlElement( name = "objeto" )
	public void setObjetos(List<ObjetoDTO> objetos) {
		this.objetos = objetos;
	}
	public String getFranquicia() {
		return franquicia;
	}
	@XmlElement( name = "franquicia" )
	public void setFranquicia(String franquicia) {
		this.franquicia = franquicia;
	}

}
